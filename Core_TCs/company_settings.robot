*** Settings ***
Library  SeleniumLibrary

*** Variables ***
#Browser used for running. Change this when testing other browsers.
${browser}   chrome

#URL for testing: Stage/Production, change this when testing other environments.
${url}   https://app.detailonline.com/auth/#/

#Credentials for accessing the tool
${username}  admin@detailonline.tech
${password}  adminpassword

*** Test Cases ***
Company Settings
#Select Browser, maximize window.
    log to console   Open Browser
    open browser     ${url}  ${browser}
    log to console   Maximize Browser Window
    maximize browser window
    sleep   5s
    set selenium speed  1s
    set selenium timeout  60s

#Test Cases:
    LoginToApplication
    sleep   30s
    SwitchCompany
    sleep   20s

8.0 Admin Support: verify that currency converter functions works as intended
     click element   xpath:/html/body/header/div[3]/div/div/header
     #click on admin support dropdown
     log to console  clicked admin support dropdown
     click element   xpath:/html/body/header/div[3]/div/div/div/section/div/section[1]/div[2]
     sleep  10s
     log to console  clicked on currency converter
     input text  xpath:/html/body/div[1]/div/div/section/div/div[3]/div[3]/input     15
     input text  xpath:/html/body/div[1]/div/div/section/div/div[2]/div[4]/input     13
     input text  xpath:/html/body/div[1]/div/div/section/div/div[2]/div[5]/input     39
     input text  xpath:/html/body/div[1]/div/div/section/div/div[8]/div[4]/input     91
     input text  xpath:/html/body/div[1]/div/div/section/div/div[11]/div[3]/input    12
     input text  xpath:/html/body/div[1]/div/div/section/div/div[12]/div[4]/input    41
     sleep  20s
     click element   xpath:/html/body/div[1]/div/div/footer/button[1]
     log to console  clicked on reset
     sleep  15s
     input text  xpath:/html/body/div[1]/div/div/section/div/div[3]/div[3]/input     15
     input text  xpath:/html/body/div[1]/div/div/section/div/div[2]/div[4]/input     13
     input text  xpath:/html/body/div[1]/div/div/section/div/div[2]/div[5]/input     39
     input text  xpath:/html/body/div[1]/div/div/section/div/div[8]/div[4]/input     91
     input text  xpath:/html/body/div[1]/div/div/section/div/div[11]/div[3]/input    12
     input text  xpath:/html/body/div[1]/div/div/section/div/div[12]/div[4]/input    41
     click element   xpath:/html/body/div[1]/div/div/footer/button[2]
     log to console  clicked on save
     sleep  10s
     #returned all values to 0
     input text  xpath:/html/body/div[1]/div/div/section/div/div[3]/div[3]/input     0
     input text  xpath:/html/body/div[1]/div/div/section/div/div[2]/div[4]/input     0
     input text  xpath:/html/body/div[1]/div/div/section/div/div[2]/div[5]/input     0
     input text  xpath:/html/body/div[1]/div/div/section/div/div[8]/div[4]/input     0
     input text  xpath:/html/body/div[1]/div/div/section/div/div[11]/div[3]/input    0
     input text  xpath:/html/body/div[1]/div/div/section/div/div[12]/div[4]/input    0
     click element   xpath:/html/body/div[1]/div/div/footer/button[2]
     log to console  clicked on save
     sleep  10s

8.0 Admin Support: User should be able to remove/add company brands
     8.0 Admin Support: User should be able to access Company settings
     sleep  15s
     click element  xpath:/html/body/div[1]/div/div/section[4]/div/div[1]/div/div[1]/label/i
     log to console     clicked on the first item on the list
     click element  xpath:/html/body/div[1]/div/div/section[4]/div/div[1]/div/div[1]/label/i
     log to console     clicked on the first item on the list
     click element  xpath:/html/body/div[1]/div/div/section[4]/div/div[1]/div/div[1]/label/i
     log to console     clicked on the first item on the list
     click element  xpath:/html/body/div[1]/div/div/footer/button[2]
     log to console     added new company brands
     sleep  10s
     click element  xpath:/html/body/header/div[1]/nav/ul/li[2]/a
     log to console     selected products module
     sleep  15s
     click element  xpath://*[@id="filter-menu"]
     log to console     click filter dropdown
     sleep  3s
     Wait until page contains    3D Systems     2s
     log to console     3d systems exists
     Wait until page contains    3dRudder     2s
     log to console     3drudder exists
     Wait until page contains    4K     2s
     log to console     4k exists
     log to console     added new brands to the system

8.0 Admin Support: User should be able to remove/add brands of special interest
     8.0 Admin Support: User should be able to access Company settings
     sleep  15s
     click element  xpath:/html/body/div[1]/div/div/section[5]/div/div[1]/div/div[1]/label/i
     log to console     clicked on the first item on the list
     click element  xpath:/html/body/div[1]/div/div/section[5]/div/div[1]/div/div[1]/label/i
     log to console     clicked on the first item on the list
     click element  xpath:/html/body/div[1]/div/div/section[5]/div/div[1]/div/div[1]/label/i
     log to console     clicked on the first item on the list
     click element  xpath:/html/body/div[1]/div/div/footer/button[2]
     log to console     added new brands of special interest
     sleep  10s
     click element  xpath:/html/body/header/div[1]/nav/ul/li[2]/a
     log to console     selected products module
     sleep  15s
     click element  xpath://*[@id="filter-menu"]
     log to console     click filter dropdown
     sleep  3s
     Wait until page contains    A.malavella     2s
     log to console     A.malavella exists
     Wait until page contains    A.montseny     2s
     log to console     A.montseny exists
     Wait until page contains    AbleNet     2s
     log to console     AbleNet exists
     log to console     added new brands of special interest to the system

8.0 Admin Support: User should be able to remove/add websites of interest
     8.0 Admin Support: User should be able to access Company settings
     sleep  15s
     click element  xpath:/html/body/div[1]/div/div/section[6]/div/div[1]/div/div[1]/label/i
     log to console     clicked on the first item on the list
     click element  xpath:/html/body/div[1]/div/div/section[6]/div/div[1]/div/div[1]/label/i
     log to console     clicked on the first item on the list
     click element  xpath:/html/body/div[1]/div/div/section[6]/div/div[1]/div/div[1]/label/i
     log to console     clicked on the first item on the list
     click element  xpath:/html/body/div[1]/div/div/footer/button[2]
     log to console     added new websites of interest

     sleep  5s
     click element  xpath:/html/body/header/div[1]/nav/ul/li[1]/a
     log to console     selected overview module
     sleep  15s
     click element  xpath://*[@id="product-overview"]/div[1]/div/div[1]/button/i
     log to console     click add group dropdown
     click element  xpath://*[@id="product-overview"]/div[1]/div/div[1]/div/button
     sleep  5s
     click element  xpath:/html/body/div[1]/div/div/section[2]/section/div/div[1]/div/label/i
     log to console     selected all retailers
     sleep  3s
     Wait until page contains    11st.co.kr     2s
     log to console     11st.co.kr exists
     Wait until page contains    1800petmeds.com     2s
     log to console     1800petmeds.com exists
     Wait until page contains    2beshop.com     2s
     log to console     2beshop.com exists
     log to console     newly added retailers are available in overview module

     sleep  5s
     click element  xpath:/html/body/header/div[1]/nav/ul/li[2]/a
     log to console     selected products module
     sleep  15s
     click element  xpath://*[@id="filter-menu"]
     log to console     click filter dropdown
     sleep  3s
     Wait until page contains    11st.co.kr     2s
     log to console     11st.co.kr exists
     Wait until page contains    1800petmeds.com     2s
     log to console     1800petmeds.com exists
     Wait until page contains    2beshop.com     2s
     log to console     2beshop.com exists
     log to console     newly added retailers are available in product module

     sleep  5s
     click element  xpath:/html/body/header/div[1]/nav/ul/li[3]/a
     log to console     selected comparisons module
     sleep  15s
     click element  xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/button/i
     log to console     click add group dropdown
     click element  xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div/button
     sleep  5s
     click element  xpath:/html/body/div[1]/div/div/section[2]/div/div[1]/div/label/i
     log to console     selected all retailers
     sleep  3s
     Wait until page contains    11st.co.kr     2s
     log to console     11st.co.kr exists
     Wait until page contains    1800petmeds.com     2s
     log to console     1800petmeds.com exists
     Wait until page contains    2beshop.com     2s
     log to console     2beshop.com exists
     log to console     newly added retailers are available in comparisons module

     sleep  5s
     click element  xpath:/html/body/header/div[1]/nav/ul/li[4]/a
     log to console     selected rankings module
     sleep  15s
     click element  xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/button
     log to console     click add group dropdown
     click element  xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div/button
     sleep  5s
     click element  xpath:/html/body/div[1]/div/div/section[2]/div/div[1]/div/label/i
     log to console     selected all retailers
     sleep  3s
     Wait until page contains    11st.co.kr     2s
     log to console     11st.co.kr exists
     Wait until page contains    1800petmeds.com     2s
     log to console     1800petmeds.com exists
     Wait until page contains    2beshop.com     2s
     log to console     2beshop.com exists
     log to console     newly added retailers are available in rankings module

     sleep  5s
     click element  xpath:/html/body/header/div[1]/nav/ul/li[5]/a
     log to console     selected compliance module
     sleep  15s
     click element  xpath:/html/body/div[1]/div/div[1]/div/div/div[1]/button
     log to console     click add group dropdown
     click element  xpath:/html/body/div[1]/div/div[1]/div/div/div[1]/div/button
     sleep  5s
     click element  xpath:/html/body/div[1]/div/div/section[2]/div/div[1]/div/label/i
     log to console     selected all retailers
     sleep  3s
     Wait until page contains    11st.co.kr     2s
     log to console     11st.co.kr exists
     Wait until page contains    1800petmeds.com     2s
     log to console     1800petmeds.com exists
     Wait until page contains    2beshop.com     2s
     log to console     2beshop.com exists
     log to console     newly added retailers are available in compliance module

     sleep  5s
     click element  xpath:/html/body/header/div[1]/nav/ul/li[6]/div[1]
     click element  xpath:/html/body/header/div[1]/nav/ul/li[6]/div[2]/a
     log to console     selected notifs module
     sleep  15s
     click element  xpath:/html/body/div[1]/div/div/div/div[1]/div[1]/button
     log to console     click add group dropdown
     click element  xpath:/html/body/div[1]/div/div/div/div[1]/div[1]/div/button
     sleep  5s
     click element  xpath:/html/body/div[1]/div/div/section[2]/section[5]/div/section/div[2]
     log to console     selected custom notification from*
     click element  xpath:/html/body/div[1]/div/div/section[2]/section[5]/section[2]/section[1]/div/div[1]/div/div[1]/label/i
     log to console     selected all retailers
     sleep  3s
     Wait until page contains    11st.co.kr     2s
     log to console     11st.co.kr exists
     Wait until page contains    1800petmeds.com     2s
     log to console     1800petmeds.com exists
     Wait until page contains    2beshop.com     2s
     log to console     2beshop.com exists
     log to console     newly added retailers are available in notifs module

8.0 Admin Support: reset company brands/special interest/retailers
     8.0 Admin Support: User should be able to access Company settings
     sleep  10s
     click element  xpath:/html/body/div[1]/div/div/section[4]/div/div[2]/div/div[1]/label/i
     click element  xpath:/html/body/div[1]/div/div/section[4]/div/div[2]/div/div[1]/label/i
     click element  xpath:/html/body/div[1]/div/div/section[4]/div/div[2]/div/div[1]/label/i
     click element  xpath:/html/body/div[1]/div/div/section[5]/div/div[2]/div/div[1]/label/i
     click element  xpath:/html/body/div[1]/div/div/section[5]/div/div[2]/div/div[1]/label/i
     click element  xpath:/html/body/div[1]/div/div/section[5]/div/div[2]/div/div[1]/label/i
     click element  xpath:/html/body/div[1]/div/div/section[6]/div/div[2]/div/div[1]/label/i
     click element  xpath:/html/body/div[1]/div/div/section[6]/div/div[2]/div/div[1]/label/i
     click element  xpath:/html/body/div[1]/div/div/section[6]/div/div[2]/div/div[1]/label/i
     log to console     deselected sample items
     click element  xpath:/html/body/div[1]/div/div/footer/button[2]
     sleep  10s

8.0 Admin Support: GDPR page functionalities should be working
    click element   xpath:/html/body/header/div[3]/div/div/header
    sleep   10s
    log to console  clicked on admin support dropdown
    click element   xpath:/html/body/header/div[3]/div/div/div/section/div/div/a
    sleep   10s
    log to console  clicked on privacy promise (GDPR)
    sleep   2s
    switch window   title=Detail
    sleep   5s

8.0 Admin Support: User should be able to access Company settings
#Access Company settings
     click element   xpath:/html/body/header/div[3]/div/div/header
     #click on admin support dropdown
     log to console  click on admin support dropdown
     click element   xpath:/html/body/header/div[3]/div/div/div/section/div/div/div[2]/a
     #click on company settings
     log to console  click on company settings
     sleep   10s

8.0 Admin Support: User should be able to remove/add "allowed features"
     click element  xpath:/html/body/div[1]/div/div/section[2]/div[1]/div/div
     #deselect all features
     log to console  deselected all features
     click element  xpath:/html/body/div[1]/div/div/footer/button[2]
     #should not be able to save
     log to console  not able to save with no selected features
     sleep  5s
     click element  xpath:/html/body/div[1]/div/div/section[2]/div[1]/ul/li[1]/label/span/span
     #selected comparisons only
     log to console  selected comparisons only
     sleep  3s
     click element  xpath:/html/body/div[1]/div/div/footer/button[2]
     sleep  10s
     Wait Until Page Does not Contain Element   xpath:/html/body/header/div[1]/nav/ul/li[3]/a    20s
     log to console  comparisons only module available in the nav bar
     sleep  10s

     click element  xpath:/html/body/div[1]/div/div/section[2]/div[1]/ul/li[4]/label/span/span
     #select products module
     log to console  selected products box
     sleep  3s
     click element  xpath:/html/body/div[1]/div/div/footer/button[2]
     #saved product modules
     Wait Until Page Contains Element   xpath:/html/body/header/div[1]/nav/ul/li[2]/a   20s
     Wait Until Page Contains Element   xpath:/html/body/header/div[1]/nav/ul/li[3]/a
     sleep  4s
     log to console  product module now available
     sleep  10s

     click element  xpath:/html/body/div[1]/div/div/section[2]/div[1]/ul/li[5]/label/span/span
     #selected rankings box
     log to console  selected rankings box
     sleep  3s
     click element  xpath:/html/body/div[1]/div/div/footer/button[2]
     #saved rankings modules
     Wait Until Page Contains Element   xpath:/html/body/header/div[1]/nav/ul/li[4]/a   20s
     Sleep  5s
     log to console  rankings module now available
     sleep  10s

     click element  xpath:/html/body/div[1]/div/div/section[2]/div[1]/ul/li[2]/label/span/span
     #selected compliance box
     log to console  selected compliance box
     sleep  3s
     click element  xpath:/html/body/div[1]/div/div/footer/button[2]
     #saved compliance module
     Wait Until Page Contains Element   xpath:/html/body/header/div[1]/nav/ul/li[5]/a   20s
     Sleep  5s
     log to console  compliance module now available
     sleep  10s

     click element  xpath:/html/body/div[1]/div/div/section[2]/div[1]/ul/li[3]/label/span/span
     #selected notification box
     log to console  selected notification box
     sleep  3s
     click element  xpath:/html/body/div[1]/div/div/footer/button[2]
     #saved notification module
     Wait Until Page Contains Element   xpath:/html/body/header/div[1]/nav/ul/li[6]/div[1]   20s
     Sleep  5s
     log to console  notification module now available
     sleep  10s

     click element  xpath:/html/body/div[1]/div/div/section[2]/div[1]/ul/li[6]/label/span/span
     #selected user management box
     log to console  selected user management box
     sleep  3s
     click element  xpath:/html/body/div[1]/div/div/footer/button[2]
     #saved user management option
     sleep  8s
     click element  xpath:/html/body/header/div[3]/div/div/header
     log to console  selected admin support dropdown
     click element  xpath:/html/body/header/div[3]/div/div/div/section/div/div/div[1]/a
     log to console  clicked on user management
     Wait Until Page Contains Element  xpath:/html/body/div[1]/div/div/section[2]/div[1]  10s
     click element  xpath:/html/body/header/div[3]/div/div/header
     log to console  selected admin support dropdown
     click element  xpath:/html/body/header/div[3]/div/div/div/section/div/div/div[2]/a
     Sleep  5s
     log to console  user management option now available in admin support
     sleep  10s

8.0 Admin Support: User should be able to remove/add "add-ons"
#still in company settings
    click element   xpath:/html/body/div[1]/div/div/section[2]/div[2]/div[1]/div[1]/div[2]/label/span/span
    #deselect Title Compliance
    log to console  deselected title compliance
    sleep   4s
    click element   xpath:/html/body/div[1]/div/div/footer/button[2]
    log to console  clicked on save
    sleep   10s
    click element   xpath:/html/body/header/div[1]/nav/ul/li[5]/a
    sleep   10s
    wait until page does not contain    Product Title   15s
    log to console  product title  no longer available in compliance module
    8.0 Admin Support: User should be able to access Company settings
    click element   xpath:/html/body/div[1]/div/div/section[2]/div[2]/div[1]/div[1]/div[2]/label/span/span

    click element   xpath:/html/body/div[1]/div/div/section[2]/div[2]/div[1]/div[1]/div[3]/label/span/span
    #deselect description Compliance
    log to console  deselected description compliance
    sleep   4s
    click element   xpath:/html/body/div[1]/div/div/footer/button[2]
    log to console  clicked on save
    sleep   10s
    click element   xpath:/html/body/header/div[1]/nav/ul/li[5]/a
    sleep   10s
    wait until page does not contain    Product Description   15s
    log to console  product description no longer available in compliance module
    8.0 Admin Support: User should be able to access Company settings
    click element   xpath:/html/body/div[1]/div/div/section[2]/div[2]/div[1]/div[1]/div[3]/label/span/span

    click element   xpath:/html/body/div[1]/div/div/section[2]/div[2]/div[1]/div[1]/div[4]/label/span/span
    #deselect product image Compliance
    log to console  deselected product image compliance
    sleep   4s
    click element   xpath:/html/body/div[1]/div/div/footer/button[2]
    log to console  clicked on save
    sleep   10s
    click element   xpath:/html/body/header/div[1]/nav/ul/li[5]/a
    sleep   10s
    wait until page does not contain    Image Matched   15s
    log to console  product image compliance no longer available in compliance module
    8.0 Admin Support: User should be able to access Company settings
    click element   xpath:/html/body/div[1]/div/div/section[2]/div[2]/div[1]/div[1]/div[4]/label/span/span

    click element   xpath:/html/body/div[1]/div/div/section[2]/div[2]/div[1]/div[1]/div[5]/label/span/span
    #deselect product specs Compliance
    log to console  deselected product specs compliance
    sleep   4s
    click element   xpath:/html/body/div[1]/div/div/footer/button[2]
    log to console  clicked on save
    sleep   10s
    click element   xpath:/html/body/header/div[1]/nav/ul/li[5]/a
    sleep   10s
    wait until page does not contain    Specs   15s
    log to console  product specs no longer available in compliance module
    8.0 Admin Support: User should be able to access Company settings
    click element   xpath:/html/body/div[1]/div/div/section[2]/div[2]/div[1]/div[1]/div[5]/label/span/span

    click element   xpath:/html/body/div[1]/div/div/section[2]/div[2]/div[2]/div/div[2]/label/span/span
    #selected amazon buybox
    log to console  amazon buybox selected
    sleep   4s
    click element   xpath:/html/body/div[1]/div/div/footer/button[2]
    log to console  clicked on save
    sleep   15s
    wait until page contains element    xpath:/html/body/header/div[1]/nav/ul/li[7]     15s
    log to console  amazon buybox now available in others module
    sleep   10s
    click element   xpath:/html/body/div[1]/div/div/section[2]/div[2]/div[2]/div/div[2]/label/span/span
    sleep   5s
    click element   xpath:/html/body/div[1]/div/div/footer/button[2]
    log to console  amazon buybox removed from module
    sleep   10s

8.0 Admin Support: User should be able to remove/add product types
    click element   xpath:/html/body/div[1]/div/div/section[3]/section/section/md-chips/md-chips-wrap/div/input
    input text  xpath:/html/body/div[1]/div/div/section[3]/section/section/md-chips/md-chips-wrap/div/input     Applesauce
    log to console  added applesauce to product types
    Press keys  xpath:/html/body/div[1]/div/div/section[3]/section/section/md-chips/md-chips-wrap/div/input  ENTER
    sleep   4s
    click element   xpath:/html/body/div[1]/div/div/footer/button[2]
    log to console  saved applesauce to product types
    sleep   12s
    click element   xpath:/html/body/header/div[1]/nav/ul/li[2]/a
    sleep   20s
    click element   xpath:/html/body/div[1]/div/div/section[1]/div[2]/div[1]/header
    Wait until page contains    Applesauce
    log to console  Applesauce *exists*
    8.0 Admin Support: User should be able to access Company settings
    click element   xpath:/html/body/div[1]/div/div/section[3]/section/section/md-chips/md-chips-wrap/md-chip[1]/div[2]/button/md-icon
    sleep   5s
    log to console  applesauce removed :<
    sleep   4s
    click element   xpath:/html/body/div[1]/div/div/footer/button[2]
    sleep   10s

8.0 Admin Support: User should be able to change the company name
#change company name
     sleep   10s
     input text  xpath:/html/body/div[1]/div/div/section[1]/input    US Demo Company Test change
     sleep   4s
     click element  xpath:/html/body/div[1]/div/div/footer/button[2]
     sleep   5s

    logoutoffapplication
    sleep   10s
    LoginToApplication
    sleep   20s
    SwitchCompany

8.0 Admin Support: User should be able to change the company name *RECHANGE TO DEFAULT
#re change company name
     8.0 Admin Support: User should be able to access Company settings
     sleep   10s
     input text  xpath:/html/body/div[1]/div/div/section[1]/input    US Demo
     sleep   4s
     click element  xpath:/html/body/div[1]/div/div/footer/button[2]
     sleep   5s


*** Keywords ***
LoginToApplication
#Login to Detail App
    #Input correct email address
    log to console   LoginToApplication - test case start execute
    log to console  Input correct username
    input text   xpath:/html/body/div/div[1]/form/div[1]/div/input   ${username}     #Input username
    #Input correct password
    input text   xpath:/html/body/div/div[1]/form/div[2]/div[1]/div/div/input    ${password}     #Input password
    log to console   Input correct password
    #Select login button
    click element  xpath:/html/body/div/div[1]/form/div[3]/span  #Select login button
    log to console   Select login button: Done
    log to console   LoginToApplication test execution DONE

LogoutOffApplication
#Logout off Detail App
     click element   xpath:/html/body/header/div[3]/div/div/header
     #click on admin support dropdown
     log to console  clicked admin support dropdown
     click element   xpath:/html/body/header/div[3]/div/div/div/section/div/section[1]/div[4]
     #Click on logout
     log to console  clicked logout
     sleep  2s
     click element   xpath:/html/body/popup/div/div/div/div[3]/button[2]
     #Confirm logout
     log to console  clicked on Yes and confirmed logout

SwitchCompany
     sleep  10s
     click element   xpath:/html/body/header/div[3]/div/div/header
     #click on admin support dropdown
     log to console  clicked admin support dropdown
     sleep   2s
     click element   xpath:/html/body/header/div[3]/div/div/div/section/div/header
     #click on company list
     log to console  cicked on list of companies
     sleep   1s
     click element   xpath:/html/body/header/div[3]/div/div/div/section/div/section/div[2]/div[5]/label/span
     #select company demo
     log to console  selected US demo company
     sleep   1s
     click element   xpath:/html/body/header/div[3]/div/div/header
     log to console  loading for US demo company
     sleep   15s
     log to console  COMPANY SWITCH DONE

8.0 Admin Support: User should be able to access Company settings
#Access Company settings
     click element   xpath:/html/body/header/div[3]/div/div/header
     #click on admin support dropdown
     log to console  click on admin support dropdown
     click element   xpath:/html/body/header/div[3]/div/div/div/section/div/div/div[2]/a
     #click on company settings
     log to console  click on company settings
     sleep   10s