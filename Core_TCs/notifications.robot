*** Settings ***
Library  SeleniumLibrary

*** Variables ***
#Browsers used for running. Change this when testing other browsers.
${browser}   chrome
#${browser}  mozilla firefox

#URL for testing: Stage/Production, change this when testing other environments.
${url}   https://detail-ultima-staging.azurewebsites.net/    #staging link
#${url}   https://app.detailonline.com    #production link

#Credentials for accessing the tool
${username}  admin@detailonline.tech
${password}  adminpassword
${groupname}     Test Group [QA]

#Credentials for accessing the tool

*** Test Cases ***
7.0 Notifications
#Settings
    open browser     ${url}  ${browser}
    maximize browser window
    set selenium speed  1s
    set selenium timeout     120s
    Set Selenium Implicit Wait   120s

#Test Case Scenarios
    LoginToDetail
    SwitchCompany
    7.0 Notifications - Proceed to Notifications Page
    7.0 Notifications - Create Notif Group with missing fields (Rankings)
    7.0 Notifications - Create Notif Group with missing fields (Comparisons)
    7.0 Notifications - Create Notif Group with missing fields (Compliance)


*** Keywords ***
LoginToDetail
    log to console   Initialize login to Application......
        input text   xpath:/html/body/div/div[1]/form/div[1]/div/input   ${username}                                                            #this line inputs a username
        input text   xpath:/html/body/div/div[1]/form/div[2]/div[1]/div/div/input    ${password}                                                #this line inputs a password
     click element   xpath:/html/body/div/div[1]/form/div[3]/span                                                                               #click Login button to officially login to detail
    log to console   Successfully login to application.....

SwitchCompany
#Switch to Demo Company --
    log to console   Initialize switch to another company (DEMO)
    Wait until Element is Enabled    xpath:/html/body/header/div[3]/div/div/header/div/div[2]
    click element    xpath:/html/body/header/div[3]/div/div/header/div/div[2]                                                                       #this line clicks Admin Panel
    click element    xpath:/html/body/header/div[3]/div/div/div/section/div/header                                                                  #this line clicks existing company
    click element    xpath:/html/body/header/div[3]/div/div/div/section/div/section/div[2]/div[1]/label/span                                        #this line selects another company
    click element    xpath:/html/body/div[1]/div                                                                                                    #this line clicks element from the page
    log to console   Successfully accessed company: DEMO

7.0 Notifications - Proceed to Notifications Page
    Wait Until Element is Enabled    xpath:/html/body/header/div[1]/nav/ul/li[6]/div[1]
                    click element    xpath:/html/body/header/div[1]/nav/ul/li[6]/div[1]                                                             #Click Notification Panel
    Wait until Element is Enabled    xpath:/html/body/header/div[1]/nav/ul/li[6]/div[2]/a
                    click element    xpath:/html/body/header/div[1]/nav/ul/li[6]/div[2]/a                                                           #Click Notification link and redirect user to Notif Page

7.0 Notifications - Create Notif Group with missing fields (Rankings)
                   log to console    7.0 Notifications - Create Notif Group with missing fields (Rankings)
                            sleep    10s
    Wait until Element is Enabled    xpath:/html/body/div[1]/div/div/div/div[1]/div[1]/button/i
                    click element    xpath:/html/body/div[1]/div/div/div/div[1]/div[1]/button/i                                                     #Click Group menu
                    click element    xpath:/html/body/div[1]/div/div/div/div[1]/div[1]/div/button                                                   #Click Add new group

#Validation No.1: Create Group w/out title
                   log to console    Validation No.1: Create Group w/out title
    Wait until Element is Enabled    xpath:/html/body/div[1]/div/div/section[2]/section[3]/section/div[1]/i[1]
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[3]/section/div[1]/i[1]                                      #Select Notification Level
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[4]/section/div/section[1]/i                                 #Select from Notify when
                       Press keys    xpath:/html/body/div[1]/div/div/footer/button[2]     Downwards Arrow
                    click element    xpath://*[@id="dropdownMenuButton"]                                                                            #Select "Select group" button
    Wait until Element is Enabled    xpath:/html/body/div[1]/div/div/section[2]/section[5]/section[1]/div/section/div/div/div/div/ul/li[1]/span
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[5]/section[1]/div/section/div/div/div/div/ul/li[1]/span     #Select an existing group
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[6]/section/div/div[1]/div/div/div/ul/li[2]/label/i          #Select existing user
       Element should be Disabled    xpath:/html/body/div[1]/div/div/footer/button[2]                                                               #Check if Save button is disabled
                   log to console    Unable to create group w/out title - PASSED

#Validation No.2: Create group w/out notification level selected
                   log to console    Validation No.2: Create Group w/out Notification Level
                       #Press keys    xpath:/html/body/div[1]/div/div/section[1]/header/button   Upwards Arrow
                       press keys    xpath:/html/body/div[1]/div/div/section[1]/header/div   scroll up                                              #Scroll up
                    click element    xpath:/html/body/div[1]/div/div/section[1]/header/button                                                       #Click Back button to redirect to main page
    Wait until Element is Enabled    xpath:/html/body/div[1]/div/div/div/div[1]/div[1]/button
                    click element    xpath:/html/body/div[1]/div/div/div/div[1]/div[1]/button                                                       #Click Group menu
                    click element    xpath:/html/body/div[1]/div/div/div/div[1]/div[1]/div/button                                                   #Click Add new group
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[1]/input                                                    #Click title text box
                       Input Text    xpath:/html/body/div[1]/div/div/section[2]/section[1]/input     ${groupname}                                   #input title Test Group [QA]
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[4]/section/div/section[1]/i                                 #Select from Notify when
                       Press keys    xpath:/html/body/div[1]/div/div/footer/button[2]     Downwards Arrow
                    click element    xpath://*[@id="dropdownMenuButton"]                                                                            #Select "Select group" button
    Wait until Element is Enabled    xpath:/html/body/div[1]/div/div/section[2]/section[5]/section[1]/div/section/div/div/div/div/ul/li[1]/span
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[5]/section[1]/div/section/div/div/div/div/ul/li[1]/span     #Select an existing group
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[6]/section/div/div[1]/div/div/div/ul/li[2]/label/i          #Select existing user
       Element should be Disabled    xpath:/html/body/div[1]/div/div/footer/button[2]                                                               #Check if Save button is disabled
                   log to console    Unable to create group w/out Notification Level - PASSED

#Validation No.3: Create group w/out selecting "Notify when"
                   log to console    Validation No.3: Create group w/out selecting "Notify when"
                      #press keys   xpath:/html/body/div[1]/div/div/section[2]/section[1]/input   .
                            sleep    2s
                       press keys    xpath:/html/body/div[1]/div/div/section[1]/header/div   scroll up                                               #Scroll up
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[3]/section/div[2]/i[1]                                       #Click Notify Level
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[4]/section/div/section[1]/i                                  #Unselect Notify when
                            sleep    2s
                       Press keys    xpath:/html/body/div[1]/div/div/footer/button[2]     Downwards Arrow                                           #Scroll down
       Element should be Disabled    xpath:/html/body/div[1]/div/div/footer/button[2]                                                               #Check if Save button is disabled
                   log to console    Verified Save button is Disabled
                   log to console    Unable to Create group w/out selecting "Notify when" - PASSED

#Validation No.4: Create group w/out selecting "Notify from"
                   log to console    Validation No.4: Create group w/out selecting "Notify from"
                            sleep    2s
                       press keys    xpath:/html/body/div[1]/div/div/section[1]/header/div   .........                                              #Scroll up
                    click element    xpath:/html/body/div[1]/div/div/section[1]/header/button                                                       #Click Back button to redirect to main page
    Wait until Element is Enabled    xpath:/html/body/div[1]/div/div/div/div[1]/div[1]/button
                    click element    xpath:/html/body/div[1]/div/div/div/div[1]/div[1]/button                                                       #Click Group menu
                    click element    xpath:/html/body/div[1]/div/div/div/div[1]/div[1]/div/button                                                   #Click Add new group
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[1]/input                                                    #Click title text box
                       Input Text    xpath:/html/body/div[1]/div/div/section[2]/section[1]/input     ${groupname}                                   #input title Test Group [QA]
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[3]/section/div[2]/i[1]                                      #Select Notif Level
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[4]/section/div/section[1]/i                                 #Select from Notify when
                   log to console    Selected Notification Level and Notify when
                       Press keys    xpath:/html/body/div[1]/div/div/footer     Downwards Arrow                                                     #Scroll down
    Wait until Element is Enabled    xpath:/html/body/div[1]/div/div/section[2]/section[5]/div/section/div[2]/i
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[5]/div/section/div[2]/i                                     #Click Custom
                   log to console    Selected "Custom" but no retailer and brand selected
                       Press keys    xpath:/html/body/div[1]/div/div/footer  Downwards Arrow                                                        #Scroll down
       Element should be Disabled    xpath:/html/body/div[1]/div/div/footer/button[2]                                                               #Check if Save button is disabled
                   log to console    Save button is disabled if no website or product selected
                   log to console    Select 'Groups' button
                       Press keys    xpath:/html/body/div[1]/div/div/section[2]/section[5]/div/section/div[2]/i  ....                               #scroll up to Notify from section
    #Wait until Element is Enabled    xpath://*[@id="dropdownMenuButton"]
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[5]/div/section/div[1]/i                                     #Click 'Groups' Radio button
       Element should be Disabled    xpath:/html/body/div[1]/div/div/footer/button[2]                                                               #Check if Save button is disabled
                   log to console    Save button is disabled if no website or product selected

7.0 Notifications - Create Notif Group with missing fields (Comparisons)
                   log to console    7.0 Notifications - Create Notif Group with missing fields (Comparisons)
                            sleep    10s
                    click element    xpath:/html/body/div[1]/div/div/footer/button[1]                                                               #Click cancel button
    Wait until Element is Enabled    xpath:/html/body/div[1]/div/div/div/div[1]/div[1]/button
                    click element    xpath:/html/body/div[1]/div/div/div/div[1]/div[1]/button                                                       #Click Group menu
                    click element    xpath:/html/body/div[1]/div/div/div/div[1]/div[1]/div/button                                                   #Click Add new group

#Validation No.1: Create Group w/out title (Comparisons)
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[2]/section/div[2]/i                                         #click type of group Comparisons
                   log to console    Validation No.1: Create Group w/out title (Comparisons)
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[3]/section/div[1]/i[1]                                      #Select Notification Level
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[4]/section/div/section[1]/i                                 #Select from Notify when
                       Press keys    xpath:/html/body/div[1]/div/div/footer  Downwards Arrow                                                        #Scroll down to the bottom of the page
                    click element    xpath://*[@id="dropdownMenuButton"]                                                                            #Select "Select group" button
    Wait until Element is Enabled    xpath:/html/body/div[1]/div/div/section[2]/section[5]/section[1]/div/section/div/div/div/div/ul/li[1]/span
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[5]/section[1]/div/section/div/div/div/div/ul/li[1]/span     #Select an existing group
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[6]/section/div/div[1]/div/div/div/ul/li[2]/label/i          #Select existing user
       Element should be Disabled    xpath:/html/body/div[1]/div/div/footer/button[2]                                                               #Check if Save button is disabled
                   log to console    Unable to create group w/out title - PASSED

#Validation No.2: Create group w/out notification level selected (Comparisons)
                   log to console    Validation No.2: Create Group w/out Notification Level - (Comparisons)
                       press keys    xpath:/html/body/div[1]/div/div/section[1]/header/div   scroll up                                              #Scroll up
                            sleep    2s
                    click element    xpath:/html/body/div[1]/div/div/section[1]/header/button                                                       #Click Back button to redirect to main page
    Wait until Element is Enabled    xpath:/html/body/div[1]/div/div/div/div[1]/div[1]/button
                    click element    xpath:/html/body/div[1]/div/div/div/div[1]/div[1]/button                                                       #Click Group menu
                    click element    xpath:/html/body/div[1]/div/div/div/div[1]/div[1]/div/button                                                   #Click Add new group
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[1]/input                                                    #Click title text box
                       Input Text    xpath:/html/body/div[1]/div/div/section[2]/section[1]/input     ${groupname}                                   #input title Test Group [QA]
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[2]/section/div[2]/i                                         #click type of group Comparisons
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[4]/section/div/section[1]/i                                 #Select from Notify when
                   log to console    Inputed title and selected Notify when....
                       Press keys    xpath:/html/body/div[1]/div/div/footer  Downwards Arrow                                                        #Scroll down to "whom to notify section"
                   log to console    Scroll down to bottom of the page
                    click element    xpath://*[@id="dropdownMenuButton"]                                                                            #Select "Select group" button
    Wait until Element is Enabled    xpath:/html/body/div[1]/div/div/section[2]/section[5]/section[1]/div/section/div/div/div/div/ul/li[1]/span
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[5]/section[1]/div/section/div/div/div/div/ul/li[1]/span     #Select an existing group
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[6]/section/header/div/span[1]                               #Select existing user
       Element should be Disabled    xpath:/html/body/div[1]/div/div/footer/button[2]                                                               #Check if Save button is disabled
                   log to console    Unable to create group w/out Notification Level - PASSED

#Validation No.3: Create group w/out selecting "Notify when" - Comparisons
                   log to console    Validation No.3: Create group w/out selecting "Notify when" - Comparisons
                            sleep    2s
                       press keys    xpath:/html/body/div[1]/div/div/section[1]/header/div   scroll up                                              #Scroll up
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[3]/section/div[2]/i[1]                                      #Click Notify Level
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[4]/section/div/section[1]/i                                 #Unselect Notify when
                            sleep    2s
                       Press keys    xpath:/html/body/div[1]/div/div/footer/button[2]     Downwards Arrow                                           #Scroll down to Selected Retailer section
       Element should be Disabled    xpath:/html/body/div[1]/div/div/footer/button[2]                                                               #Check if Save button is disabled
                   log to console    Verified Save button is Disabled
                   log to console    Unable to Create group w/out selecting "Notify when" - PASSED

#Validation No.4: Create group w/out selecting "Notify from"
                   log to console    Validation No.4: Create group w/out selecting "Notify from" - Comparisons
                       press keys    xpath:/html/body/div[1]/div/div/section[1]/header/div   .........                                             #Scroll up
                            sleep    2s
                    click element    xpath:/html/body/div[1]/div/div/section[1]/header/button                                                      #Click Back button to redirect to main page
    Wait until Element is Enabled    xpath:/html/body/div[1]/div/div/div/div[1]/div[1]/button
                    click element    xpath:/html/body/div[1]/div/div/div/div[1]/div[1]/button                                                      #Click Group menu
                    click element    xpath:/html/body/div[1]/div/div/div/div[1]/div[1]/div/button                                                  #Click Add new group
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[1]/input                                                   #Click title text box
                       Input Text    xpath:/html/body/div[1]/div/div/section[2]/section[1]/input     ${groupname}                                  #input title Test Group [QA]
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[2]/section/div[2]/i                                        #click type of group Comparisons
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[3]/section/div[2]/i[1]                                     #Select Notif Level
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[4]/section/div/section[1]/i                                #Select from Notify when
                   log to console    Selected Notification Level and Notify when
                       Press keys    xpath:/html/body/div[1]/div/div/section[2]/section[6]/section/div/div[2]/div[1]     Q                         #Scroll down to Selected Retailer section
    Wait until Element is Enabled    xpath:/html/body/div[1]/div/div/section[2]/section[5]/div/section/div[2]/i
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[5]/div/section/div[2]/i                                    #Click Custom
                   log to console    Selected "Custom" but no retailer and brand selected
                       Press keys    xpath:/html/body/div[1]/div/div/footer/button[2]     Downwards Arrow                                          #Scroll down
       Element should be Disabled    xpath:/html/body/div[1]/div/div/footer/button[2]                                                              #Check if Save button is disabled
                   log to console    Save button is disabled if no website or product selected
                   log to console    Select 'Groups' button
                       #press keys    xpath:/html/body/div[1]/div/div/section[2]/section[5]/div/section/div[2]/i  ....                             #scroll up to Notify from section
    #Wait until Element is Enabled    xpath://*[@id="dropdownMenuButton"]
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[5]/div/section/div[1]/i                                    #Click 'Groups' Radio button
       Element should be Disabled    xpath:/html/body/div[1]/div/div/footer/button[2]                                                              #Check if Save button is disabled
                   log to console    Save button is disabled if no website or product selected

#Validation No.5: Create group w/out selecting "whom to notify" - Comparisons
                   log to console    Validation No.5: Create group w/out selecting "whom to notify" - Comparisons
                    click element    xpath:/html/body/div[1]/div/div/footer/button[1]                                                              #Click Cancel button
    Wait until Element is Enabled    xpath:/html/body/div[1]/div/div/div/div[1]/div[1]/button
                    click element    xpath:/html/body/div[1]/div/div/div/div[1]/div[1]/button                                                      #Click Group menu
                    click element    xpath:/html/body/div[1]/div/div/div/div[1]/div[1]/div/button                                                  #Click Add new group
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[1]/input                                                   #Click title text box
                       Input Text    xpath:/html/body/div[1]/div/div/section[2]/section[1]/input     ${groupname}                                  #input title Test Group [QA]
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[2]/section/div[2]/i                                        #click type of group Comparisons
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[3]/section/div[2]/i[1]                                     #Select Notif Level
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[4]/section/div/section[1]/i                                #Select from Notify when
                   log to console    Selected Notification Level and Notify when
               execute javascript     window.scrollTo(document.body.scrollHeight,0)
                    click element    xpath://*[@id="dropdownMenuButton"]                                                                            #Select dropdown button to select existing group
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[5]/section[1]/div/section/div/div/div/div/ul/li[1]          #Select existing group
        Element should be Enabled    xpath:/html/body/div[1]/div/div/footer/button[2]                                                               #Verify if Save button is enabled
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[6]/section/div/div[2]/div[2]/label/i                        #Unselect the email recipients
                   log to console     Unselected email recipients
       Element should be Disabled    xpath:/html/body/div[1]/div/div/footer/button[2]                                                               #Verify if Save button is Disabled
                   log to console     Cannot add group w/out selecting recipient



7.0 Notifications - Create Notif Group with missing fields (Compliance)
                            sleep    10s
                       Press Keys    xpath:/html/body/div[1]/div/div/footer  Downwards Arrows
                   log to console    7.0 Notifications - Create Notif Group with missing fields (Compliance)
                    click element    xpath:/html/body/div[1]/div/div/footer/button[1]                                                              #Click cancel button
    Wait until Element is Enabled    xpath:/html/body/div[1]/div/div/div/div[1]/div[1]/button/i
                    click element    xpath:/html/body/div[1]/div/div/div/div[1]/div[1]/button/i                                                     #Click Group menu
                    click element    xpath:/html/body/div[1]/div/div/div/div[1]/div[1]/div/button                                                   #Click Add new group

#Validation No.1: Create Group w/out title (Compliance)
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[2]/section/div[3]/i                                         #click type of group Comliance
                   log to console    Validation No.1: Create Group w/out title (Compliance)
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[3]/section/div[1]/i[1]                                      #Select Notification Level
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[3]/section/div[1]/i[1]                                      #Select from Notify when
                       Press keys    xpath:/html/body/div[1]/div/div/footer  Downwards Arrow                                                        #Scroll down to the bottom of the page
                    click element    xpath://*[@id="dropdownMenuButton"]                                                                            #Select "Select group" button
    Wait until Element is Enabled    xpath:/html/body/div[1]/div/div/section[2]/section[5]/section[1]/div/section/div/div/div/div/ul/li[1]/span
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[5]/section[1]/div/section/div/div/div/div/ul/li[1]/span     #Select an existing group
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[6]/section/div/div[1]/div/div/div/ul/li[2]/label/i          #Select existing user
       Element should be Disabled    xpath:/html/body/div[1]/div/div/footer/button[2]                                                               #Check if Save button is disabled
                   log to console    Unable to create group w/out title - PASSED

#Validation No.2: Create group w/out notification level selected (Compliance)
                   log to console    Validation No.2: Create Group w/out Notification Level - (Compliance)
                       press keys    xpath:/html/body/div[1]/div/div/section[1]/header/div   scroll up                                              #Scroll up
                            sleep    2s
                    click element    xpath:/html/body/div[1]/div/div/section[1]/header/button                                                       #Click Back button to redirect to main page
    Wait until Element is Enabled    xpath:/html/body/div[1]/div/div/div/div[1]/div[1]/button
                    click element    xpath:/html/body/div[1]/div/div/div/div[1]/div[1]/button                                                       #Click Group menu
                    click element    xpath:/html/body/div[1]/div/div/div/div[1]/div[1]/div/button                                                   #Click Add new group
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[1]/input                                                    #Click title text box
                       Input Text    xpath:/html/body/div[1]/div/div/section[2]/section[1]/input     ${groupname}                                   #input title Test Group [QA]
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[2]/section/div[3]/i                                         #click type of group Compliance
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[4]/section/div/section[1]/i                                 #Select from Notify when
                   log to console    Inputed title and selected Notify when....
              #execute javascript    window.scrollTo(document.body.scrollHeight,0)                                                                  #Scroll down to bottom of the page
                       #Press keys    xpath:/html/body/div[1]/div/div/section[2]/section[6]/section/div/div[1]/input  Scroll Down                   #Scroll down to "whom to notify section"
                       Press keys    xpath:/html/body/div[1]/div/div/footer  Downwards Arrow
                   log to console    Scroll down to bottom of the page
                    click element    xpath://*[@id="dropdownMenuButton"]                                                                            #Select "Select group" button
    Wait until Element is Enabled    xpath:/html/body/div[1]/div/div/section[2]/section[5]/section[1]/div/section/div/div/div/div/ul/li[1]/span
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[5]/section[1]/div/section/div/div/div/div/ul/li[1]/span     #Select an existing group
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[6]/section/header/div/span[1]                               #Select existing user
       Element should be Disabled    xpath:/html/body/div[1]/div/div/footer/button[2]                                                               #Check if Save button is disabled
                   log to console    Unable to create group w/out Notification Level - PASSED

#Validation No.3: Create group w/out selecting "Notify when" - Compliance
                   log to console    Validation No.3: Create group w/out selecting "Notify when" - Compliance
                            sleep    2s
                       press keys    xpath:/html/body/div[1]/div/div/section[1]/header/div   scroll up                                              #Scroll up
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[3]/section/div[2]/i[1]                                      #Click Notify Level
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[4]/section/div/section[1]/i                                 #Unselect Notify when
                            sleep    2s
                       Press keys    xpath:/html/body/div[1]/div/div/footer/button[2]     Downwards Arrow                                           #Scroll down to Selected Retailer section
       Element should be Disabled    xpath:/html/body/div[1]/div/div/footer/button[2]                                                               #Check if Save button is disabled
                   log to console    Verified Save button is Disabled
                   log to console    Unable to Create group w/out selecting "Notify when" - PASSED

#Validation No.4: Create group w/out selecting "Notify from" - Compliance
                   log to console    Validation No.4: Create group w/out selecting "Notify from" - Compliance
                       Press keys    xpath:/html/body/div[1]/div/div/footer  Downwards Arrow                                                       #Scroll Down
                            sleep    2s
                    click element    xpath:/html/body/div[1]/div/div/section[1]/header/button                                                      #Click Back button to redirect to main page
    Wait until Element is Enabled    xpath:/html/body/div[1]/div/div/div/div[1]/div[1]/button
                    click element    xpath:/html/body/div[1]/div/div/div/div[1]/div[1]/button                                                      #Click Group menu
                    click element    xpath:/html/body/div[1]/div/div/div/div[1]/div[1]/div/button                                                  #Click Add new group
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[1]/input                                                   #Click title text box
                       Input Text    xpath:/html/body/div[1]/div/div/section[2]/section[1]/input     ${groupname}                                  #input title Test Group [QA]
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[2]/section/div[3]/i                                        #click type of group Comparisons
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[3]/section/div[2]/i[1]                                     #Select Notif Level
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[4]/section/div/section[1]/i                                #Select from Notify when
                   log to console    Selected Notification Level and Notify when
                       Press keys    xpath:/html/body/div[1]/div/div/footer  Downwards Arrow                                                       #Scroll down to Selected Retailer section
    Wait until Element is Enabled    xpath:/html/body/div[1]/div/div/section[2]/section[5]/div/section/div[2]/i
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[5]/div/section/div[2]/i                                    #Click Custom
                   log to console    Selected "Custom" but no retailer and brand selected
                       Press keys    xpath:/html/body/div[1]/div/div/footer/button[2]     Downwards Arrow                                          #Scroll down
       Element should be Disabled    xpath:/html/body/div[1]/div/div/footer/button[2]                                                              #Check if Save button is disabled
                   log to console    Save button is disabled if no website or product selected
                   log to console    Select 'Groups' button
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[5]/div/section/div[1]/i                                    #Click 'Groups' Radio button
       Element should be Disabled    xpath:/html/body/div[1]/div/div/footer/button[2]                                                              #Check if Save button is disabled
                   log to console    Save button is disabled if no website or product selected

#Validation No.5: Create group w/out selecting "whom to notify" - Compliance
                   log to console    Validation No.5: Create group w/out selecting "whom to notify" - Compliance
                    click element    xpath:/html/body/div[1]/div/div/footer/button[1]                                                              #Click Cancel button
    Wait until Element is Enabled    xpath:/html/body/div[1]/div/div/div/div[1]/div[1]/button
                    click element    xpath:/html/body/div[1]/div/div/div/div[1]/div[1]/button                                                      #Click Group menu
                    click element    xpath:/html/body/div[1]/div/div/div/div[1]/div[1]/div/button                                                  #Click Add new group
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[1]/input                                                   #Click title text box
                       Input Text    xpath:/html/body/div[1]/div/div/section[2]/section[1]/input     ${groupname}                                  #input title Test Group [QA]
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[2]/section/div[3]/i                                        #click type of group Compliance
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[3]/section/div[2]/i[1]                                     #Select Notif Level
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[4]/section/div/section[1]/i                                #Select from Notify when
                   log to console    Selected Notification Level and Notify when
               execute javascript     window.scrollTo(document.body.scrollHeight,0)
                    click element    xpath://*[@id="dropdownMenuButton"]                                                                            #Select dropdown button to select existing group
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[5]/section[1]/div/section/div/div/div/div/ul/li[1]          #Select existing group
        Element should be Enabled    xpath:/html/body/div[1]/div/div/footer/button[2]                                                               #Verify if Save button is enabled
                    click element    xpath:/html/body/div[1]/div/div/section[2]/section[6]/section/div/div[2]/div[2]/label/i                        #Unselect the email recipients
                   log to console     Unselected email recipients
       Element should be Disabled    xpath:/html/body/div[1]/div/div/footer/button[2]                                                               #Verify if Save button is Disabled
                   log to console     Cannot add group w/out selecting recipient

