*** Settings ***
Library  SeleniumLibrary

*** Variables ***
#Browser used for running. Change this when testing other browsers.
${browser}   chrome

#URL for testing: Stage/Production, change this when testing other environments.
${url}   https://app.detailonline.com/auth/#/

#Credentials for accessing the tool
${username}  admin@detailonline.tech
${password}  adminpassword
${GroupName}     [TEST-QA] Automation Rankings test group
${GroupName_2}     [TEST-QA] Automation Rankings test group: Selected
${GroupName_3}   [TEST-QA] Automation: Set as template group
${RetailerName}  amazon.co.uk
${RetailerName_2}  amazon.co.uk/3P
${BrandName}     Adidas

*** Test Cases ***
Rankings
#Select Browser, maximize window.
    log to console   Open Browser
    open browser     ${url}  ${browser}
    log to console   Maximize Browser Window
    maximize browser window
    sleep   5s
    set selenium speed  2s
    set selenium timeout  60s

#Test Cases
LoginToApplication
    #Login to Detail App
    #Input correct email address
    log to console   LoginToApplication - test case start execute
    log to console  Input correct username
    input text   xpath:/html/body/div/div[1]/form/div[1]/div/input   ${username}     #Input username
    #Input correct password
    input text   xpath:/html/body/div/div[1]/form/div[2]/div[1]/div/div/input    ${password}     #Input password
    log to console   Input correct password
    #Select login button
    click element  xpath:/html/body/div/div[1]/form/div[3]/span  #Select login button
    log to console   Select login button: Done
    log to console   LoginToApplication test execution DONE

SwitchCompany
    sleep   20s
    #Select user details: Upper right corner
    log to console   SwitchCompany test case start execute
    log to console   Select Account Details
    sleep    10s
    Click element   xpath:/html/body/header/div[3]/div/div/header    #Select account details on navbar
    #Select company selection
    sleep    5s
    log to console   Click company selection
    click element    xpath:/html/body/header/div[3]/div/div/div/section/div/header   #Click company
    #Input Demo Company
    log to console   Select input Demo Company
    input text  xpath:/html/body/header/div[3]/div/div/div/section/div/section/div[1]/input  Demo    #Input Demo
    sleep    5s
    #Select on white space to be able to switch company
    sleep    5s
    click element   xpath:/html/body/header/div[3]/div/div/div/section/div/section/div[2]/div/label/i    #Select whitespace for loading
    click element   xpath:/html/body/div[1]/div
    log to console   SwitchCompany test execution DONE

ModuleSelection
    sleep    15s
    #Select rankings module on navbar
    log to console   ModuleSelection test case start execute
    log to console   Select rankings module
    click element    xpath:/html/body/header/div[1]/nav/ul/li[4]/a   #Click rankings module
    log to console   ModuleSelection test case execution DONE

4.0 Rankings - be able to create a rankings group: All retailers and brands
    sleep    25s
    #Select add new group button
    log to console   4.0 Rankings - be able to create a rankings group: All retailers and brands - test case start execute
    Press Keys   xpath:/html/body/div[1]/div/div[1]    Downwards Arrow
    log to console   Select button for add new group
    click element    xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/button     #Select dropdown
    log to console   Select add new group button
    click element    xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div/button     #Click add new group button
   #Toggle private button
    sleep    30s
    log to console   Toggle private button
    click element    xpath:/html/body/div[1]/div/div/header/div[2]/button[1]
    #Input group name
    sleep    10s
    log to console   Input group name
    input text   xpath:/html/body/div[1]/div/div/section[1]/input    ${GroupName}    #Add group name
    #Select all retailers
    log to console   Select ALL retailers
    click element    xpath:/html/body/div[1]/div/div/section[2]/div/div[1]/div/label/i
    #Add keywords for your rankings group
    log to console   Select ALL keywords
    input text   xpath:/html/body/div[1]/div/div/section[3]/form/md-chips/md-chips-wrap/div/input  Shoes    #1st keyword
    Press Keys    xpath:/html/body/div[1]/div/div/section[3]/form/md-chips/md-chips-wrap    ENTER    #Enter keywords
    input text   xpath:/html/body/div[1]/div/div/section[3]/form/md-chips/md-chips-wrap/div/input  Adidas   #2nd keyword
    Press Keys    xpath:/html/body/div[1]/div/div/section[3]/form/md-chips/md-chips-wrap    ENTER    #Enter keywords
    #Select all brands
    sleep    5s
    log to console   Select All Brands
    click element  xpath:/html/body/div[1]/div/div/section[4]/div/div[1]/div/div/div/div     #Click Select ALL brands
    #Save Group
    sleep    3s
    log to console   Select Save button
    click element    xpath:/html/body/div[1]/div/div/header/div[2]/button[3]     #Click Save button
    log to console   4.0 Rankings - be able to create a rankings group: All retailers and brands - test case execution DONE

4.0 Rankings - be able to edit a rankings group
    sleep    15s
    #Select group selection setting
    log to console   4.0 Rankings - be able to edit a rankings group - test case start execute
    log to console   Select Group
    click element    xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/button     #Click group dropdown button
    #Search for group name (created group)
    log to console   Search for created group
    input text   xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div/input  ${GroupName}    #Input created group name
    #Select edit group details
    sleep    3s
    log to console   Select edit group button
    click element    xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[2]/div[2]/button[3]   #Click Edit button
    sleep    10s
    #Unselect retailers
    sleep    5s
    log to console   Unselect retailers
    click element    xpath:/html/body/div[1]/div/div/section[2]/div/div[2]/div/div[1]/label/i    #Unselect retailer 1
    click element    xpath:/html/body/div[1]/div/div/section[2]/div/div[2]/div/div[6]/label/i    #Unselect retailer 2
    #Unselect keywords
    sleep    5s
    log to console   Unselect keywords
    click element    xpath:/html/body/div[1]/div/div/section[3]/form/md-chips/md-chips-wrap/md-chip[2]/div[2]/button/md-icon     #Remove keyword
    #Unselect brands
    sleep    3s
    log to console   Unselect brands
    click element    xpath:/html/body/div[1]/div/div/section[4]/div/div[2]/div/div[1]/label/i
    click element    xpath:/html/body/div[1]/div/div/section[4]/div/div[2]/div/div[3]/label/i
    click element    xpath:/html/body/div[1]/div/div/section[4]/div/div[2]/div/div[4]/label/i
    click element    xpath:/html/body/div[1]/div/div/section[4]/div/div[2]/div/div[5]/label/i
    click element    xpath:/html/body/div[1]/div/div/section[4]/div/div[2]/div/div[6]/label/i
    click element    xpath:/html/body/div[1]/div/div/section[4]/div/div[2]/div/div[7]/label/i
    click element    xpath:/html/body/div[1]/div/div/section[4]/div/div[2]/div/div[8]/label/i
    click element    xpath:/html/body/div[1]/div/div/section[4]/div/div[2]/div/div[9]/label/i
    click element    xpath:/html/body/div[1]/div/div/section[4]/div/div[2]/div/div[10]/label/i
    click element    xpath:/html/body/div[1]/div/div/section[4]/div/div[2]/div/div[11]/label/i
    click element    xpath:/html/body/div[1]/div/div/section[4]/div/div[2]/div/div[12]/label/i
    click element    xpath:/html/body/div[1]/div/div/section[4]/div/div[2]/div/div[13]/label/i
    #Save Group
    sleep    3s
    log to console   Select SAVE button
    click element    xpath:/html/body/div[1]/div/div/footer/button[2]    #Click SAVE button
    log to console   4.0 Rankings - be able to edit a rankings group - test case execution DONE

4.0 Rankings - be able to create a rankings group: Selected retailers/brands
    sleep    15s
    #Select add new group button
    log to console   4.0 Rankings - be able to create a rankings group: Selected retailers/brands - test case execution start
    log to console   Select Add new group
    click element    xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/button     #Select group dropdown option
    click element    xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div/button     #Select add new group button
   #Toggle private button
    sleep    5s
    log to console   Toggle private button done
    click element    xpath:/html/body/div[1]/div/div/header/div[2]/button[1]    #Click private button
   #Input group name
    sleep    5s
    log to console   Add group name
    input text   xpath:/html/body/div[1]/div/div/section[1]/input    ${GroupName_2}  #Add group title
   #Select atleast 3 retailers
    sleep     2s
    log to console    Select 3 retailers
    click element     xpath:/html/body/div[1]/div/div/section[2]/div/div[1]/div/div/div[1]/ul/li[1]/label/i  #Select retailer 1
    click element     xpath:/html/body/div[1]/div/div/section[2]/div/div[1]/div/div/div[3]/ul/li[1]/label/i  #Select retailer 2
    click element     xpath:/html/body/div[1]/div/div/section[2]/div/div[1]/div/div/div[3]/ul/li[2]/label/i  #Select retailer 3
    #Add keywords for your rankings group
    log to console   Add keywords
    input text   xpath:/html/body/div[1]/div/div/section[3]/form/md-chips/md-chips-wrap/div/input  Shoes    #1st keyword
    Press Keys    xpath:/html/body/div[1]/div/div/section[3]/form/md-chips/md-chips-wrap    ENTER    #Enter keyword 1
    input text   xpath:/html/body/div[1]/div/div/section[3]/form/md-chips/md-chips-wrap/div/input  Adidas   #2nd keyword
    Press Keys    xpath:/html/body/div[1]/div/div/section[3]/form/md-chips/md-chips-wrap    ENTER    #Enter keyword 2
   #Select 3 brands
    sleep     2s
    log to console   Select 3 brands
    click element     xpath:/html/body/div[1]/div/div/section[4]/div/div[1]/div/div/ul/li[1]/label/i     #Select Brand 1
    click element     xpath:/html/body/div[1]/div/div/section[4]/div/div[1]/div/div/ul/li[2]/label/i     #Select Brand 2
    click element     xpath:/html/body/div[1]/div/div/section[4]/div/div[1]/div/div/ul/li[3]/label/i     #Select Brand 3
    #   Save Group
    sleep    3s
    log to console   Save Group details
    click element    xpath:/html/body/div[1]/div/div/header/div[2]/button[3]     #Click Add button
    log to console   4.0 Rankings - be able to create a rankings group: Selected retailers/brands - test case execution DONE

4.0 Rankings - verify that the rankings history filters works properly
    sleep    20s
    log to console   4.0 Rankings - verify that the rankings history filters works properly - test case execution START
    Press Keys   xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[2]    Downwards Arrow
    #Search for group created:
    log to console   Search for group created
    click element    xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/button
    input text   xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div/input  ${GroupName}
    sleep    3s
    click element    xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div/section/div[1]/div[1]
    sleep    10s
    #Select configure button
    log to console   Select configure button
    click element    xpath:/html/body/div[1]/div/div[3]/section/div[1]/div[1]/div[1]/div[1]/button   #Click configure button
    #Unselect all websites
    sleep    5s
    log to console   Unselect all websites
    click element    xpath://*[@id="hrankings-date-holder"]/section[1]/div[2]/div[1]/label/i     #Click unselect all websites
    click element    xpath:/html/body/div[1]/div/div[3]/section/div[1]/div[1]/div[1]/div[1]/button   #Click configure button
    #Select All websites
    sleep    5s
    log to console   Select configure button
    click element    xpath:/html/body/div[1]/div/div[3]/section/div[1]/div[1]/div[1]/div[1]/button   #Click configure button
    log to console   Select all websites
    click element    xpath://*[@id="hrankings-date-holder"]/section[1]/div[2]/div[1]/label/i     #Click select all websites
    log to console   Select configure button
    click element    xpath:/html/body/div[1]/div/div[3]/section/div[1]/div[1]/div[1]/div[1]/button  #Click configure button
    log to console   Select all websites DONE
    #Unselect ALL brands
    sleep    5s
    log to console   Select configure button
    click element    xpath:/html/body/div[1]/div/div[3]/section/div[1]/div[1]/div[1]/div[1]/button   #Select configure button
    log to console   Unselect all brands
    click element    xpath://*[@id="hrankings-date-holder"]/section[2]/div/div/div[3]/div[1]/label/i     #Click unselect ALL
    log to console   Select configure button
    click element    xpath:/html/body/div[1]/div/div[3]/section/div[1]/div[1]/div[1]/div[1]/button
    #Select ALL brands
    sleep    5s
    log to console   Select configure button
    click element    xpath:/html/body/div[1]/div/div[3]/section/div[1]/div[1]/div[1]/div[1]/button   #Select configure button
    log to console   Select All Brands
    click element    xpath://*[@id="hrankings-date-holder"]/section[2]/div/div/div[3]/div[1]/label/i     #Click Select ALL
    log to console   Select configure button
    click element    xpath:/html/body/div[1]/div/div[3]/section/div[1]/div[1]/div[1]/div[1]/button
    #Select month filters
    log to console   Select month filter
    Press Keys   xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[2]    Downwards Arrow
    click element    xpath:/html/body/div[1]/div/div[3]/section/div[1]/div[1]/div[2]/div[2]/div[2]   #Click Month filter
    sleep    5s
    #Select qtr filter
    log to console   Select qtr filter
    click element    xpath:/html/body/div[1]/div/div[3]/section/div[1]/div[1]/div[2]/div[2]/div[3]   #Click Qtr filter
    sleep    5s
    #Select yr filter
    log to console   Select yr filter
    click element    xpath:/html/body/div[1]/div/div[3]/section/div[1]/div[1]/div[2]/div[2]/div[4]   #Click yr filter
    sleep    5s
    log to console   4.0 Rankings - verify that the rankings history filters works properly - test case execution DONE

4.0 Rankings - verify product rankings rack filters are working properly
    sleep    20s
    #Select Top 25, 50
    log to console   4.0 Rankings - verify product rankings rack filters are working properly - test execution START
    log to console   Go to rankings rack filter section
    Press Keys  xpath:/html/body/div[1]/div/div[3]/section/div[2]/header   Downwards Arrow
    Press Keys  xpath:/html/body/div[1]/div/div[3]/section/div[2]/header   Downwards Arrow
    sleep    5s
    log to console   Select configure button
    click element    xpath:/html/body/div[1]/div/div[3]/section/div[2]/div[1]/div[1]/button  #Select configure button
    log to console   Select Top 25
    click element    xpath://*[@id="prankings-date-holder"]/section[1]/div/div[2]/label/i    #Select Top 25
    log to console   Re-click configure button
    click element    xpath:/html/body/div[1]/div/div[3]/section/div[2]/div[1]/div[1]/button  #Re-click configure button
    log to console   Scroll to bottom
    Press Keys     xpath:/html/body/div[1]/div/div[3]/div[2]  Downwards Arrow   #Scroll to bottom
    #Select Top 50
    sleep    20s
    log to console   Select configure button
    click element    xpath:/html/body/div[1]/div/div[3]/section/div[2]/div[1]/div[1]/button  #Click configure button
    log to console   Select Top 50
    click element    xpath://*[@id="prankings-date-holder"]/section[1]/div/div[3]/label/i    #Click Top 50
    log to console   Re-select configure button
    click element    xpath:/html/body/div[1]/div/div[3]/section/div[2]/div[1]/div[1]/button   #Re-click configure button
    log to console   Scroll to bottom
    Press Keys     xpath:/html/body/div[1]/div/div[3]/div[2]  Downwards Arrow   #Scroll to bottom
    #Select/Unselect keywords
    sleep    10s
    log to console   Select configure button
    click element    xpath:/html/body/div[1]/div/div[3]/section/div[2]/div[1]/div[1]/button  #Select configure button
    log to console   Unselect keywords
    click element    xpath://*[@id="prankings-date-holder"]/section[2]/div/div/div   #Unselect keywords
    log to console   Re-select configure button
    click element    xpath:/html/body/div[1]/div/div[3]/section/div[2]/div[1]/div[1]/button  #Select configure button
    sleep    3s
    click element    xpath:/html/body/div[1]/div/div[3]/section/div[2]/div[1]/div[1]/button  #Reselect configure button
    log to console   Select ALL keywords option
    click element    xpath://*[@id="prankings-date-holder"]/section[2]/div/div/div   #Reselect "Select All" keywords option
    click element    xpath:/html/body/div[1]/div/div[3]/section/div[2]/div[1]/div[1]/button  #Select configure button
    #Select retailer selection dropdown (corner right side)
    log to console   Select retailer dropdown
    click element    xpath:/html/body/div[1]/div/div[3]/section/div[2]/div[1]/div[2]/div    #Click retailer dropdown
    log to console   Search retailer
    input text   xpath:/html/body/div[1]/div/div[3]/section/div[2]/div[1]/div[2]/div/section/div/input   ${RetailerName_2}

4.0 Rankings - verify rankings download works
    sleep    20s
    log to console   Rankings download test case start execute
    log to console   Scroll to bottom
    Press Keys   xpath:/html/body/div[1]/div/div[3]/section/div[2]   Downwards Arrow    #Scroll Downwards
    Press Keys   xpath:/html/body/div[1]/div/div[3]/div[2]  Downwards Arrow   #Scroll to bottom
    log to console   Select download button
    click element    xpath:/html/body/div[1]/div/div[3]/div[2]/footer/div/button    #Click download button
    log to console   Select Apply button
    click element    xpath:/html/body/div[1]/div/div[3]/div[2]/footer/div/div/div/section[2]/footer/button[2]
    log to console   Rankings download test case execution Done

4.0 Rankings - Set as Template
    sleep    20s
    log to console   Set as template test case start execute
    Press Keys   xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[2]    Downwards Arrow
    log to console   Select group dropdown button
    click element    xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/button
    log to console   Search for group created
    input text   xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div/input  ${GroupName}
    log to console   Select group
    click element    xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div/section/div[1]/div[1]
    log to console   Click Edit Group button
    click element    xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[2]/div[2]/button[3]
    sleep    15s
    log to console   Scroll downwards
    Press Keys   xpath:/html/body/div[1]/div/div/footer   Downwards Arrow    #Scroll Downwards
    sleep    5s
    log to console   Select elipsis button
    click element    xpath:/html/body/div[1]/div/div/footer/div/button
    log to console   Select "Set as Template" button
    click element    xpath:/html/body/div[1]/div/div/footer/div/ul/li[1]
    sleep    3s
    #Check if save button is disabled if no group title
    log to console   Check if save button is disabled if no group title
    element should be disabled   xpath:/html/body/div[1]/div/div/header/div[2]/button[3]
    #Save Group
    log to console   Add group name
    input text   xpath:/html/body/div[1]/div/div/section[1]/input    ${GroupName_3}    #Add group name
    log to console   Save Group
    click element    xpath:/html/body/div[1]/div/div/footer/button[2]

4.0 Rankings - Delete group functionality
    sleep    30s
    log to console   Delete group functionality test case start execute
    log to console   Select group dropdown button
    click element    xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/button
    log to console   Search for group created
    #Delete 1st group created
    input text   xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div/input  ${GroupName}
    log to console   Select group
    click element    xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div/section/div[1]/div[1]
    log to console   Click Edit Group button
    click element    xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[2]/div[2]/button[3]
    sleep    20s
    log to console   Scroll downwards
    Press Keys   xpath:/html/body/div[1]/div/div/footer   Downwards Arrow    #Scroll Downwards
    sleep    5s
    log to console   Select elipsis button
    click element    xpath:/html/body/div[1]/div/div/footer/div/button
    log to console   Select Delete button
    click element    xpath:/html/body/div[1]/div/div/footer/div/ul/li[2]
    log to console   Select "Yes"
    click element    xpath:/html/body/popup/div/div/div/div[3]/button[2]

    #Delete 2nd group created
    sleep    30s
    log to console   Select group dropdown button
    click element    xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/button
    log to console   Search for group created
    input text   xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div/input  ${GroupName_2}
    log to console   Select group
    click element    xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div/section/div[1]/div[1]
    log to console   Click Edit Group button
    click element    xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[2]/div[2]/button[3]
    sleep    20s
    log to console   Scroll downwards
    Press Keys   xpath:/html/body/div[1]/div/div/footer   Downwards Arrow    #Scroll Downwards
    sleep    5s
    log to console   Select elipsis button
    click element    xpath:/html/body/div[1]/div/div/footer/div/button
    log to console   Select Delete button
    click element    xpath:/html/body/div[1]/div/div/footer/div/ul/li[2]
    log to console   Select "Yes"
    click element    xpath:/html/body/popup/div/div/div/div[3]/button[2]

    #Delete 3rd group created
    sleep    30s
    log to console   Select group dropdown button
    click element    xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/button
    log to console   Search for group created
    #Delete 1st group created
    input text   xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div/input  ${GroupName_3}
    log to console   Select group
    click element    xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div/section/div[1]/div[1]
    log to console   Click Edit Group button
    click element    xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[2]/div[2]/button[3]
    sleep    20s
    log to console   Scroll downwards
    Press Keys   xpath:/html/body/div[1]/div/div/footer   Downwards Arrow    #Scroll Downwards
    sleep    5s
    log to console   Select elipsis button
    click element    xpath:/html/body/div[1]/div/div/footer/div/button
    log to console   Select Delete button
    click element    xpath:/html/body/div[1]/div/div/footer/div/ul/li[2]
    log to console   Select "Yes"
    click element    xpath:/html/body/popup/div/div/div/div[3]/button[2]
    #Close browser after testing
    close browser