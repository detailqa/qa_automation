*** Settings ***
Library  SeleniumLibrary

*** Variables ***
#Browser used for running. Change this when testing other browsers.
${browser}   chrome

#URL for testing: Stage/Production, change this when testing other environments.
${url}   https://detail-ultima-staging.azurewebsites.net/auth/#/

#Credentials for accessing the tool
${username}  admin@detailonline.tech
${password}  adminpassword
${_GroupName}    [TEST-QA] rdgz All Products

*** Test Cases ***
LoginTest
     open browser    ${url}    ${browser}
     Maximize Browser Window
     LoginToApplication  #login to app
     SwitchCompany   #switch company
     ModuleSelection     #module TC
     5.0 Compliance - verify user should be able to create a Compliance group    #add details for group
     5.0 Compliance - verify configure filter works properly
     5.0 Compliance - verify that the download button works as intended
     5.0 Compliance - verify user should be able to edit a Compliance group
     5.0 Compliance - verify that created compliance groups can be deleted

*** Keywords ***
LoginToApplication
     sleep   2s
     input text  xpath:/html/body/div[1]/div[1]/form/div[1]/div[1]/input     ${username}
     #adds username
     log to console  Username done input
     sleep   1s
     input text  xpath:/html/body/div[1]/div[1]/form/div[2]/div[1]/div/div[1]/input  ${password}
     #adds password
     log to console  Password done input
     sleep   1s
     click element   xpath:/html/body/div[1]/div[1]/form/div[3]
     #click on login button
     log to console  click on login button done
     sleep  15 s
     log to console  LOGIN DONE

SwitchCompany
     click element   xpath:/html/body/header/div[3]/div/div/header
     #click on admin support dropdown
     log to console  clicked admin support dropdown
     sleep   2s
     click element   xpath:/html/body/header/div[3]/div/div/div/section/div/header
     #click on company list
     log to console  cicked on list of companies
     sleep   2s
     click element   xpath:/html/body/header/div[3]/div/div/div/section/div/section/div[2]/div[1]/label
     #select company demo
     log to console  selected demo company
     sleep   2s
     click element   xpath:/html/body/header/div[3]/div/div/header
     log to console  loading for demo company
     sleep   20s
     log to console  COMPANY SWITCH DONE

ModuleSelection
     click element   xpath:/html/body/header/div[1]/nav/ul/li[5]/a
     #selects compliance module
     log to console  selected compliance module
     sleep   3s
     log to console  SELECT MODULE DONE

5.0 Compliance - verify user should be able to create a Compliance group
     log to console  initialize ADD group
     click element   xpath:/html/body/div[1]/div/div[1]/div/div/div[1]/button/i
     #select group list dropdown
     log to console  click on compliance group dropdown
     click element   xpath:/html/body/div[1]/div/div[1]/div/div/div[1]/div/button
     #click on add new group
     log to console  clicked on add new group
     sleep   10s
     input text  xpath:/html/body/div[1]/div/div/section[1]/div/input    ${_GroupName}
     #input group name
     log to console  add group name [TEST-QA] all products
     sleep   5s
     click element   xpath:/html/body/div[1]/div/div/section[2]/div/div[1]/div/div/div[1]/div/div
     #select russia retailers
     log to console  click on first region group
     sleep   4s
     click element   xpath:/html/body/div[1]/div/div/section[2]/div/div[1]/div/div/div[2]/div/div
     #select south korea retailers
     log to console  click on second region group
     sleep   4s
     click element   xpath:/html/body/div[1]/div/div/section[2]/div/div[1]/div/div/div[3]/div/div
     #select UK retailers
     log to console  click on third region group
     sleep   4s
     click element   xpath:/html/body/div[1]/div/div/header/div[2]/button[1]/i
     #select all products
     log to console  select all products
     click element   xpath:/html/body/div[1]/div/div/header/div[2]/button[2]
     #click on private
     log to console  click on private toggle
     sleep   2s
     click element   xpath:/html/body/div[1]/div/div/section[3]/div/div[1]/div[2]/label/span
     #click on favorite
     log to console  click on favorite
     sleep   1s
     click element   xpath:/html/body/div[1]/div/div/footer/button[2]
     #click on save
     log to console  click on save button
     sleep   15s
     #click element   xpath:/html/body/div[1]/div/div[2]/div[3]/button
     #click on download button
     #log to console  click on compliance download button
     log to console  CREATE GROUP DONE
     execute javascript  window.scrollTo(document.body.scrollHeight,0)
     sleep  50s

5.0 Compliance - verify that the download button works as intended
     click element   xpath:/html/body/div[1]/div/div[2]/div[3]/button
     #click on download button
     log to console  click on compliance download button

5.0 Compliance - verify configure filter works properly
     #Deselect/No Results/ Blank Display
     sleep   5s
     click element   xpath:/html/body/div[1]/div/div[1]/section/section/header/div/div/div/button
     click element   xpath:/html/body/div[1]/div/div[1]/section/section/header/div/div/div/div/div/section[1]/div[2]/div[1]/label/i
     click element   xpath:/html/body/div[1]/div/div[1]/section/section/header/div/div/div/div/div/section[2]/div/div/label/i
     click element   xpath:/html/body/div[1]/div/div[1]/section/section/header/div/div/div/button
     log to console  DESLECT IN CONFIGURE DONE
     sleep  30s
     click element   xpath:/html/body/div[1]/div/div[1]/section/section/header/div/div/div/button
     click element   xpath:/html/body/div[1]/div/div[1]/section/section/header/div/div/div/div/div/section[1]/div[2]/div[4]/label/i
     click element   xpath:/html/body/div[1]/div/div[1]/section/section/header/div/div/div/div/div/section[2]/div/div/label/i
     click element   xpath:/html/body/div[1]/div/div[1]/section/section/header/div/div/div/div/div/section[2]/div/div/label/i
     sleep   2s
     click element   xpath:/html/body/div[1]/div/div[1]/section/section/header/div/div/div/div/div/section[2]/div/label[6]/span/span
     sleep   2s
     click element   xpath:/html/body/div[1]/div/div[1]/section/section/header/div/div/div/div/div/section[2]/div/label[1]/i
     sleep   2s
     click element   xpath:/html/body/div[1]/div/div[1]/section/section/header/div/div/div/button
     sleep  3s
     log to console  SELECTION DONE

5.0 Compliance - verify user should be able to edit a Compliance group
     log to console  initialize EDIT GROUP
     sleep   5s
     click element   xpath:/html/body/div[1]/div/div[1]/div/div/div[2]/div[2]/button[3]
     #click on edit button
     log to console  edit button clicked
     log to console  wait for page loading
     sleep   7s
     click element  xpath:/html/body/div[1]/div/div/section[2]/div/div[2]/div/div[1]/label
     #removed 1st retailer
     log to console  unselected 1st retailer
     sleep   1s
     click element  xpath:/html/body/div[1]/div/div/section[2]/div/div[2]/div/div[2]/label
     #removed 2nd retailer
     log to console  unselected 2nd retailer
     sleep   1s
     click element  xpath:/html/body/div[1]/div/div/section[2]/div/div[2]/div/div[5]/label
     #removed 3rd retailer
     log to console  unselected 3rd retailer
     sleep   1s
     click element   xpath:/html/body/div[1]/div/div/section[3]/div/div[2]/div/div[1]/label
     #remove 1st product
     log to console  unselect 1st product
     click element   xpath:/html/body/div[1]/div/div/section[3]/div/div[2]/div/div[1]/label
     #remove 2nd product
     log to console  uselect 2nd product
     click element   xpath:/html/body/div[1]/div/div/section[3]/div/div[2]/div/div[1]/label
     #remove 3rd product
     log to console  uselect 3rd product
     sleep   1s
     click element   xpath:/html/body/div[1]/div/div/footer/button[2]
     #click on save button
     log to console  done click on SAVE
     log to console  DONE EDIT GROUP

5.0 Compliance - verify that created compliance groups can be deleted
    sleep   5s
    click element   xpath:/html/body/div[1]/div/div[1]/div/div/div[2]/div[2]/button[3]
    sleep   7s
    click element   xpath:/html/body/div[1]/div/div/footer/div/button
    sleep   2s
    click element   xpath:/html/body/div[1]/div/div/footer/div/ul/li[2]
    sleep   2s
    click element   xpath:/html/body/popup/div/div/div/div[3]/button[2]
    sleep   5s
    log to console  DONE DELETE GROUP