*** Settings ***
Library  SeleniumLibrary

*** Variables ***
#Browser used for running. Change this when testing other browsers.
${browser}   chrome

#URL for testing: Stage/Production, change this when testing other environments.
${url}   https://app.detailonline.com/auth/#/
${gmail}   https://mail.google.com/

#Credentials for accessing the tool
${username}  admin@detailonline.tech
${password}  adminpassword

${invalidemail}     admin
${invalidusername}  admin@detailonline.com
${invalidpassword}  admin123password

${gmail-address}  Detail.Emea@gmail.com
${gmail-password}   emeaemail123
${Detail-NewPass}   adminpassword
*** Test Cases ***
Login
    9.0 Others - Gmail: Empty Inbox
    9.0 Others - Detail: Open App
    8.0 Login - Verify if user can login using correct credentials
    9.0 Others - Detail: Open App
    8.0 Login - Verify if user can login using incorrect credentials
    8.0 Login - Verify if user can login using correct email address/incorrect password
    8.0 Login - Verify if user can login using incorrect email address/correct password
    8.0 Login - Verify if forgot password functionality is working with "Remember Me" toggled
    8.0 Login - Verify if forgot password functionality is working with invalid address
    8.0 Login - Verify if forgot password functionality is working with incorrect confirm password
    9.0 Others - Gmail: Empty Inbox
    9.0 Others - Detail: Open App
    8.0 Login - Verify if forgot password functionality is working with valid email address and correct confirm password on password reset.
    9.0 Others - Gmail: Empty Inbox
    9.0 Others - Detail: Open App
    8.0 Login - Verify if forgot password functionality is working with valid email address & new password but incorrect new password upon login
*** Keywords ***
9.0 Others - Detail: Open App
    Log to console  9.0 Others - Detail: Open App
    Open browser     ${url}  ${browser}
    Maximize browser window
    set selenium speed  1s
    set selenium timeout  60s
    Sleep   5s
9.0 Others - Gmail: Empty Inbox
    Log to console  9.0 Others - Gmail: Empty Inbox
    Open browser    ${gmail}  ${browser}
    Maximize browser window
    Sleep   10s
    Log to console  Enter Email Address                                                                                 #Enter Email Address
    Input text  xpath://*[@id="identifierId"]   ${gmail-address}
    Click Element   xpath://*[@id="identifierNext"]/span
    Sleep   5s
    Log to console  Enter Password                                                                                      #Enter Password
    Input text  xpath://*[@id="password"]/div[1]/div/div[1]/input   ${gmail-password}
    Click Element   xpath://*[@id="passwordNext"]/span
    Sleep   5s
    Log to Console      Gmail: Empty Inbox                                                                              #Gmail: Empty Inbox
    Click element   xpath://*[@id=":2l"]/div[1]/span
    Sleep   2s
    Click element   xpath://*[@id=":4"]/div/div[1]/div[1]/div/div/div[2]/div[3]/div
    Close browser
8.0 Login - Verify if user can login using correct credentials
    Log to console  8.0 Login - Verify if user can login using correct credentials
    Log to console  Input correct email address                                                                         #Input correct email address
    Input text      xpath:/html/body/div/div[1]/form/div[1]/div/input   ${username}
    Log to console  Input correct password                                                                              #Input correct password
    Input text      xpath:/html/body/div/div[1]/form/div[2]/div[1]/div/div/input    ${password}
    Log to console   Select password seeker on login form                                                               #Select password seeker on login form
    Click element   xpath:/html/body/div/div[1]/form/div[2]/div[2]
    Click element   xpath:/html/body/div/div[1]/form/div[2]/div[2]
    Log to console   Select login button & Verify Successful Login                                                      #Select login button & Verify Successful Login
    Click element   xpath:/html/body/div/div[1]/form/div[3]
    Wait Until Element Is Visible   xpath:/html/body/header     60s
    Close browser
8.0 Login - Verify if user can login using incorrect credentials
    Log to console   8.0 Login - Verify if user can login using incorrect credentials
    Log to console  Input incorrect email address                                                                       #Input incorrect email address
    input text   xpath:/html/body/div/div[1]/form/div[1]/div/input   ${invalidusername}
    Log to console  Input incorrect password                                                                            #Input incorrect password
    input text   xpath:/html/body/div/div[1]/form/div[2]/div[1]/div/div/input   ${invalidpassword}
    Log to console   Select password seeker on login form                                                               #Select password seeker on login form
    Click element   xpath:/html/body/div/div[1]/form/div[2]/div[2]
    Click element   xpath:/html/body/div/div[1]/form/div[2]/div[2]
    Log to console   Select login button & Verify Error message                                                         #Select login button & Verify Error message
    Click element   xpath:/html/body/div/div[1]/form/div[3]
    Page should contain     Your email/password combination does not match.
    Sleep   3s
    Reload Page
    Sleep   5s
8.0 Login - Verify if user can login using correct email address/incorrect password
    Log to console   8.0 Login - Verify if user can login using correct email address/incorrect password
    Log to console  Input correct email address                                                                         #Input correct email address
    input text   xpath:/html/body/div/div[1]/form/div[1]/div/input   ${username}
    Log to console  Input incorrect password                                                                            #Input incorrect password
    input text   xpath:/html/body/div/div[1]/form/div[2]/div[1]/div/div/input   ${invalidpassword}
    Log to console   Select password seeker on login form                                                               #Select password seeker on login form
    Click element   xpath:/html/body/div/div[1]/form/div[2]/div[2]
    Click element   xpath:/html/body/div/div[1]/form/div[2]/div[2]
    Log to console   Select login button & Verify Error Message                                                         #Select login button & Verify Error Message
    Click element   xpath:/html/body/div/div[1]/form/div[3]
    Page should contain     Your email/password combination does not match.
    Sleep   3s
    Reload Page
    Sleep   5s
8.0 Login - Verify if user can login using incorrect email address/correct password
    Log to console   8.0 Login - Verify if user can login using incorrect email address/correct password
    Log to console  Input incorrect email address                                                                       #Input incorrect email address
    input text   xpath:/html/body/div/div[1]/form/div[1]/div/input   ${invalidusername}
    Log to console  Input incorrect password                                                                            #Input correct password
    input text   xpath:/html/body/div/div[1]/form/div[2]/div[1]/div/div/input   ${password}
    Log to console   Select password seeker on login form                                                               #Select password seeker on login form
    Click element   xpath:/html/body/div/div[1]/form/div[2]/div[2]
    Click element   xpath:/html/body/div/div[1]/form/div[2]/div[2]
    Sleep   3s
    Log to console   Select login button & Verify Error message                                                         #Select login button & Verify Error message
    Click element   xpath:/html/body/div/div[1]/form/div[3]
    Page should contain     Your email/password combination does not match.
    Sleep   3s
    Reload Page
    Sleep   5s
8.0 Login - Verify if forgot password functionality is working with "Remember Me" toggled
    Log to console  8.0 Login - Verify if forgot password functionality is working with "Remember Me" toggled
    Log to console  Input correct email address                                                                         #Input correct email address
    Input text  xpath:/html/body/div/div[1]/form/div[1]/div/input   ${username}
    Log to console  Input correct password                                                                              #Input correct password
    Input text  xpath:/html/body/div/div[1]/form/div[2]/div[1]/div/div/input    ${password}
    Log to console  Select "Remember Me" & log-in button                                                                #Select "Remember Me" & log-in button
    Click element   xpath:/html/body/div/div[1]/form/div[4]/label/icon
    Click element   xpath:/html/body/div/div[1]/form/div[3]
    Wait Until Element Is Visible   xpath:/html/body/header     60s
    Log to console  Log-Out account & Verify login credentials retain                                                   #Log-Out account & Verify login credentials retain
    Click element   xpath:/html/body/header/div[3]/div/div/header
    Sleep   3s
    Click element   xpath:/html/body/header/div[3]/div/div/div/section/div/section[1]/div[4]
    Sleep   3s
    Click element   xpath:/html/body/popup/div/div/div/div[3]/button[2]
    Sleep   5s
    Page should contain     ${username}
    Log to console  Unselect "Remember Me" & log-in button                                                              #Unselect "Remember Me" & log-in button
    Click element   xpath:/html/body/div/div[1]/form/div[4]/label/icon
    Click element   xpath:/html/body/div/div[1]/form/div[3]
    Wait Until Element Is Visible   xpath:/html/body/header     60s
    Log to console  Log-Out account & Verify login credentials did not retain                                           #Log-Out account & Verify login credentials did not retain
    Click element   xpath:/html/body/header/div[3]/div/div/header
    Sleep   3s
    Click element   xpath:/html/body/header/div[3]/div/div/div/section/div/section[1]/div[4]
    Sleep   3s
    Click element   xpath:/html/body/popup/div/div/div/div[3]/button[2]
    Sleep   5s
    Page should not contain     ${username}
8.0 Login - Verify if forgot password functionality is working with invalid address
    Log to console      8.0 Login - Verify if forgot password functionality is working with invalid address             #Click "Forgot password" functionality
    Log to console  Click "Forgot password" functionality
    Click element   xpath:/html/body/div/div[2]/span[2]/a
    Sleep   2s
    Log to console  Input invalid email address                                                                         #Input invalid email address
    Input text  xpath:/html/body/div/div/div[2]/div[1]/div/input    ${invalidemail}
    Log to console  Click "Reset Password" button & Verify Error message                                                #Click "Reset Password" button & Verify Error message
    Click element   xpath:/html/body/div/div/div[2]/div[2]/span
    Page should contain     E-mail is invalid.
    Click element   xpath:/html/body/div/div/div[2]/div[3]/div/a
    Sleep   3s
8.0 Login - Verify if forgot password functionality is working with incorrect confirm password
    Log to console      8.0 Login - Verify if forgot password functionality is working with incorrect confirm password
    Log to console  Click "Forgot password" functionality                                                               #Click "Forgot password" functionality
    Click element   xpath:/html/body/div/div[2]/span[2]/a
    Sleep   3s
    Log to console  Input valid email address                                                                           #Input valid email address
    Input text  xpath:/html/body/div/div/div[2]/div[1]/div/input    ${gmail-address}
    Log to console  Click "Reset Password" button                                                                       #Click "Reset Password" button
    Click element   xpath:/html/body/div/div/div[2]/div[2]/span
    Log to console      Open Gmail                                                                                      #Open Gmail
    Open browser     ${gmail}  ${browser}
    Sleep   5s
    Log to console  Enter Email Address                                                                                 #Enter Email Address
    Input text  xpath://*[@id="identifierId"]   ${gmail-address}
    Click Element   xpath://*[@id="identifierNext"]/span
    Sleep   5s
    Log to console  Enter Password                                                                                      #Enter Password
    Input text  xpath://*[@id="password"]/div[1]/div/div[1]/input   ${gmail-password}
    Click Element   xpath://*[@id="passwordNext"]/span
    Sleep   5s
    Log to console  Open Detail Invitation                                                                              #Open Detail Invitation
    Click Element   xpath://*[@id=":2q"]
    Sleep   3s
    Log to console  Click Reset password                                                                                #Click Reset password
    Click Element   xpath:/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div[2]/div/table/tr/td[1]/div[2]/div[2]/div/div[3]/div/div/div/div/div/div[1]/div[2]/div[3]/div[3]/div/div[2]/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[3]/td/table/tbody/tr/td[2]/a/span
    Sleep   3s
    Log to console  Enter New password & Toggle Password seeker feature                                                 #Enter New password & Toggle Password seeker feature
    Switch window   title=Detail
    Input text  xpath:/html/body/div/div/form/div[1]/div[1]/div/div/input   ${Detail-NewPass}
    Click element   xpath:/html/body/div/div/form/div[1]/div[2]
    Click element   xpath:/html/body/div/div/form/div[1]/div[2]
    Log to console  Enter incorrect Confirm password & Toggle Password seeker feature                                   #Enter incorrect Confirm password & Toggle Password seeker feature
    Input text  xpath:/html/body/div/div/form/div[2]/div[1]/div/div/input   ${gmail-password}
    Click element   xpath:/html/body/div/div/form/div[2]/div[2]
    Click element   xpath:/html/body/div/div/form/div[2]/div[2]
    Log to console  Click "Set Password" button & Verify Error message                                                  #Click "Set Password" button & Verify Error message
    Click element   xpath:/html/body/div/div/form/div[3]
    Page should contain     Password did not match.
    Sleep   3s
    Close All Browsers
8.0 Login - Verify if forgot password functionality is working with valid email address and correct confirm password on password reset.
    Log to console     8.0 Login - Verify if forgot password functionality is working with valid email address and correct confirm password on password reset.
    Log to console  Click "Forgot password" functionality                                                               #Click "Forgot password" functionality
    Click element   xpath:/html/body/div/div[2]/span[2]/a
    Sleep   3s
    Log to console  Input invalid email address                                                                         #Input invalid email address
    Input text  xpath:/html/body/div/div/div[2]/div[1]/div/input    ${gmail-address}
    Log to console  Click "Reset Password" button                                                                       #Click "Reset Password" button
    Click element   xpath:/html/body/div/div/div[2]/div[2]/span
    Sleep   3s
    Log to console      Open Gmail                                                                                      #Open Gmail
    Open browser     ${gmail}  ${browser}
    Sleep   5s
    Log to console  Enter Email Address                                                                                 #Enter Email Address
    Input text  xpath://*[@id="identifierId"]   ${gmail-address}
    Click Element   xpath://*[@id="identifierNext"]/span
    Sleep   5s
    Log to console  Enter Password                                                                                      #Enter Password
    Input text  xpath://*[@id="password"]/div[1]/div/div[1]/input   ${gmail-password}
    Click Element   xpath://*[@id="passwordNext"]/span
    Sleep   5s
    Log to console  Open Detail Invitation                                                                              #Open Detail Invitation
    Click Element   xpath://*[@id=":2q"]
    Sleep   3s
    Log to console  Click Reset password                                                                                #Click Reset password
    Click Element   xpath:/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div[2]/div/table/tr/td[1]/div[2]/div[2]/div/div[3]/div/div/div/div/div/div[1]/div[2]/div[3]/div[3]/div/div[2]/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[3]/td/table/tbody/tr/td[2]/a/span
    Sleep   3s
    Switch window   title=Detail
    Log to console  Enter New password & Toggle Password seeker feature                                                 #Enter New password & Toggle Password seeker feature
    Input text  xpath:/html/body/div/div/form/div[1]/div[1]/div/div/input   ${Detail-NewPass}
    Click element   xpath:/html/body/div/div/form/div[1]/div[2]
    Click element   xpath:/html/body/div/div/form/div[1]/div[2]
    Log to console  Confirm password & Toggle Password seeker feature                                                   #Enter incorrect Confirm password & Toggle Password seeker feature
    Input text  xpath:/html/body/div/div/form/div[2]/div[1]/div/div/input   ${Detail-NewPass}
    Click element   xpath:/html/body/div/div/form/div[2]/div[2]
    Click element   xpath:/html/body/div/div/form/div[2]/div[2]
    Log to console  Click Set Password                                                                                  #Click Set Password
    Click element   xpath:/html/body/div/div/form/div[3]
    Sleep   3s
    Close Browser
    Switch browser  2
    Log to console  Verify Login using new password                                                                     #Verify Login using new password
    Input text  xpath:/html/body/div/div[1]/form/div[2]/div[1]/div/div/input    ${detail-newpass}
    Click element   xpath:/html/body/div/div[1]/form/div[2]/div[2]
    Click element   xpath:/html/body/div/div[1]/form/div[2]/div[2]
    Click element   xpath:/html/body/div/div[1]/form/div[3]
    Wait until Element is visible   xpath:/html/body/header     60s
    Log to console  Log-out account                                                                                     #Log-out account
    Click element   xpath:/html/body/header/div[3]/div/div/header
    Sleep   2s
    Click element   xpath:/html/body/header/div[3]/div/div/div/section/div/section/div[4]
    Sleep   2s
    Click element   xpath:/html/body/popup/div/div/div/div[3]/button[2]
    Sleep   3s
    Close All Browsers
8.0 Login - Verify if forgot password functionality is working with valid email address & new password but incorrect new password upon login
    Log to console     8.0 Login - Verify if forgot password functionality is working with valid email address & new password but incorrect new password upon login
    Log to console  Click "Forgot password" functionality                                                               #Click "Forgot password" functionality
    Click element   xpath:/html/body/div/div[2]/span[2]/a
    Sleep   3s
    Log to console  Input invalid email address                                                                         #Input invalid email address
    Input text  xpath:/html/body/div/div/div[2]/div[1]/div/input    ${gmail-address}
    Log to console  Click "Reset Password" button                                                                       #Click "Reset Password" button
    Click element   xpath:/html/body/div/div/div[2]/div[2]/span
    Sleep   5s
    Log to console      Open Gmail                                                                                      #Open Gmail
    Open browser     ${gmail}  ${browser}
    Sleep   5s
    Log to console  Enter Email Address                                                                                 #Enter Email Address
    Input text  xpath://*[@id="identifierId"]   ${gmail-address}
    Click Element   xpath://*[@id="identifierNext"]/span
    Sleep   5s
    Log to console  Enter Password                                                                                      #Enter Password
    Input text  xpath://*[@id="password"]/div[1]/div/div[1]/input   ${gmail-password}
    Click Element   xpath://*[@id="passwordNext"]/span
    Sleep   5s
    Log to console  Open Detail Invitation                                                                              #Open Detail Invitation
    Click Element   xpath://*[@id=":2q"]
    Sleep   3s
    Log to console  Click Reset password                                                                                #Click Reset password
    Click Element   xpath:/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div[2]/div/table/tr/td[1]/div[2]/div[2]/div/div[3]/div/div/div/div/div/div[1]/div[2]/div[3]/div[3]/div/div[2]/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[3]/td/table/tbody/tr/td[2]/a/span
    Sleep   3s
    Log to console  Enter New password & Toggle Password seeker feature                                                 #Enter New password & Toggle Password seeker feature
    Switch window   title=Detail
    Input text  xpath:/html/body/div/div/form/div[1]/div[1]/div/div/input   ${Detail-NewPass}
    Click element   xpath:/html/body/div/div/form/div[1]/div[2]
    Click element   xpath:/html/body/div/div/form/div[1]/div[2]
    Log to console  Confirm password & Toggle Password seeker feature                                                   #Enter incorrect Confirm password & Toggle Password seeker feature
    Input text  xpath:/html/body/div/div/form/div[2]/div[1]/div/div/input   ${Detail-NewPass}
    Click element   xpath:/html/body/div/div/form/div[2]/div[2]
    Click element   xpath:/html/body/div/div/form/div[2]/div[2]
    Log to console  Click Set Password                                                                                  #Click Set Password
    Click element   xpath:/html/body/div/div/form/div[3]
    Sleep   3s
    Close Browser
    Switch browser  2
    Log to console  Verify Login using incorrect password                                                               #Verify Login using incorrect password & Verify Error message
    Input text  xpath:/html/body/div/div[1]/form/div[2]/div[1]/div/div/input    ${gmail-password}
    Click element   xpath:/html/body/div/div[1]/form/div[2]/div[2]
    Click element   xpath:/html/body/div/div[1]/form/div[2]/div[2]
    Click element   xpath:/html/body/div/div[1]/form/div[3]
    Page should contain     Your email/password combination does not match.
    Sleep   3s
    Close browser