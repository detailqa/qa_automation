*** Settings ***
Library  SeleniumLibrary

*** Variables ***
#Browser used for running. Change this when testing other browsers.
${browser}   chrome

#URL for testing: Stage/Production, change this when testing other environments.
${url}   https://app.detailonline.com/auth/#/
${gmail}   https://mail.google.com/

#Credentials for accessing the tool
${username}  admin@detailonline.tech
${password}  adminpassword

${UserFName}    Detail
${UserLName}    US
${UserEmail}    detail.USemail@gmail.com
${UserPass}     usemail123
${NewPass}  adminpassword

${InvalidEmail}     jkl.com
*** Test Cases ***
User Management
    9.0 Others - Gmail: Empty Inbox
    8.0 Login - Verify if Admin can login using correct credentials
    8.0 Switch Company - US Demo
    8.0 User management - Access User management page
    8.0 User management - Verify Filter functionalities
    8.0 User management - Verify Cancel option in Add User feature
    8.0 User management - Verify Invite option in Add User feature
    9.0 Others - Open Gmail
    8.0 Update Profile - Verify Cancel option in Update profile page
    8.0 Update Profile - Verify Update feature in Update profile page
    8.0 Change password - Verify NO option in Change password panel
    8.0 Change password - Verify YES option in Change password panel
    8.0 Change password - Verify Newly set Password in Login Page
    8.0 Update Profle - Verify Remove Account feature in Update profile page
*** Keywords ***
9.0 Others - Gmail: Empty Inbox
    Log to console  9.0 Others - Gmail: Empty Inbox
    Open browser    ${gmail}  ${browser}
    Maximize browser window
    Sleep   10s
    #Enter Email Address
    Log to console  Enter Email Address
    Input text  xpath://*[@id="identifierId"]   ${UserEmail}
    Sleep   3s
    Click Element   xpath://*[@id="identifierNext"]/span
    Sleep   5s
    #Enter Password
    Log to console  Enter Password
    Input text  xpath://*[@id="password"]/div[1]/div/div[1]/input   ${UserPass}
    Sleep   3s
    Click Element   xpath://*[@id="passwordNext"]/span
    Sleep   5s
    #Gmail: Empty Inbox
    Log to Console      Gmail: Empty Inbox
    Click element   xpath://*[@id=":2l"]/div[1]/span
    Sleep   2s
    Click element   xpath://*[@id=":4"]/div/div[1]/div[1]/div/div/div[2]/div[3]/div
    Sleep   5s
    Close browser
8.0 Login - Verify if Admin can login using correct credentials
    Open browser     ${url}  ${browser}
    Maximize browser window
    Sleep   5s
    Log to console  8.0 Login - Verify if Admin can login using correct credentials
    #Input correct email address
    Log to console  Input correct email address
    Input text  xpath:/html/body/div/div[1]/form/div[1]/div/input   ${username}
    Sleep   3s
    #Input correct password
    Log to console  Input correct password
    Input text  xpath:/html/body/div/div[1]/form/div[2]/div[1]/div/div/input    ${password}
    Sleep   3s
    #Select login button
    Log to console   Select login button
    Click element   xpath:/html/body/div/div[1]/form/div[3]
    Sleep   10s
8.0 Switch Company - US Demo
    Log to console  8.0 Switch Company - US Demo
    Wait Until Element Is Visible   xpath:/html/body/header     60s
    #Click Acct. Dashboard
    Log to console  Click Acct. Dashboard
    Click element   xpath:/html/body/header/div[3]/div/div/header
    Sleep   5s
    #Click Company list
    Log to console  Click Company list
    Click element   xpath:/html/body/header/div[3]/div/div/div/section/div/header
    Sleep   3s
    #Click US Demo
    Log to console  Click US Demo
    Click element   xpath:/html/body/header/div[3]/div/div/div/section/div/section/div[2]/div[5]/label/i
    Sleep   3s
    Click element   xpath:/html/body
    Sleep   10s
    Page should contain     US Demo
8.0 User management - Access User management page
    Log to console  8.0 User management - Access User management page
    #Click Acct. Dashboard
    Log to console  Click Acct. Dashboard
    Click element   xpath:/html/body/header/div[3]/div/div/header
    Sleep   5s
    #Load User Management page
    Log to console  Load User Management page
    Click element   xpath:/html/body/header/div[3]/div/div/div/section/div/div/div[1]
    Sleep   5s
8.0 User management - Verify Filter functionalities
    Log to console  8.0 User management - Verify Filter user field functionalities
    #Filter text Present in User management page
    Log to console  Filter text Present in User management page
    Input text  xpath:/html/body/div[1]/div/div/section[1]/input    ${username}
    Sleep   3s
    Page should contain     ${username}
    Sleep   3s
    Clear Element Text  xpath:/html/body/div[1]/div/div/section[1]/input
    Sleep   3s
    #Filter text that is not Present in User management page
    Log to console  Filter text that is not Present in User management page
    Input text  xpath:/html/body/div[1]/div/div/section[1]/input    abcd
    Sleep   3s
    Page should contain     No Users Found
    Sleep   3s
    Clear Element Text  xpath:/html/body/div[1]/div/div/section[1]/input
8.0 User management - Verify Cancel option in Add User feature
    Log to console  8.0 User management - Verify Cancel option in Add User feature
    #Click Cancel button
    Log to console  Click Cancel button
    Click element   xpath:/html/body/div[1]/div/div/section[1]/button
    Sleep   3s
    #Verify Cancel button redirects to main User management page
    Log to console  Verify Cancel button redirects to main User management page
    Click element   xpath:/html/body/div[1]/div/div/section[2]/footer/button[1]
    Sleep   3s
    Page should contain     Users
    Sleep   3s
8.0 User management - Verify Invite option in Add User feature
    Log to console  8.0 User management - Verify Invite option in Add User feature
    #Click Add User button
    Log to console  Click Add User button
    Click element   xpath:/html/body/div[1]/div/div/section[1]/button
    Sleep   3s
    #Enter User First name
    Log to console  Enter User First name
    Input text  xpath:/html/body/div[1]/div/div/section[1]/section[1]/div[1]/div[1]/input   ${UserFName}
    Sleep   3s
    #Enter User Last name
    Log to console  Enter User Last name
    Input text  xpath:/html/body/div[1]/div/div/section[1]/section[1]/div[1]/div[2]/input   ${UserLName}
    Sleep   3s
    #Enter Invalid Email Address format
    Log to console  Enter Invalid Email Address format
    Input text  xpath:/html/body/div[1]/div/div/section[1]/section[1]/div[2]/div[1]/input   ${InvalidEmail}
    Sleep   3s
    #Confirm Invalid email address format
    Log to console  Confirm Invalid email address format
    Input text  xpath:/html/body/div[1]/div/div/section[1]/section[1]/div[2]/div[2]/input   ${InvalidEmail}
    Sleep   3s
    #Verify Invite button is disabled
    Log to console  Verify Invite button is disabled
    Element should be disabled  xpath:/html/body/div[1]/div/div/section[2]/footer/button[2]
    Sleep   3s
    #Enter Valid Email Address format
    Log to console  Enter Valid Email Address format
    Clear element text  xpath:/html/body/div[1]/div/div/section[1]/section[1]/div[2]/div[1]/input
    Clear element text  xpath:/html/body/div[1]/div/div/section[1]/section[1]/div[2]/div[2]/input
    Sleep   3s
    Input text  xpath:/html/body/div[1]/div/div/section[1]/section[1]/div[2]/div[1]/input   ${UserEmail}
    Sleep   3s
    #Confirm Valid email address format
    Log to console  Confirm Valid email address format
    Input text  xpath:/html/body/div[1]/div/div/section[1]/section[1]/div[2]/div[2]/input   ${UserEmail}
    Sleep   3s
    #Click Invite button
    Log to console  Click Invite button
    Click element   xpath:/html/body/div[1]/div/div/section[2]/footer/button[2]
    Sleep   10s
9.0 Others - Open Gmail
    Log to console  9.0 Others - Open gmail
    Open browser    ${gmail}  ${browser}
    Maximize browser window
    Sleep   10s
    #Enter Email Address
    Log to console  Enter Email Address
    Input text  xpath://*[@id="identifierId"]   ${UserEmail}
    Sleep   3s
    Click Element   xpath://*[@id="identifierNext"]/span
    Sleep   5s
    #Enter Password
    Log to console  Enter Password
    Input text  xpath://*[@id="password"]/div[1]/div/div[1]/input   ${UserPass}
    Sleep   3s
    Click Element   xpath://*[@id="passwordNext"]/span
    Sleep   5s
    #Open Detail Invitation
    Log to console  Open Detail Invitation
    Click Element   xpath://*[@id=":2q"]
    Sleep   5s
    #Click Set new password
    Log to console  Click Set new password
    Click Element   xpath:/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div[2]/div/table/tr/td[1]/div[2]/div[2]/div/div[3]/div/div/div/div/div/div[1]/div[2]/div[3]/div[3]/div/div[1]/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[3]/td/table/tbody/tr/td[2]/a/span
    Sleep   3s
    #Enter New password & Toggle Password seeker feature
    Log to console  Enter New password & Toggle Password seeker feature
    Switch window   title=Detail
    Sleep   3s
    Input text  xpath:/html/body/div/div/form/div[1]/div[1]/div/div/input   ${UserPass}
    Click element   xpath:/html/body/div/div/form/div[1]/div[2]
    Sleep   3s
    Click element   xpath:/html/body/div/div/form/div[1]/div[2]
    Sleep   3s
    #Confirm password & Toggle Password seeker feature
    Log to console  Confirm password & Toggle Password seeker feature
    Input text  xpath:/html/body/div/div/form/div[2]/div[1]/div/div/input   ${UserPass}
    Click element   xpath:/html/body/div/div/form/div[2]/div[2]
    Sleep   3s
    Click element   xpath:/html/body/div/div/form/div[2]/div[2]
    Sleep   3s
    #Click Set Password
    Log to console  Click Set Password
    Click element   xpath:/html/body/div/div/form/div[3]
    Sleep   5s
    #Click Agree to terms and Conditions
    Log to console  Click Agree to terms and Conditions
    Click element   xpath:/html/body/div/section/section[1]/div/div/div/label/icon
    Element should be enabled   xpath:/html/body/div/section/section[1]/div/div/div/label/icon
    Sleep   2s
    Click element   xpath:/html/body/div/section/section[3]/button
8.0 Update Profile - Verify Cancel option in Update profile page
    Log to console  8.0 Update Profile - Verify Cancel option in Update profile page feature
    Wait Until Element Is Visible   xpath:/html/body/header     60s
    #Click Acct. Dashboard
    Log to console  Click Acct. Dashboard
    Click element   xpath:/html/body/header/div[3]/div/div/header
    Sleep   5s
    #Load Update profile page
    Log to console  Load Update profile page
    Click element   xpath:/html/body/header/div[3]/div/div/div/section/div/section/div[1]
    Sleep   5s
    #Click Cancel button
    Log to console  Click Cancel button
    Click element   xpath:/html/body/div[1]/div/div/section[3]/footer/button[1]
    Sleep   5s
    #Verify Cancel button redirects to Overview page
    Log to console  Verify Cancel button redirects to Overview page
    Page should contain     Overview
    Sleep   3s
8.0 Update Profile - Verify Update feature in Update profile page
    Log to console  8.0 Update Profile - Verify Update feature in Update profile page
    Wait Until Element Is Visible   xpath:/html/body/header     60s
    #Click Acct. Dashboard
    Log to console  Click Acct. Dashboard
    Click element   xpath:/html/body/header/div[3]/div/div/header
    Sleep   5s
    #Load Update profile page
    Log to console  Load Update profile page
    Click element   xpath:/html/body/header/div[3]/div/div/div/section/div/section/div[1]
    Sleep   5s
    #Update First Name
    Log to console  Update First Name
    Clear element text  xpath:/html/body/div[1]/div/div/section[1]/section[1]/div[1]/div[1]/input
    Sleep   2s
    Input text  xpath:/html/body/div[1]/div/div/section[1]/section[1]/div[1]/div[1]/input   ${userlname}
    Sleep   3s
    #Click Save button
    Log to console  Click Save button
    Click element   xpath:/html/body/div[1]/div/div/section[3]/footer/button[2]
    Sleep   2s
    #Verify Update
    Log to console  Verify Update
    Page should contain     Your changes have been saved successfully.
    Page should contain     ${userlname} ${userlname}
    Sleep   3s
    #Update Last Name
    Log to console  Update Last Name
    Clear element text  xpath:/html/body/div[1]/div/div/section[1]/section[1]/div[1]/div[2]/input
    Sleep   2s
    Input text  xpath:/html/body/div[1]/div/div/section[1]/section[1]/div[1]/div[2]/input   ${userfname}
    Sleep   3s
    #Click Save button
    Log to console  Click Save button
    Click element   xpath:/html/body/div[1]/div/div/section[3]/footer/button[2]
    Sleep   2s
    #Verify Update
    Log to console  Verify Update
    Page should contain     Your changes have been saved successfully.
    Page should contain     ${userlname} ${userfname}
    Sleep   5s
8.0 Change password - Verify NO option in Change password panel
    Log to console  8.0 Change password - Verify NO option in Change password panel
    #Click Acct. Dashboard
    Log to console  Click Acct. Dashboard
    Click element   xpath:/html/body/header/div[3]/div/div/header
    Sleep   5s
    #Click Change password and Verify Panel pop-up is visible
    Log to console  Click Change password and Verify Panel pop-up is visible
    Click element   xpath:/html/body/header/div[3]/div/div/div/section/div/section/div[3]
    Sleep   2s
    Page should contain     Confirm
    Sleep   2s
    #Click No and Verify Panel pop-up is not visible
    Log to console  Click No and Verify Panel pop-up is not visible
    Click element   xpath:/html/body/popup/div/div/div/div[3]/button[1]
    Sleep   2s
    Page should not contain     Confirm
    Sleep   2s
8.0 Change password - Verify YES option in Change password panel
    Log to console  8.0 Change password - Verify YES option in Change password panel
    #Click Acct. Dashboard
    Log to console  Click Acct. Dashboard
    Click element   xpath:/html/body/header/div[3]/div/div/header
    Sleep   5s
    #Click Change password and Verify Panel pop-up is visible
    Log to console  Click Change password and Verify Panel pop-up is visible
    Click element   xpath:/html/body/header/div[3]/div/div/div/section/div/section/div[3]
    Sleep   2s
    Page should contain     Confirm
    Sleep   2s
    #Verify YES option is disabled if both passwords typed do not match
    Log to console  Verify YES option is disabled if both passwords typed do not match
    Input text   xpath:/html/body/popup/div/div/div/div[2]/div[1]/input  ${NewPass}
    Sleep   2s
    Click element   xpath:/html/body/popup/div/div/div/div[2]/div[1]/div
    Sleep   2s
    Click element   xpath:/html/body/popup/div/div/div/div[2]/div[1]/div
    Sleep   2s
    Input text   xpath:/html/body/popup/div/div/div/div[2]/div[2]/input  ${UserPass}
    Sleep   2s
    Click element   xpath:/html/body/popup/div/div/div/div[2]/div[2]/div
    Sleep   2s
    Click element   xpath:/html/body/popup/div/div/div/div[2]/div[2]/div
    Sleep   2s
    Element should be disabled  xpath:/html/body/popup/div/div/div/div[3]/button[2]
    Sleep   3s
    #Verify YES option is enabled if both passwords typed matched
    Log to console  Verify YES option is enabled if both passwords typed matched
    Input text   xpath:/html/body/popup/div/div/div/div[2]/div[1]/input  ${NewPass}
    Sleep   2s
    Click element   xpath:/html/body/popup/div/div/div/div[2]/div[1]/div
    Sleep   2s
    Click element   xpath:/html/body/popup/div/div/div/div[2]/div[1]/div
    Sleep   2s
    Input text   xpath:/html/body/popup/div/div/div/div[2]/div[2]/input  ${NewPass}
    Sleep   2s
    Click element   xpath:/html/body/popup/div/div/div/div[2]/div[2]/div
    Sleep   2s
    Click element   xpath:/html/body/popup/div/div/div/div[2]/div[2]/div
    Sleep   2s
    Element should be enabled   xpath:/html/body/popup/div/div/div/div[3]/button[2]
    Sleep   3s
    #Click YES and Verify Redirect to Login page
    Log to console  Click YES and Verify Redirect to Login page
    Click element   xpath:/html/body/popup/div/div/div/div[3]/button[2]
    Sleep   5s
8.0 Change password - Verify Newly set Password in Login Page
    Log to console  8.0 Change password - Verify Newly set Password in Login Page
    Wait Until Element is Visible   xpath:/html/body/div/div[1]/form/div[1]/div/input     60s
    #Input correct email address
    Log to console  Input correct email address
    Input text  xpath:/html/body/div/div[1]/form/div[1]/div/input   ${UserEmail}
    Sleep   3s
    #Input correct password
    Log to console  Input correct password
    Input text  xpath:/html/body/div/div[1]/form/div[2]/div[1]/div/div/input    ${NewPass}
    Sleep   3s
    #Select login button
    Log to console   Select login button
    Click element   xpath:/html/body/div/div[1]/form/div[3]
    Sleep   10s
8.0 Update Profle - Verify Remove Account feature in Update profile page
    Log to console  8.0 Update Profle - Verify Remove Account feature in Update profile page
    Wait Until Element Is Visible   xpath:/html/body/header     60s
    #Click Acct. Dashboard
    Log to console  Click Acct. Dashboard
    Click element   xpath:/html/body/header/div[3]/div/div/header
    Sleep   5s
    #Load Update profile page
    Log to console  Load Update profile page
    Click element   xpath:/html/body/header/div[3]/div/div/div/section/div/section/div[1]
    Sleep   5s
    #Click Remove Account
    Log to Console  Click Remove Account
    Click element   xpath:/html/body/div[1]/div/div/section[3]/div/button
    Sleep   2s
    #Fill in Password and Toggle Password Seeker
    Log to Console  Fill in Password and Toggle Password Seeker
    Input text  xpath:/html/body/popup/div/div/div/div[2]/div[1]/input  ${NewPass}
    Sleep   2s
    Click element   xpath:/html/body/popup/div/div/div/div[2]/div[1]/div
    Sleep   2s
    Click element   xpath:/html/body/popup/div/div/div/div[2]/div[1]/div
    Sleep   2s
    Input text  xpath:/html/body/popup/div/div/div/div[2]/div[2]/input  ${NewPass}
    Sleep   2s
    Click element   xpath:/html/body/popup/div/div/div/div[2]/div[2]/div
    Sleep   2s
    Click element   xpath:/html/body/popup/div/div/div/div[2]/div[2]/div
    Sleep   2s
    #Click Remove button
    Log to Console  Click Remove button
    Click element   xpath:/html/body/popup/div/div/div/div[3]/button[2]
    Sleep   10s
