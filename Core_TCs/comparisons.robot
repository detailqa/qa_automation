*** Settings ***
Library  SeleniumLibrary

*** Variables ***
#Browsers used for running. Change this when testing other browsers.
${browser}   chrome
#${browser}  mozilla firefox

#URL for testing: Stage/Production, change this when testing other environments.
${url}   https://detail-ultima-staging.azurewebsites.net/    #staging link
#${url}   https://app.detailonline.com    #production link


#Credentials for accessing the tool
${username}  admin@detailonline.tech
${password}  adminpassword

#Credentials for accessing the tool
${groupname}     [QA Test Group1]
${groupname2}    [QA Test Group2]


*** Test Cases ***

3.0 Comparisons

#Settings
    open browser     ${url}  ${browser}
    maximize browser window
    set selenium speed  1s
    set selenium timeout  120s

#Test Case Scenarios
    LoginToDetail
    SwitchCompany
    3.0 Comparisons - Proceed to Comparisons Page
    3.0 Comparisons - check the pricing comparisons graph display
    3.0 Comparisons - validate that the comparisons pricing trends graph filters are working
    3.0 Comparisons - validate that product comparisons graph are working
    3.0 Comparisons - validate and check the data shown in the Price VS SRP table
    3.0 Comparisons - be able to create a comparisons group
    3.0 Comparisons - Set group as Template feature and create group
    3.0 Comparisons - be able to edit a comparison group
    3.0 Comparisons - be able to delete comparison group


*** Keywords ***
LoginToDetail
    log to console   Initialize login to Application......
        input text   xpath:/html/body/div/div[1]/form/div[1]/div/input   ${username}                                                                    # this line inputs a username
        input text   xpath:/html/body/div/div[1]/form/div[2]/div[1]/div/div/input    ${password}                                                        # this line inputs a password
     click element   xpath:/html/body/div/div[1]/form/div[3]/span                                                                                       # click Login button to officially login to detail
    log to console   Successfully login to application.....

SwitchCompany
#Switch to Demo Company --
                   log to console    Initialize switch to another company (DEMO)
    wait until element is visible    xpath:/html/body/header/div[3]/div/div/header/div/div[2]
                    click element    xpath:/html/body/header/div[3]/div/div/header/div/div[2]                                                           # this line clicks Admin Panel
                    click element    xpath:/html/body/header/div[3]/div/div/div/section/div/header                                                      # this line clicks existing company
                    click element    xpath:/html/body/header/div[3]/div/div/div/section/div/section/div[2]/div[1]/label/span                            # this line selects another company
                    click element    xpath:/html/body/div[1]/div                                                                                        # this line clicks element from the page
                   log to console    Successfully accessed company: DEMO

3.0 Comparisons - Proceed to Comparisons Page
    wait until element is visible    xpath:/html/body/header/div[1]/nav/ul/li[3]/a
                    click element    xpath:/html/body/header/div[1]/nav/ul/li[3]/a                                                                      # this line clicks Comparisons Page


3.0 Comparisons - check the pricing comparisons graph display
    wait until element is visible    xpath:/html/body/div[1]/div/div[3]/section/div[1]                                                                  # this line validates if pricing trends graph is visible
                    click element    xpath:/html/body/div[1]/div/div[1]/section/div[1]/header/div[2]/div[2]/button                                      # click Settings icon in the racks
                   log to console    validate if buttons inside the racks are enabled
        element should be enabled    xpath:/html/body/div[1]/div/div[1]/section/div[1]/header/div[2]/div[2]/div/div[1]/div/div[1]/div[1]                # this line verifies dropdown selection
        element should be enabled    xpath:/html/body/div[1]/div/div[1]/section/div[1]/header/div[2]/div[2]/div/div[1]/div/div[1]/div[2]/span           # this line verifies View button
        element should be enabled    xpath:/html/body/div[1]/div/div[1]/section/div[1]/header/div[2]/div[2]/div/header[2]                               # this line verifies All Brands
        element should be enabled    xpath:/html/body/div[1]/div/div[1]/section/div[1]/header/div[2]/div[2]/div/div[2]/div[1]/label                     # this line verifies My Brands
        element should be enabled    xpath:/html/body/div[1]/div/div[1]/section/div[1]/header/div[2]/div[2]/div/div[2]/div[3]/label/span                # this line verifies Competitor Brands
        element should be enabled    xpath:/html/body/div[1]/div/div[1]/section/div[1]/header/div[2]/div[2]/div/div[3]/div[1]                           # this line verifies da
        element should be enabled    xpath:/html/body/div[1]/div/div[1]/section/div[1]/header/div[2]/div[2]/div/div[3]/div[2]                           # this line verifies wk
        element should be enabled    xpath:/html/body/div[1]/div/div[1]/section/div[1]/header/div[2]/div[2]/div/div[3]/div[3]                           # this line verifies mo
        element should be enabled    xpath:/html/body/div[1]/div/div[1]/section/div[1]/header/div[2]/div[2]/div/div[3]/div[4]                           # this line verifies qtr
        element should be enabled    xpath:/html/body/div[1]/div/div[1]/section/div[1]/header/div[2]/div[2]/div/div[3]/div[5]                           # this line verifies yr
                   log to console    Verified: all elements are enabled
                    click element    xpath:/html/body/div[1]/div/div[1]/section/div[1]/header/div[2]/div[2]/button                                      # this line closes the racks
                   log to console    hover the price increase indicator
                       mouse over    xpath:/html/body/div[1]/div/div[1]/section/div[2]/section/div[1]/div[1]/div[2]                                     # this line clicks price changes
                            sleep    3s
                   log to console    hover the price decrease indicator
                       mouse over    xpath:/html/body/div[1]/div/div[1]/section/div[2]/section/div[1]/div[3]/div[2]                                     # this line clicks price changes
                            sleep    3s

3.0 Comparisons - validate that the comparisons pricing trends graph filters are working
    sleep    3s
#Step No.1: select a group in the comparisons module; click on the dropdown for comparisons
                   log to console    Step No.1: select a group in the comparisons module; click on the dropdown for comparisons
                    click element    xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/button                                                        # initialize selection of group
                   log to console    Search a group
                    click element    xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div/input
                       input text    xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div/input  Android Phone
                   log to console    Select a group
                    click element    xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div/section/div                                               # this line selects a Comparisons group
                            sleep    3s
    wait until element is visible    xpath:/html/body/div[1]/div/div[3]/section/div[1]
                   log to console    Successfully selected a Comparison group
                   log to console    Step No.2: check the configure filter by selecting and deselecting data
                       Press keys    xpath:/html/body/div[1]/div/div[3]/section/div[1]   Downwards Arrow                                                # this line scroll the page down
                   log to console    Click Configure button
                            sleep    2s
                    click element    xpath:/html/body/div[1]/div/div[3]/section/div[1]/div[1]/div[1]/div[1]/button                                      # this line clicks configure button
        element should be enabled    xpath://*[@id="pricing-date-holder"]/section[1]/div[2]/div[1]/label/i
        element should be enabled    xpath://*[@id="pricing-date-holder"]/section[1]/div[2]/div[2]/label
                   log to console    Verified "Select All" buttons are enabled.......
                   log to console    Unselect All Retailer
                    click element    xpath://*[@id="pricing-date-holder"]/section[1]/div[2]/div[1]/label/i                                              # this line unselects retailer
                    click element    xpath:/html/body/div[1]/div/div[3]/section/div[1]/div[1]/div[1]/div[1]/button                                      # this line selects configure button
                    click element    xpath:/html/body/div[1]/div/div[3]/section/div[1]/div[1]/div[1]/div[1]/button                                      # this line selects configure button
                   log to console    Select Retailer
                    click element    xpath://*[@id="pricing-date-holder"]/section[1]/div[2]/div[2]/label                                                # this line selects retailer

#Step No.2: Select from Brand Dropdown
    Wait until Element is Enabled    xpath://*[@id="pricing-date-holder"]/section[2]/div/div[1]/label/i
                   log to console    Step No.2: Select Brand Dropdown
                    click element    xpath://*[@id="pricing-date-holder"]/section[2]/div/div[1]/label/i                                                         # unselect all brands
                    click element    xpath:/html/body/div[1]/div/div[3]/section/div[1]/div[1]/div[1]/div[1]/button                                              # this line clicks configure button
    Wait until Element is Enabled    xpath:/html/body/div[1]/div/div[3]/section/div[1]/div[3]/span
        Element Should be visible    xpath:/html/body/div[1]/div/div[3]/section/div[1]/div[3]/span                                                              # verify if page contains prompt message after unselecting all products
                   log to console    Page contains the correct prompt message after unselecting brand
                   log to console    Click Configure and select a product
    Wait until Element is Enabled    xpath:/html/body/div[1]/div/div[3]/section/div[1]/div[1]/div[1]/div[1]/button
                    click element    xpath:/html/body/div[1]/div/div[3]/section/div[1]/div[1]/div[1]/div[1]/button                                              #click configure button
                    click element    xpath://*[@id="pricing-date-holder"]/section[2]/div/div[1]/label/i                                                         #select all brands
                    click element    xpath:/html/body/div[1]/div/div[3]/section/div[1]/div[1]/div[1]/div[1]/button                                              #click configure button

#Step No.3-4: check products dropdown filter by clicking on the products in the dropdown
     Wait until Element is Enabled   xpath:/html/body/div[1]/div/div[3]/section/div[1]/div[1]/div[1]/div[2]/header
                    log to console   Step No.3-4: check products dropdown filter by clicking on the products in the dropdown
                    log to console   click PRODUCTS Dropdown Filter
                     click element   xpath:/html/body/div[1]/div/div[3]/section/div[1]/div[1]/div[1]/div[2]/header                                              # this line selects product dropdown list
     Wait until Element is Enabled   xpath:/html/body/div[1]/div/div[3]/section/div[1]/div[1]/div[1]/div[2]/div/div[1]/div
                    log to console   Hover the product selected
                        mouse over   xpath:/html/body/div[1]/div/div[3]/section/div[1]/div[1]/div[1]/div[2]/div/div[1]/div
     Wait until Element is Enabled   xpath:/html/body/div[1]/div/div[3]/section/div[1]
                    log to console   click div to initiate hovering of prices
                     click element   xpath:/html/body/div[1]/div/div[3]/section/div[1]


#Step No.5: check the date filters for day - week - month - quarter - year
     Wait until Element is Enabled   xpath:/html/body/div[1]/div/div[3]/section/div[1]/div[1]/div[2]/div[2]/div[2]
                    log to console   Step No.5: check the date filters for day - week - month - quarter - year
         element should be enabled   xpath:/html/body/div[1]/div/div[3]/section/div[1]/div[1]/div[2]/div[2]/div[2]
                    log to console   click Monthly view
                     click element   xpath:/html/body/div[1]/div/div[3]/section/div[1]/div[1]/div[2]/div[2]/div[2]                                              # this line selects Month
     Wait until Element is Enabled   xpath:/html/body/div[1]/div/div[3]/section/div[1]/div[1]/div[2]/div[2]/div[3]
         element should be enabled   xpath:/html/body/div[1]/div/div[3]/section/div[1]/div[1]/div[2]/div[2]/div[3]
                    log to console   click Qtr view
                     click element   xpath:/html/body/div[1]/div/div[3]/section/div[1]/div[1]/div[2]/div[2]/div[3]                                              # this line selects Quarter
     Wait until Element is Enabled   xpath:/html/body/div[1]/div/div[3]/section/div[1]/div[1]/div[2]/div[2]/div[4]
         Element should be Enabled   xpath:/html/body/div[1]/div/div[3]/section/div[1]/div[1]/div[2]/div[2]/div[4]
                    log to console   click Year view
                     click element   xpath:/html/body/div[1]/div/div[3]/section/div[1]/div[1]/div[2]/div[2]/div[4]                                              # this line selects Year
     Wait until Element is Enabled   xpath:/html/body/div[1]/div/div[3]/section/div[1]/div[1]/div[2]/div[2]/div[1]
         Element should be Enabled   xpath:/html/body/div[1]/div/div[3]/section/div[1]/div[1]/div[2]/div[2]/div[1]
                    log to console   click Wk view
                     click element   xpath:/html/body/div[1]/div/div[3]/section/div[1]/div[1]/div[2]/div[2]/div[1]                                              # this line selects Year
     Wait until Element is Enabled   xpath:/html/body/div[1]/div/div[3]/section/div[1]/div[1]/div[2]/div[1]/button[1]/i
                    log to console   navigate to previous weeks data
                     click element   xpath:/html/body/div[1]/div/div[3]/section/div[1]/div[1]/div[2]/div[1]/button[1]/i                                         #navigate to previous weeks data
                             sleep   3s

3.0 Comparisons - validate that product comparisons graph are working
                    log to console   scroll down to 2nd graph
                        press keys   xpath:/html/body/div[1]/div/div[3]/section/div[2]   Downwards Arrow                                                        #this line scrolls down to per specific product section
                             sleep   3s
                    log to console   select a product from the list
                     click element   xpath:/html/body/div[1]/div/div[3]/section/div[2]/div[1]/header/span                                                       #clicks the product dropdown selection
                     click element   xpath:/html/body/div[1]/div/div[3]/section/div[2]/div[1]/ul/li[2]/span                                                     #selects a product from the list
                             sleep   5s
                    log to console   click configure button
                     click element   xpath:/html/body/div[1]/div/div[3]/section/div[2]/div[2]/div[1]/div[1]/button                                              #clicks configure button
                     click element   xpath://*[@id="product-date-holder"]/section[1]/div[2]/div[1]/label/i                                                      #unselects all retailer
                     click element   xpath:/html/body/div[1]/div/div[3]/section/div[2]/div[2]/div[1]/div[1]/button                                              #closes configure button
         Element should be Visible   xpath:/html/body/div[1]/div/div[3]/section/div[2]/div[4]/span                                                  #verify if prompt message is shown in the page
                     click element   xpath:/html/body/div[1]/div/div[3]/section/div[2]/div[2]/div[1]/div[1]/button                                              #click configure button
                    log to console   verify if search retailer feature is working...
                        input text   xpath://*[@id="product-date-holder"]/section[1]/div[1]/input    .com
         Element should be enabled   xpath://*[@id="product-date-holder"]/section[1]/div[2]/div[2]/div/label[3]/i
                    log to console   Verified: search field is working.....
                    log to console   click the searched retailer
                     click element   xpath://*[@id="product-date-holder"]/section[1]/div[2]/div/div/label/span/span                                             #select a retailer
                     click element   xpath://*[@id="product-date-holder"]/section[1]/div[2]/div[1]/label/i                                                      #select another retailer
                     click element   xpath:/html/body/div[1]/div/div[3]/section/div[2]/div[2]/div[1]/div[1]/button                                              #close configure button
                    log to console   select from retailer dropdown
                     click element   xpath:/html/body/div[1]/div/div[3]/section/div[2]/div[2]/div[1]/div[2]/header                                              #selects retailer dropdown
                    log to console   hover the retailer
                        mouse over   xpath:/html/body/div[1]/div/div[3]/section/div[2]/div[2]/div[1]/div[2]/div/div[1]/div                                      #hover the retailer
                             sleep   2s
                    log to console   click Retailer dropdown
                     click element   xpath:/html/body/div[1]/div/div[3]/section/div[2]/div[2]/div[1]/div[2]/header
                    log to console   Verify if date selections are working...
         Element should be enabled   xpath:/html/body/div[1]/div/div[3]/section/div[2]/div[2]/div[2]/div[2]/div[1]                                              #verify if Wk button is enable
         Element should be enabled   xpath:/html/body/div[1]/div/div[3]/section/div[2]/div[2]/div[2]/div[2]/div[2]                                              #verify if Mo button is enable
         Element should be enabled   xpath:/html/body/div[1]/div/div[3]/section/div[2]/div[2]/div[2]/div[2]/div[3]                                              #verify if Qtr button is enable
         Element should be enabled   xpath:/html/body/div[1]/div/div[3]/section/div[2]/div[2]/div[2]/div[2]/div[4]                                              #verify if Yr button is enable
                    log to console   Verified Date selections = enabled
                    log to console   Click Monthly View
                     click element   xpath:/html/body/div[1]/div/div[3]/section/div[2]/div[2]/div[2]/div[2]/div[2]                                              #selects Mo view
                             sleep   4s
                    log to console   Click Qtr View
                     click element   xpath:/html/body/div[1]/div/div[3]/section/div[2]/div[2]/div[2]/div[2]/div[3]                                              #selects Qtr view
                             sleep   4s
                    log to console   Click Yearly View
                     click element   xpath:/html/body/div[1]/div/div[3]/section/div[2]/div[2]/div[2]/div[2]/div[4]                                              #selects Yr view
                             sleep   4s
                    log to console   Click Weekly View
                     click element   xpath:/html/body/div[1]/div/div[3]/section/div[2]/div[2]/div[2]/div[2]/div[1]                                              #selects Wk view


3.0 Comparisons - validate and check the data shown in the Price VS SRP table
                       Press keys    xpath:/html/body/div[1]/div/div[3]/section/div[3]/div[1]     Downwards Arrow
                    click element    xpath://*[@id="price-vs-srp-settings"]  #click configure button
                    click element    xpath:/html/body/div[1]/div/div[3]/section/div[3]/div[1]/div/div/div/section[2]/div/div[1]/label/i                         #click all products
                    click element    xpath:/html/body/div[1]/div/div[3]/section/div[3]/div[1]/div/div/div/section[2]/div/div[1]/label/i                         #unclick all products
                    click element    xpath:/html/body/div[1]/div/div[3]/section/div[3]/div[1]/div/div/div/section[2]/div/div[2]/label/span/span                 #click product No.1
                    click element    xpath:/html/body/div[1]/div/div[3]/section/div[3]/div[1]/div/div/div/section[2]/div/div[3]/label/span/span                 #click product No.2
                    click element    xpath://*[@id="price-vs-srp-settings"]  #click configure button
                    sleep    2s

3.0 Comparisons - be able to create a comparisons group
                   log to console    3.0 Comparisons - be able to create a comparisons group
                   log to console    clicks button to Add Group
    Wait until Element is Enabled    xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/button                                                                 #verified add group button
                   log to console    Button is enabled
                    click element    xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/button                                                                 #this line clicks button to Add Group
                    click element    xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div/button                                                             #this line redirects user to Add New Group Page

#Scenario no.1: Add group withouth inputing group name
                    log to console   Scenario no.1: Add group withouth inputing group name
     Wait until Element is Enabled   xpath:/html/body/div[1]/div/div/section[2]/div/div[1]/div/div/div[1]/div/div
                     click element   xpath:/html/body/div[1]/div/div/section[2]/div/div[1]/div/div/div[1]/div/div                                                #this line selects Retailer
                    log to console   Done selecting retailers
                     #click element   xpath:/html/body/div[1]/div/div/section[3]/div/div[1]/div[2]/div[1]/div/label/i                                            #this line selects Products
     Wait until Element is Enabled   xpath:/html/body/div[1]/div/div/section[3]/div/div[1]/div[2]/label/i
                     click element   xpath:/html/body/div[1]/div/div/section[3]/div/div[1]/div[2]/label/i
                    log to console   Done selecting products
        Element Should Be Disabled   xpath:/html/body/div[1]/div/div/footer/button[2]                                                                            #this line validates if Save button is clickable
        Element Should Be Disabled   xpath:/html/body/div[1]/div/div/header/div[2]/button[3]                                                                     #this line validates if Save button is clickable
                    log to console   Result: Unable to create group w/out name - PASSED

#Scenario no.2: Add group without selecting retailer
                    log to console   Scenario no.2: Add group without selecting retailer
                    log to console   click group name
                     click element   xpath:/html/body/div[1]/div/div/section[1]/div/input                                                                        #clicks group name field
                    log to console   input group name
                        input text   xpath:/html/body/div[1]/div/div/section[1]/div/input    ${groupname}                                                        #input group name
                     click element   xpath:/html/body/div[1]/div/div/section[3]/div/div[2]/div/label/i                                                           #unselect all products
                    log to console   unselect all products
                    log to console   unselect all retailers
                     click element   xpath:/html/body/div[1]/div/div/section[2]/div/div[2]/div/label/i                                                           #unselect all retailer
        Element Should Be Disabled   xpath:/html/body/div[1]/div/div/footer/button[2]                                                                            #this line validates if Save button is clickable
        Element Should Be Disabled   xpath:/html/body/div[1]/div/div/header/div[2]/button[3]                                                                     #this line validates if Save button is clickable
                    log to console   Result: Unable to create group w/out Retailer - PASSED

#Scenario no.3: Add group without selecting Products
                    log to console   Scenario no.3: Add group without selecting Products
                     click element   xpath:/html/body/div[1]/div/div/section[2]/div/div[1]/div/label/i                                                           #select retailer
        Element Should Be Disabled   xpath:/html/body/div[1]/div/div/footer/button[2]                                                                            #this line validates if Save button is clickable
        Element Should Be Disabled   xpath:/html/body/div[1]/div/div/header/div[2]/button[3]                                                                     #this line validates if Save button is clickable
                    log to console   Result: Unable to create group w/out Products - PASSED

    #Scenario no.4: Create Group using only 1 website and 1 product
                    log to console   Scenario no.4: Create Group using only 1 website and 1 product
                     click element   xpath:/html/body/div[1]/div/div/header/div[1]/button                                                                      #navigate to comparison page
     Wait until element is Visible   xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/button                                                               #validates if element is visible
                     click element   xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/button                                                               #click group menu
                    log to console   Click Add Group button
                     click element   xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div/button                                                           #click Add group button
                    log to console   input group name
                     click element   xpath:/html/body/div[1]/div/div/section[1]/div/input                                                                      #click the text field to input group name
                        input text   xpath:/html/body/div[1]/div/div/section[1]/div/input    ${groupname}
                    log to console   Select Retailer
                     click element   xpath:/html/body/div[1]/div/div/section[2]/div/div[1]/div/div/div[3]/ul/li[1]/label/i                                     #select retailer
     Wait until element is Enabled   xpath:/html/body/div[1]/div/div/section[3]/div/div[1]/div[2]/div[1]/div/label/span/span
                     click element   xpath:/html/body/div[1]/div/div/section[3]/div/div[1]/div[2]/div[1]/div/label/span/span                                   #select a product
                    log to console   Select Products under the selected retailer
         Element should be enabled   xpath:/html/body/div[1]/div/div/header/div[2]/button[3]                                                                   #top Save button
         Element should be enabled   xpath:/html/body/div[1]/div/div/footer/button[2]                                                                          #bottom Save button
                    log to console   Verified Save buttons are enabled.....
                     click element   xpath:/html/body/div[1]/div/div/header/div[2]/button[3]                                                                   #click Save button
         Element should be enabled   xpath:/html/body/div[2]/div
                    log to console   Successfully create a group.....

#Scenario no.5: Create Group using multiple websites and multiple products
                    log to console   Scenario no.5: Create Group using multiple websites and multiple products
     Wait until Element is Enabled   xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/button
                     click element   xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/button                                                                #click group menu
                    log to console   Click Add Group button
                     click element   xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div/button                                                            #click Add group button
                    log to console   input group name
                     click element   xpath:/html/body/div[1]/div/div/section[1]/div/input                                                                       #click the text field to input group name
                        input text     xpath:/html/body/div[1]/div/div/section[1]/div/input    ${groupname2}
     Wait until Element is Enabled     xpath:/html/body/div[1]/div/div/section[2]/div/div[1]/div/div/div[1]/div/div
                     click element     xpath:/html/body/div[1]/div/div/section[2]/div/div[1]/div/div/div[1]/div/div                                             #select retailers
                    log to console     Select Retailers
     Wait until Element is Enabled     xpath:/html/body/div[1]/div/div/section[2]/div/div[1]/div/div/div[2]/ul/li/label/span/span
                     click element     xpath:/html/body/div[1]/div/div/section[2]/div/div[1]/div/div/div[2]/ul/li/label/span/span
     Wait until Element is Enabled     xpath:/html/body/div[1]/div/div/section[2]/div/div[1]/div/div/div[3]/ul/li[1]/label/span/span
                     click element     xpath:/html/body/div[1]/div/div/section[2]/div/div[1]/div/div/div[3]/ul/li[1]/label/span/span
     Wait until Element is Enabled     xpath:/html/body/div[1]/div/div/section[3]/div/div[1]/div[2]/div[1]/div/label/i
                     click element     xpath:/html/body/div[1]/div/div/section[3]/div/div[1]/div[2]/div[1]/div/label/i                                          #select a product using checkbox
                    log to console     Select Products
     Wait until Element is Enabled     xpath:/html/body/div[1]/div/div/section[3]/div/div[1]/div[2]/div[2]/div/label/i
                     click element     xpath:/html/body/div[1]/div/div/section[3]/div/div[1]/div[2]/div[2]/div/label/i                                          #select a product by clicking the product name
     Wait until Element is Enabled     xpath:/html/body/div[1]/div/div/section[3]/div/div[1]/div[2]/div[3]/div/label/span/span
                     click element     xpath:/html/body/div[1]/div/div/section[3]/div/div[1]/div[2]/div[3]/div/label/span/span
                     click element     xpath:/html/body/div[1]/div/div/section[3]/div/div[1]/div[2]/div[5]/div/label/span/span
         element should be enabled     xpath:/html/body/div[1]/div/div/header/div[2]/button[3]                                                                  #top Save button
         element should be enabled     xpath:/html/body/div[1]/div/div/footer/button[2]                                                                         #bottom Save button
                    log to console     Verified Save buttons are enabled.....
                execute javascript     window.scrollTo(document.body.scrollHeight,0)
                     click element     xpath:/html/body/div[1]/div/div/footer/button[2]                                                                         #click bottom Save button


3.0 Comparisons - Set group as Template feature and create group
                    log to console     Set existing group as template to create new one.....
     wait until element is enabled     xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/button
                     click element     xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/button                                                              #click group menu
                     click element     xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div/input                                                           #click search field
                        input text     xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div/input  Beer                                                     #input name of group
                    log to console     select existing group
                     click element     xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div/section/div[1]                                                  #select the searched group
                     click element     xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[2]/div[2]/button[3]                                                    #click Edit button
                    log to console     Scroll down the page
                        press keys     xpath:/html/body/div[1]/div/div/footer/div/button   Downwards Arrow                                                      #scrolls down the page and click elips button
     Wait Until Element Is Enabled     xpath:/html/body/div[1]/div/div/footer/div/button                                                                        #verifies if elipsis button is enabled
                    log to console     verifies elipsis button is enabled
     Wait Until Element Is Enabled     xpath:/html/body/div[1]/div/div/footer/div/ul/li[1]
                     click element     xpath:/html/body/div[1]/div/div/footer/div/ul/li[1]                                                                      #click set as template
                    log to console     Click Set as Template
        Element Should Be Disabled     xpath:/html/body/div[1]/div/div/header/div[2]/button[3]
        Element Should Be Disabled     xpath:/html/body/div[1]/div/div/footer/button[2]
                    log to console     Successfully utilized the existing group
                    log to console     click and input a group name
                        input text     xpath:/html/body/div[1]/div/div/section[1]/div/input    Template Group [QA Test]                                          #input group nam
         Element Should Be Enabled     xpath:/html/body/div[1]/div/div/footer/button[2]                                                                          #this line validates if Save button is clickable
         Element Should Be Enabled     xpath:/html/body/div[1]/div/div/header/div[2]/button[3]                                                                   #this line validates if Save button is clickable
                     click element     xpath:/html/body/div[1]/div/div/header/div[2]/button[3]                                                                   #Click Save
                    log to console     Successfully Created Comparisons Group using template feature


3.0 Comparisons - be able to edit a comparison group
                     log to console  3.0 Comparisons - be able to edit a comparison group
      Wait Until Element Is Enabled  xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/button                                                                  #Verify if group menu is enabled
                      click element  xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/button                                                                  #Click group menu
                      click element  xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div/input                                                               #click search button
                         input text  xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div/input  QA                                                           #input name of group
                      click element  xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div/section/div                                                         #select group to update
                      click element  xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[2]/div[2]/button[3]                                                        #click edit button
          Element Should be Enabled  xpath:/html/body/div[1]/div/div/header/div[2]/button[3]                                                                      #validates save button if enabled
          Element Should be Enabled  xpath:/html/body/div[1]/div/div/footer/button[2]                                                                             #Validates save button if enabled
                     log to console  Verified Save button = enabled by default
                     log to console  unselect all Retailers
                      click element  xpath:/html/body/div[1]/div/div/section[2]/div/div[2]/div/label/i                                                            #unselect all retailers
                Page should Contain  No retailers selected.                                                                                                       #prompt message after unselecting retailers = "No retailer selected"
                     log to console  Prompt message ="No retailer selected" displays after unselecting all retailers
      Wait until Element is Enabled  xpath:/html/body/div[1]/div/div/section[2]/div/div[1]/div/div/div[1]/div/div
                     log to console  Element is enabled
                      click element  xpath:/html/body/div[1]/div/div/section[2]/div/div[1]/div/div/div[1]/div/div                                                 #select 1 retailer
                     log to console  Select 1 Retailer
      Wait until Element is Enabled  xpath:/html/body/div[1]/div/div/section[3]/div/div[1]/div[2]/div[1]/div/label/span/span
                     log to console  Verified element is visible
                      click element  xpath:/html/body/div[1]/div/div/section[3]/div/div[1]/div[2]/div[3]/div/label/i                                             #add a product select a product
                              sleep  2s
                     log to console  Selected a Product
                         Press keys  xpath:/html/body/div[1]/div/div/footer  Downwards arrow
                     log to console  scroll down to the bottom of the page
          Element Should Be Enabled  xpath:/html/body/div[1]/div/div/footer/button[2]                                                                             #Validates if save button is clicable
          Element Should Be Enabled  xpath:/html/body/div[1]/div/div/header/div[2]/button[3]                                                                      #Validates if Save button is clickable
                     log to console  click title group name
                      click element  xpath:/html/body/div[1]/div/div/section[1]/div/input                                                                         #click title name
                         input text  xpath:/html/body/div[1]/div/div/section[1]/div/input    _Edited [QA]                                                         #input group name
                      click element  xpath:/html/body/div[1]/div/div/footer/button[2]                                                                             #click Save button
          Element Should be Enabled  xpath:/html/body/div[2]/div/div/label                                                                                        #Verify Prompt Message upon saving
                     log to console  Successfully Updated Comparisons Group


3.0 Comparisons - be able to delete comparison group
                     log to console    3.0 Comparisons - be able to delete comparison group
      Wait Until Element Is Enabled    xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/button                                                                 #Verify if group menu is enabled
                      click element    xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/button                                                                 #Click group menu
                     log to console    selected group menu
                      click element    xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div/input                                                              #click search button
                     log to console    selected search button
                         input text    xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div/input  _Edited [QA]                                                #input name of group
                     log to console    inputted name of group
                      click element    xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[1]/div/section/div/div[1]                                                 #select group
                     log to console    selected group
      Wait Until Element Is Enabled    xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[2]/div[2]/button[3]                                                          #verify if Edit button is enabled
                      click element    xpath:/html/body/div[1]/div/div[3]/div[1]/div/div[2]/div[2]/button[3]                                                          #click Edit button
                     log to console    click edit button
                         Press keys    xpath:/html/body/div[1]/div/div/footer/div/button   Downwards Arrow                                                            #this line scroll the page down
                      click element    xpath:/html/body/div[1]/div/div/footer/div/ul/li[2]                                                                            #click Delete button
                     log to console    click delete button
                      click element    xpath:/html/body/popup/div/div/div/div[3]/button[1]                                                                            #click No
                     log to console    click no
                      click element    xpath:/html/body/div[1]/div/div/footer/div/button                                                                              #click elipses
                     log to console    click elipses
                      click element    xpath:/html/body/div[1]/div/div/footer/div/ul/li[2]                                                                            #click delete button
                      click element    xpath:/html/body/popup/div/div/div/div[3]/button[2]                                                                            #click Yes
                     log to console    click Yes
                Page should Contain    You have successfully deleted a group.                                                                                         #prompt message
                     log to console    Successfully deleted a group




















