*** Settings ***
Library  SeleniumLibrary

*** Variables ***
#Browser used for running. Change this when testing other browsers.
${browser}   chrome

#URL for testing: Stage/Production, change this when testing other environments.
${url}   https://app.detailonline.com/auth/#/

#Credentials for accessing the tool
${username}  admin@detailonline.tech
${password}  adminpassword

#Group setup
${GroupName}    [Test-QA] For Automation
${Filter}    Amazon

*** Test Cases ***
Overview
    8.0 Login - Verify if user can login using correct credentials
    8.0 Switch Company - US Demo
    1.0 Overview - Verify that user can successfully create group with complete fields entered
    1.0 Overview - Verify that user cannot create group with No Title entered
    1.0 Overview - Verify that user cannot create group with No Retailer selected
    1.0 Overview - Verify that user cannot create group with No Products selected
    1.0 Overview - Verify that user cannot create group with No Keywords selected
    1.0 Overview - Verify that user cannot create group when Updating Retailers selected
    1.0 Overview - Verify that user can successfully create group when Updating Products selected
    1.0 Overview - Verify that user should be able to edit a group that is already created (Owned group)
    1.0 Overview - Verify that user can delete owned group
    8.0 Switch Company - Fiskars
    1.0 Overview - Verify Visibility of donut charts
    1.0 Overview - Verify Share of Shelf functionalities
    1.0 Overview - Verify Volatility section functionalities
    1.0 Overview - Verify Compliance section functionalities
    1.0 Overview - Verify Download functionality
*** Keywords ***
8.0 Login - Verify if user can login using correct credentials
    Log to console  8.0 Login - Verify if user can login using correct credentials
    Open browser     ${url}  ${browser}
    Maximize browser window
    set selenium speed  1s
    set selenium timeout  60s
    Sleep   5s
    Log to console  Input correct email address                                                                         #Input correct email address
    Input text      xpath:/html/body/div/div[1]/form/div[1]/div/input   ${username}
    Log to console  Input correct password                                                                              #Input correct password
    Input text      xpath:/html/body/div/div[1]/form/div[2]/div[1]/div/div/input    ${password}
    Log to console   Select password seeker on login form                                                               #Select password seeker on login form
    Click element   xpath:/html/body/div/div[1]/form/div[2]/div[2]
    Click element   xpath:/html/body/div/div[1]/form/div[2]/div[2]
    Log to console   Select login button & Verify Successful Login                                                      #Select login button & Verify Successful Login
    Click element   xpath:/html/body/div/div[1]/form/div[3]
    Wait Until Element Is Visible   xpath:/html/body/header     60s
8.0 Switch Company - US Demo
    Log to console  8.0 Switch Company - US Demo
    Log to console  Click Acct. Dashboard                                                                               #Click Acct. Dashboard
    Click element   xpath:/html/body/header/div[3]/div/div/header
    Log to console  Click Company list                                                                                  #Click Company list
    Click element   xpath:/html/body/header/div[3]/div/div/div/section/div/header
    Log to console  Click US Demo                                                                                       #Click US Demo
    Click element   xpath:/html/body/header/div[3]/div/div/div/section/div/section/div[2]/div[5]/label/i
    Click element   xpath:/html/body
    Wait Until Element Is Visible   xpath:/html/body/header     60s
    Page should contain     US Demo
    Sleep   3s
1.0 Overview - Verify that user can successfully create group with complete fields entered
    Log to console  1.0 Overview - Verify that user should be able to add a new group with retailers, products and keywords
    Log to console  Click on the dropdown menu for groups                                                               #Click on the dropdown menu for groups
    Click element   xpath://*[@id="product-overview"]/div[1]/div/div[1]/button
    Log to console  Click on Add New Group                                                                              #Click on Add New Group
    Click element   xpath://*[@id="product-overview"]/div[1]/div/div[1]/div/button
    Sleep   3s
    Log to console  Toggle Private Icon                                                                                 #Toggle Private Icon
    Click element   xpath:/html/body/div[1]/div/div/header/div[2]/button[1]
    Element Should Be Enabled   xpath:/html/body/div[1]/div/div/header/div[2]/button[1]
    Log to console  Toggle Favorite Icon                                                                                #Toggle Favorite Icon
    Click element   xpath:/html/body/div[1]/div/div/header/div[2]/button[2]
    Element Should Be Enabled   xpath:/html/body/div[1]/div/div/header/div[2]/button[2]
    Log to console  Enter group name                                                                                    #Enter group name
    Input text  xpath:/html/body/div[1]/div/div/section[1]/div/input    ${GroupName}
    Log to console  Verify Save button is disabled                                                                      #Verify Save button is disabled
    Element Should Be Disabled  xpath:/html/body/div[1]/div/div/footer/button[2]
    Log to console  Select desired retailers                                                                            #Select desired retailers
    Input text  xpath:/html/body/div[1]/div/div/section[2]/section/div/div[1]/input     ${Filter}
    Click element   xpath:/html/body/div[1]/div/div/section[2]/section/div/div[1]/div/label/i
    Log to console  Verify Save button is disabled                                                                      #Verify Save button is disabled
    Element Should Be Disabled  xpath:/html/body/div[1]/div/div/footer/button[2]
    Log to console  Select desired products                                                                             #Select desired products
    Input text  xpath:/html/body/div[1]/div/div/section[3]/div/div[1]/div[1]/div[1]/input   Ad
    Click element   xpath:/html/body/div[1]/div/div/section[3]/div/div[1]/div[2]/label/i
    Log to console  Verify Save button is disabled                                                                      #Verify Save button is disabled
    Element Should Be Disabled  xpath:/html/body/div[1]/div/div/footer/button[2]
    Log to console  Select All Keywords                                                                                 #Select All Keywords
    Click element   xpath:/html/body/div[1]/div/div/section[4]/section/div/div[1]/div/div/div/div/div
    Log to console  Select Save Button                                                                                  #Select Save Button
    Element Should Be Enabled  xpath:/html/body/div[1]/div/div/footer/button[2]
    Click element   xpath:/html/body/div[1]/div/div/footer/button[2]
    Wait Until Element Is Visible   xpath:/html/body/div[1]/div/div/section[1]/div[1]/div/div[1]/button     60s
1.0 Overview - Verify that user cannot create group with No Title entered
    Log to console  1.0 Overview - Verify that user cannot create group with No Title entered
    #Click on the dropdown menu for groups
    Log to console  Click on the dropdown menu for groups
    Click element   xpath://*[@id="product-overview"]/div[1]/div/div[1]/button
    #Click on Add New Group
    Log to console  Click on Add New Group
    Click element   xpath://*[@id="product-overview"]/div[1]/div/div[1]/div/button
    #Toggle Private Icon
    Log to console  Toggle Private Icon
    Click element   xpath:/html/body/div[1]/div/div/header/div[2]/button[1]
    Element Should Be Enabled   xpath:/html/body/div[1]/div/div/header/div[2]/button[1]
    #Toggle Favorite Icon
    Log to console  Toggle Favorite Icon
    Click element   xpath:/html/body/div[1]/div/div/header/div[2]/button[2]
    Element Should Be Enabled   xpath:/html/body/div[1]/div/div/header/div[2]/button[2]
    #Select desired retailers
    Log to console  Select desired retailers
    Input text  xpath:/html/body/div[1]/div/div/section[2]/section/div/div[1]/input     ${Filter}
    Click element   xpath:/html/body/div[1]/div/div/section[2]/section/div/div[1]/div/label/i
    #Select desired products
    Log to console  Select desired products
    Input text  xpath:/html/body/div[1]/div/div/section[3]/div/div[1]/div[1]/div[1]/input   Ad
    Click element   xpath:/html/body/div[1]/div/div/section[3]/div/div[1]/div[2]/label/i
    #Select All Keywords
    Log to console  Select All Keywords
    Click element   xpath:/html/body/div[1]/div/div/section[4]/section/div/div[1]/div/div/div/div/div
    #Verify Save button is disabled
    Log to console  Verify Save button is disabled
    Element Should Be Disabled  xpath:/html/body/div[1]/div/div/footer/button[2]
    #Redirect to Overview page
    Log to Console  Redirect to Overview page
    Click element   xpath:/html/body/header/div[1]/nav/ul/li[1]
    Wait Until Element Is Visible   xpath:/html/body/div[1]/div/div/section[1]/div[1]/div/div[1]/button     60s
1.0 Overview - Verify that user cannot create group with No Retailer selected
    Log to console  1.0 Overview - Verify that user cannot create group with No Retailer selected
    Sleep   3s
    #Click on the dropdown menu for groups
    Log to console  Click on the dropdown menu for groups
    Click element   xpath://*[@id="product-overview"]/div[1]/div/div[1]/button
    Sleep   3s
    #Click on Add New Group
    Log to console  Click on Add New Group
    Click element   xpath://*[@id="product-overview"]/div[1]/div/div[1]/div/button
    Sleep   5s
    #Toggle Private Icon
    Log to console  Toggle Private Icon
    Click element   xpath:/html/body/div[1]/div/div/header/div[2]/button[1]
    Element Should Be Enabled   xpath:/html/body/div[1]/div/div/header/div[2]/button[1]
    Sleep   3s
    #Toggle Favorite Icon
    Log to console  Toggle Favorite Icon
    Click element   xpath:/html/body/div[1]/div/div/header/div[2]/button[2]
    Element Should Be Enabled   xpath:/html/body/div[1]/div/div/header/div[2]/button[2]
    Sleep   3s
    #Enter group name
    Log to console  Enter group name
    Input text  xpath:/html/body/div[1]/div/div/section[1]/div/input    ${GroupName}
    Sleep   3s
    #Verify Save button is disabled
    Log to console  Verify Save button is disabled
    Element Should Be Disabled  xpath:/html/body/div[1]/div/div/footer/button[2]
    Sleep   5s
    #Redirect to Overview page
    Log to Console  Redirect to Overview page
    Click element   xpath:/html/body/header/div[1]/nav/ul/li[1]
    Wait Until Element Is Visible   xpath:/html/body/div[1]/div/div/section[1]/div[1]/div/div[1]/button     60s
1.0 Overview - Verify that user cannot create group with No Products selected
    Log to console  1.0 Overview - Verify that user cannot create group with No Products selected
    Sleep   3s
    #Click on the dropdown menu for groups
    Log to console  Click on the dropdown menu for groups
    Click element   xpath://*[@id="product-overview"]/div[1]/div/div[1]/button
    Sleep   3s
    #Click on Add New Group
    Log to console  Click on Add New Group
    Click element   xpath://*[@id="product-overview"]/div[1]/div/div[1]/div/button
    Sleep   5s
    #Toggle Private Icon
    Log to console  Toggle Private Icon
    Click element   xpath:/html/body/div[1]/div/div/header/div[2]/button[1]
    Element Should Be Enabled   xpath:/html/body/div[1]/div/div/header/div[2]/button[1]
    Sleep   3s
    #Toggle Favorite Icon
    Log to console  Toggle Favorite Icon
    Click element   xpath:/html/body/div[1]/div/div/header/div[2]/button[2]
    Element Should Be Enabled   xpath:/html/body/div[1]/div/div/header/div[2]/button[2]
    Sleep   3s
    #Enter group name
    Log to console  Enter group name
    Input text  xpath:/html/body/div[1]/div/div/section[1]/div/input    ${GroupName}
    Sleep   3s
    #Select desired retailers
    Log to console  Select desired retailers
    Input text  xpath:/html/body/div[1]/div/div/section[2]/section/div/div[1]/input     ${Filter}
    Sleep   3s
    Click element   xpath:/html/body/div[1]/div/div/section[2]/section/div/div[1]/div/label/i
    Sleep   3s
    #Select All Keywords
    Log to console  Select All Keywords
    Click element   xpath:/html/body/div[1]/div/div/section[4]/section/div/div[1]/div/div/div/div/div
    Sleep   3s
    #Verify Save button is disabled
    Log to console  Verify Save button is disabled
    Element Should Be Disabled  xpath:/html/body/div[1]/div/div/footer/button[2]
    Sleep   5s
    #Redirect to Overview page
    Log to Console  Redirect to Overview page
    Click element   xpath:/html/body/header/div[1]/nav/ul/li[1]
    Wait Until Element Is Visible   xpath:/html/body/div[1]/div/div/section[1]/div[1]/div/div[1]/button     60s
1.0 Overview - Verify that user cannot create group with No Keywords selected
    Log to console  1.0 Overview - Verify that user cannot create group with No Keywords selected
    #Click on the dropdown menu for groups
    Log to console  Click on the dropdown menu for groups
    Click element   xpath://*[@id="product-overview"]/div[1]/div/div[1]/button
    #Click on Add New Group
    Log to console  Click on Add New Group
    Click element   xpath://*[@id="product-overview"]/div[1]/div/div[1]/div/button
    Sleep   3s
    #Toggle Private Icon
    Log to console  Toggle Private Icon
    Click element   xpath:/html/body/div[1]/div/div/header/div[2]/button[1]
    Element Should Be Enabled   xpath:/html/body/div[1]/div/div/header/div[2]/button[1]
    #Toggle Favorite Icon
    Log to console  Toggle Favorite Icon
    Click element   xpath:/html/body/div[1]/div/div/header/div[2]/button[2]
    Element Should Be Enabled   xpath:/html/body/div[1]/div/div/header/div[2]/button[2]
    #Enter group name
    Log to console  Enter group name
    Input text  xpath:/html/body/div[1]/div/div/section[1]/div/input    ${GroupName}
    #Verify Save button is disabled
    Log to console  Verify Save button is disabled
    Element Should Be Disabled  xpath:/html/body/div[1]/div/div/footer/button[2]
    #Select desired retailers
    Log to console  Select desired retailers
    Input text  xpath:/html/body/div[1]/div/div/section[2]/section/div/div[1]/input     ${Filter}
    Click element   xpath:/html/body/div[1]/div/div/section[2]/section/div/div[1]/div/label/i
    #Select desired products
    Log to console  Select desired products
    Input text  xpath:/html/body/div[1]/div/div/section[3]/div/div[1]/div[1]/div[1]/input   Ad
    Click element   xpath:/html/body/div[1]/div/div/section[3]/div/div[1]/div[2]/label/i
    #Verify Save button is disabled
    Log to console  Verify Save button is disabled
    Element Should Be Disabled  xpath:/html/body/div[1]/div/div/footer/button[2]
    #Redirect to Overview page
    Log to Console  Redirect to Overview page
    Click element   xpath:/html/body/header/div[1]/nav/ul/li[1]
    Wait Until Element Is Visible   xpath:/html/body/div[1]/div/div/section[1]/div[1]/div/div[1]/button     60s
1.0 Overview - Verify that user cannot create group when Updating Retailers selected
    Log to console  1.0 Overview - Verify that user cannot create group when Updating Retailers selected
    #Click on the dropdown menu for groups
    Log to console  Click on the dropdown menu for groups
    Click element   xpath://*[@id="product-overview"]/div[1]/div/div[1]/button
    #Click on Add New Group
    Log to console  Click on Add New Group
    Click element   xpath://*[@id="product-overview"]/div[1]/div/div[1]/div/button
    Sleep   3s
    #Toggle Private Icon
    Log to console  Toggle Private Icon
    Click element   xpath:/html/body/div[1]/div/div/header/div[2]/button[1]
    Element Should Be Enabled   xpath:/html/body/div[1]/div/div/header/div[2]/button[1]
    #Toggle Favorite Icon
    Log to console  Toggle Favorite Icon
    Click element   xpath:/html/body/div[1]/div/div/header/div[2]/button[2]
    Element Should Be Enabled   xpath:/html/body/div[1]/div/div/header/div[2]/button[2]
    #Enter group name
    Log to console  Enter group name
    Input text  xpath:/html/body/div[1]/div/div/section[1]/div/input    ${GroupName}
    #Select desired retailers
    Log to console  Select desired retailers
    Input text  xpath:/html/body/div[1]/div/div/section[2]/section/div/div[1]/input     ${Filter}
    Click element   xpath:/html/body/div[1]/div/div/section[2]/section/div/div[1]/div/label/i
    #Select desired products
    Log to console  Select desired products
    Input text  xpath:/html/body/div[1]/div/div/section[3]/div/div[1]/div[1]/div[1]/input   Ad
    Click element   xpath:/html/body/div[1]/div/div/section[3]/div/div[1]/div[2]/label/i
    #Select All Keywords
    Log to console  Select All Keywords
    Click element   xpath:/html/body/div[1]/div/div/section[4]/section/div/div[1]/div/div/div/div/div
    #Update Retailers
    Log to console  Unselect Retailers
    Click element   xpath:/html/body/div[1]/div/div/section[2]/section/div/div[2]/div/div[5]/label/i
    #Verify Save button is disabled
    Log to console  Verify Save button is disabled
    Element Should Be Disabled  xpath:/html/body/div[1]/div/div/footer/button[2]
    #Redirect to Overview page
    Log to Console  Redirect to Overview page
    Click element   xpath:/html/body/header/div[1]/nav/ul/li[1]
    Wait Until Element Is Visible   xpath:/html/body/div[1]/div/div/section[1]/div[1]/div/div[1]/button     60s
1.0 Overview - Verify that user can successfully create group when Updating Products selected
    Log to console  1.0 Overview - Verify that user can successfully create group when Updating Products selected
    #Click on the dropdown menu for groups
    Log to console  Click on the dropdown menu for groups
    Click element   xpath://*[@id="product-overview"]/div[1]/div/div[1]/button
    #Click on Add New Group
    Log to console  Click on Add New Group
    Click element   xpath://*[@id="product-overview"]/div[1]/div/div[1]/div/button
    Sleep   3s
    #Toggle Private Icon
    Log to console  Toggle Private Icon
    Click element   xpath:/html/body/div[1]/div/div/header/div[2]/button[1]
    Element Should Be Enabled   xpath:/html/body/div[1]/div/div/header/div[2]/button[1]
    #Toggle Favorite Icon
    Log to console  Toggle Favorite Icon
    Click element   xpath:/html/body/div[1]/div/div/header/div[2]/button[2]
    Element Should Be Enabled   xpath:/html/body/div[1]/div/div/header/div[2]/button[2]
    #Enter group name
    Log to console  Enter group name
    Input text  xpath:/html/body/div[1]/div/div/section[1]/div/input    ${GroupName}
    #Verify Save button is disabled
    Log to console  Verify Save button is disabled
    Element Should Be Disabled  xpath:/html/body/div[1]/div/div/footer/button[2]
    #Select desired retailers
    Log to console  Select desired retailers
    Input text  xpath:/html/body/div[1]/div/div/section[2]/section/div/div[1]/input     ${Filter}
    Click element   xpath:/html/body/div[1]/div/div/section[2]/section/div/div[1]/div/label/i
    #Verify Save button is disabled
    Log to console  Verify Save button is disabled
    Element Should Be Disabled  xpath:/html/body/div[1]/div/div/footer/button[2]
    #Select desired products
    Log to console  Select desired products
    Input text  xpath:/html/body/div[1]/div/div/section[3]/div/div[1]/div[1]/div[1]/input   Ad
    Click element   xpath:/html/body/div[1]/div/div/section[3]/div/div[1]/div[2]/label/i
    #Verify Save button is disabled
    Log to console  Verify Save button is disabled
    Element Should Be Disabled  xpath:/html/body/div[1]/div/div/footer/button[2]
    #Select All Keywords
    Log to console  Select All Keywords
    Click element   xpath:/html/body/div[1]/div/div/section[4]/section/div/div[1]/div/div/div/div/div
    #Update Products
    Log to console  Update Products
    Click element   xpath:/html/body/div[1]/div/div/section[3]/div/div[2]/div/div[3]/label/span/span
    #Verify Save button is Enabled
    Log to console  Verify Save button is Enabled
    Element Should Be Enabled  xpath:/html/body/div[1]/div/div/footer/button[2]
    #Redirect to Overview page
    Log to Console  Redirect to Overview page
    Click element   xpath:/html/body/header/div[1]/nav/ul/li[1]
    Wait Until Element Is Visible   xpath:/html/body/div[1]/div/div/section[1]/div[1]/div/div[1]/button     60s
1.0 Overview - Verify that user should be able to edit a group that is already created (Owned group)
    Log to console  1.0 Overview - Verify that user should be able to edit a group that is already created (Owned group)
    #Click Edit button
    Log to console  Click Edit button
    Click element   xpath://*[@id="product-overview"]/div[1]/div/div[2]/div[2]/button[3]
    Sleep   3s
    #Add Retailers
    Log to console  Add Retailers
    Click element   xpath:/html/body/div[1]/div/div/section[2]/section/div/div[1]/div/div/div[2]/ul/li/label/i
    #Unselect Retailers
    Log to console  Unselect Retailers
    Click element   xpath:/html/body/div[1]/div/div/section[2]/section/div/div[2]/div/div[5]/label/i
    #Verify Save button is disabled
    Log to console  Verify Save button is disabled
    Element Should Be Disabled  xpath:/html/body/div[1]/div/div/footer/button[2]
    #Select All Keywords
    Log to console  Select All Keywords
    Click element   xpath:/html/body/div[1]/div/div/section[4]/section/div/div[1]/div/div/div/div/div
    #Select Save Button
    Log to console  Select Save Button
    Element Should Be Enabled  xpath:/html/body/div[1]/div/div/footer/button[2]
    Sleep   3s
    Click element   xpath:/html/body/div[1]/div/div/footer/button[2]
    Wait Until Element Is Visible   xpath:/html/body/div[1]/div/div/section[1]/div[1]/div/div[1]/button     60s
1.0 Overview - Verify that user can delete owned group
    Log to console  1.0 Overview - Verify that user can delete owned group
    #Click Edit button
    Log to console  Click Edit button
    Click element   xpath://*[@id="product-overview"]/div[1]/div/div[2]/div[2]/button[3]
    Sleep   3s
    #Click Elipses menu
    Log to console  Click Elipses menu
    Click element   xpath:/html/body/div[1]/div/div/footer/div/button
    #Click Delete option
    Log to console  Click Delete option
    Click element   xpath:/html/body/div[1]/div/div/footer/div/ul/li[2]
    #Click Confirm: Yes
    Log to console  Click Confirm: Yes
    Click element   xpath:/html/body/popup/div/div/div/div[3]/button[2]
    Wait Until Element Is Visible   xpath:/html/body/div[1]/div/div/section[1]/div[1]/div/div[1]/button     60s
8.0 Switch Company - Fiskars
    Log to console  8.0 Switch Company - Fiskars
    Log to console  Click Acct. Dashboard                                                                               #Click Acct. Dashboard
    Click element   xpath:/html/body/header/div[3]/div/div/header
    Log to console  Click Company list                                                                                  #Click Company list
    Click element   xpath:/html/body/header/div[3]/div/div/div/section/div/header
    Log to console  Click Fiskars                                                                                       #Click Fiskars
    Click element   xpath:/html/body/header/div[3]/div/div/div/section/div/section/div[2]/div[2]/label
    Click element   xpath:/html/body
    Wait Until Element Is Visible   xpath:/html/body/div[1]/div/div/section[1]/div[1]/div/div[1]/button     60s
    Log to console  Click on the dropdown menu for groups                                                               #Click on the dropdown menu for groups
    Click element   xpath:/html/body/div[1]/div/div/section[1]/div[1]/div/div[1]/button
    Log to console  Select group
    Click element   xpath:/html/body/div[1]/div/div/section[1]/div[1]/div/div[1]/div/section/div[5]
    Sleep   5s
1.0 Overview - Verify Visibility of donut charts
    Log to console  1.0 Overview - Verify Visibility of donut charts
    Log to console  Visibility is Present
    Element should be visible   xpath://*[@id="product-overview"]/div[2]/div/div[2]/div[2]/div[1]/div[1]/div[2]
    Log to console  Volatility is Present
    Element should be visible   xpath://*[@id="product-overview"]/div[2]/div/div[2]/div[2]/div[1]/div[2]/div[2]
    Log to console  Compliant is Present
    Element should be visible   xpath://*[@id="product-overview"]/div[2]/div/div[2]/div[2]/div[2]/div[1]/div[2]
    Log to console  Availability is Present
    Element should be visible   xpath://*[@id="product-overview"]/div[2]/div/div[2]/div[2]/div[2]/div[2]/div[2]
1.0 Overview - Verify Share of Shelf functionalities
    Log to Console  1.0 Overview - Verify Share of Shelf functionalities
    Log to Console  Scroll to Share of Shelf section                                                                    #Scroll to Share of Shelf section
    Press Keys  xpath:/html/body/div[1]     Downwards Arrow
    Wait Until Element Is Visible   xpath://*[@id="product-details"]/div/div[2]     60s
    Log to Console  Verify first load                                                                                   #Verify first load
    Element Should Not Be Visible   xpath:/md-progress-circular
    Log to Console  Click Retailer Filter                                                                               #Click Retailer Filter
    Click element   xpath://*[@id="product-details"]/div/div/div/div[1]/div[2]/div[1]/button
    Log to Console  Select Retailer and Verify Loading                                                                  #Select Retailer and Verify Loading
    Click element   xpath:/html/body/div[1]/div/div/section[2]/div/div/div/div[1]/div[2]/div[1]/ul/li[3]/label
    Element Should Not Be Visible   xpath:/md-progress-circular     60s
    Log to console  Verify Rank & Keywords option under Configure feature                                               #Verify Rank & Keywords option under Configure feature
    Click element   xpath://*[@id="product-details"]/div/div/div/div[1]/div[2]/div[2]/button
    Element should be visible   xpath://*[@id="keyword-drop"]/section[1]/div/label[1]
    Element Should Be Enabled   xpath://*[@id="keyword-drop"]/section[1]/div/label[1]
    Element should be visible   xpath://*[@id="keyword-drop"]/section[2]/input
    Log to console  Click Top 25 Rank                                                                                   #Click Top 25 Rank
    Element should be visible   xpath://*[@id="keyword-drop"]/section[1]/div/label[2]
    Click element   xpath://*[@id="keyword-drop"]/section[1]/div/label[2]
    Click element   xpath:/html/body/div[1]/div
    Element Should Not Be Visible   xpath:/md-progress-circular
    Log to console  Click Top 50 Rank                                                                                   #Click Top 50 Rank
    Click element   xpath://*[@id="product-details"]/div/div/div/div[1]/div[2]/div[2]/button
    Element should be visible   xpath://*[@id="keyword-drop"]/section[1]/div/label[3]
    Click element   xpath://*[@id="keyword-drop"]/section[1]/div/label[3]
    Click element   xpath:/html/body/div[1]/div
    Element Should Not Be Visible   xpath:/md-progress-circular
    Sleep   3s
1.0 Overview - Verify Volatility section functionalities
    Log to Console  1.0 Overview - Volatility functionalities
    Log to Console  Scroll to Volatility section                                                                        #Scroll to Volatility section
    Press Keys  xpath:/html/body/div[1]/div/div/section[3]/div/div[1]/div/overview-volatility/div     Downwards Arrow
    Log to Console  Click Retailer filter                                                                               #Click Retailer filter
    Click element   xpath:/html/body/div[1]/div/div/section[3]/div/div[1]/div/overview-volatility/div/div[1]/div[2]/button
    Log to Console  Select Retailer                                                                                     #Select Retailer
    Click element   xpath:/html/body/div[1]/div/div/section[3]/div/div[1]/div/overview-volatility/div/div[1]/div[2]/ul/li[2]
    Log to Console  Click Products Filter                                                                               #Click Products Filter#Click Products Filter
    Click element   xpath://*[@id="product-details"]/div/div[1]/div/overview-volatility/div/div[2]/div[1]
    Log to Console  Select Product                                                                                      #Select Product
    Click element   xpath:/html/body/div[1]/div/div/section[3]/div/div[1]/div/overview-volatility/div/div[2]/div[1]/ul/li[3]/div[2]
    Element should be enabled   xpath:/html/body/div[1]/div/div/section[3]/div/div[1]/div/overview-volatility/div/div[2]/div[1]/ul/li[3]/div[2]
    Log to Console  Toggle View by filter                                                                               #Toggle View by filter
    Click element   xpath:/html/body/div[1]/div/div/section[3]/div/div[1]/div/overview-volatility/div/div[2]/div[2]
    Sleep   2s
    Click element   xpath:/html/body/div[1]/div/div/section[3]/div/div[1]/div/overview-volatility/div/div[2]/div[2]/ul/li[1]/div/label/input
    Click element   xpath:/html/body/div[1]/div/div/section[3]/div/div[1]/div/overview-volatility/div/div[2]/div[2]/ul/li[2]/div/label/input
    Page should contain     No Results Found
    Click element   xpath:/html/body/div[1]/div/div/section[3]/div/div[1]/div/overview-volatility/div/div[2]/div[2]/ul/li[1]/div/label/input
    Click element   xpath:/html/body/div[1]/div/div/section[3]/div/div[1]/div/overview-volatility/div/div[2]/div[2]/ul/li[2]/div/label/input
    Click element   xpath:/html/body/div[1]/div/div/section[3]/div/div[1]/div/overview-volatility/div
    Sleep   3s
1.0 Overview - Verify Compliance section functionalities
    Log to Console  1.0 Overview - Verify Compliance section functionalities
    Log to Console  Scroll to View Compliance section                                                                   #Scroll to View Compliance section
    Press keys  xpath://*[@id="product-details"]/div/div[2]     Downwards Arrow
    Sleep   3s
    Log to Console  Click Retailer filter                                                                               #Click Retailer filter
    Click element   xpath:/html/body/div[1]/div/div/section[3]/div/div[2]/div/overview-compliance/div/div[1]/div[2]/button
    Log to Console  Click Retailer                                                                                      #Click Retailer
    Click element   xpath://*[@id="product-details"]/div/div[2]/div/overview-compliance/div/div[1]/div[2]/ul/li[3]
    Log to Console  Sort Product Name                                                                                   #Sort Product Name
    Click element   xpath:/html/body/div[1]/div/div/section[3]/div/div[2]/div/overview-compliance/div/div[3]/div[1]/header/div
    Log to Console  Click Product and Verify redirect to Products page                                                  #Click Product and Verify redirect to Products page
    Click element   xpath://*[@id="product-details"]/div/div[2]/div/overview-compliance/div/div[3]/div[2]/div[1]/div[1]/a
    Page should contain     Products
    Switch window
1.0 Overview - Verify Download functionality
    Log to Console  1.0 Overview - Verify Download functionality
    Log to Console  Scroll to Footer: Download button                                                                   #Scroll to Footer: Download button
    Press keys  xpath:/html/body/div[1]/div/div/div[2]/div/div[3]    Downwards Arrow
    Log to Console  Click Download Button & Verify File is Downloaded                                                   #Click Download Button & Verify File is Downloaded
    Click element   xpath:/html/body/div[1]/div/div/div[2]/div/div[3]/button
    Sleep   10s