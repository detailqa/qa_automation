*** Settings ***
Library  SeleniumLibrary

*** Test Cases ***
Login
#    create webdriver    chrome  executable_path:"C:\Driver\chromedriver.exe"
    open browser    https://app.detailonline.com    chrome   #site   #browser
    maximize browser window
    Set selenium speed  1s
    Set selenium timeout    60s
    input text  xpath: /html/body/div/div[1]/form/div[1]/div/input  rona.bananola@detailonline.tech     #email
    input text  xpath: /html/body/div/div[1]/form/div[2]/div[1]/div/div/input   randompassword  #password
    click element   xpath: /html/body/div/div[1]/form/div[3]/span   #login button
    Wait until element is visible   xpath: /html/body/header   60s
    Select US Demo
    Verify access to Products page
    Search Bar works properly
    All Product Types button works properly
    Filter button works properly
    Add New Product
    Go to Product Overview
    Create new comparison group
    Create new compliance group
    check pricing section
    go to search and stocks section
    Edit Product
*** Keywords ***
Select US Demo
     sleep   10
     log to console  click admin profile
     click element   xpath: /html/body/header/div[3]/div/div/header  #admin profile to dropdown
     log to console  dropdown companies
     click element   xpath: /html/body/header/div[3]/div/div/div/section/div/header  #companies to dropdown
     sleep   5
     log to console  select Demo
     click element   xpath: /html/body/header/div[3]/div/div/div/section/div/section/div[2]/div[1]/label/i   #radio button US Demo
     log to console  verify access to Demo company
     click element   xpath: /html/body/div[1]
     Wait until element is visible   xpath: /html/body/header  60s
Verify access to Products page
     #Click Products
     click element     xpath: /html/body/header/div[1]/nav/ul/li[2]
     Wait until element is visible  xpath: /html/body/div[1]/div/div/section[2]     60s
     log to console  redirected to Products page
     sleep   5
Search Bar works properly
     sleep   10
     click element   xpath: /html/body/header/div[1]/nav/ul/li[2]/a  #click search bar
     log to console  page contains search bar
     sleep   10
     input text  xpath: /html/body/div[1]/div/div/section[1]/input   Acer Predator Triton 500 i5   #search Acer Predator Triton 500 i5
     log to console  Verify product found on page
     #sleep   5
     clear element text  xpath: /html/body/div[1]/div/div/section[1]/input
     #sleep   5
     input text  xpath: /html/body/div[1]/div/div/section[1]/input   NON-EXISTENT PRODUCT    #search non-existent product
     log to console  Verify product NOT found on page
     clear element text  xpath: /html/body/div[1]/div/div/section[1]/input

All Product Types button works properly

     sleep   5
     #wait until page contains element    xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/header/span
     click element   xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/header/span  #click All product types button
     #add- display all products under All Product Types
     sleep   5
     log to console  click All Product Types button

     #wait until page contains element    xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/ul/li[2]  60s
     click element   xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/ul/li[2]    #click Apparel and Shoes
     #add- display all products under Apparel and Shoes
     click element   xpath: /html/body/div[1]    #click outside
     #sleep   5
     log to console  Apparel and Shoes selected

     #wait until page contains element    xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/header/span   60s
     click element   xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/header/span  #click All product types button
     #add- display all products under All Product Types
     #sleep   5
     log to console  click All Product Types button

     #wait until page contains element    xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/ul/li[3]  60s
     click element   xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/ul/li[3]     #click Cars
     #add- display all products under Cars
     click element   xpath: /html/body/div[1]    #click outside
     #sleep   5
     log to console  Cars selected

     #wait until page contains element    xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/header/span   60s
     click element   xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/header/span  #click All product types button
     #add- display all products under All Product Types
     #sleep   5
     log to console  click All Product Types button

     #wait until page contains element    xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/ul/li[4]/span     60s
     click element   xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/ul/li[4]/span     #click Cosmetics and Perfumes
     #add- display all products under Cosmetics and Perfumes
     click element   xpath: /html/body/div[1]    #click outside
     #sleep   5
     log to console  Cosmetics and Perfumes selected

     #wait until page contains element    xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/header/span   60s
     click element   xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/header/span  #click All product types button
     #add- display all products under All Product Types
     #sleep   5
     log to console  click All Product Types button

     #wait until page contains element    xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/ul/li[5]  60s
     click element   xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/ul/li[5]  #click DIY
     #add- display all products under DIY
     click element   xpath: /html/body/div[1]    #click outside
     #sleep   5
     log to console  DIY selected

     #wait until page contains element    xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/header/span   60s
     click element   xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/header/span  #click All product types button
     #add- display all products under All Product Types
     #sleep   5
     log to console  click All Product Types button

     #wait until page contains element    xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/ul/li[6]/span     60s
     click element   xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/ul/li[6]/span  #click FMCG
     #add- display all products under FMCG
     click element   xpath: /html/body/div[1]    #click outside
     #sleep   5
     log to console  FMCG selected

     #wait until page contains element    xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/header/span   60s
     click element   xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/header/span  #click All product types button
     #add- display all products under All Product Types
     #sleep   5
     log to console  click All Product Types button

     #wait until page contains element    xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/ul/li[7]  60s
     click element   xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/ul/li[7]  #click Makeup
     #add- display all products under Makeup
     click element   xpath: /html/body/div[1]    #click outside
     #sleep   5
     log to console  Makeup selected

     #wait until page contains element    xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/header/span   60s
     click element   xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/header/span  #click All product types button
     #add- display all products under All Product Types
     #sleep   5
     log to console  click All Product Types button

     #wait until page contains element    xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/ul/li[8]/span     60s
     click element   xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/ul/li[8]/span  #click Pet Healthcare
     #add- display all products under Pet Healthcare
     click element   xpath: /html/body/div[1]    #click outside
     #sleep   5
     log to console  Pet Healthcare selected

     #wait until page contains element    xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/header/span   60s
     click element   xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/header/span  #click All product types button
     #add- display all products under All Product Types
     #sleep   5
     log to console  click All Product Types button

     #wait until page contains element    xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/ul/li[9]  60s
     click element   xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/ul/li[9]  #click Razors
     #add- display all products under Razors
     click element   xpath: /html/body/div[1]    #click outside
     #sleep   5
     log to console  Razors selected

    # wait until page contains element    xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/header/span   60s
     click element   xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/header/span  #click All product types button
     #add- display all products under All Product Types
     #sleep   5
     log to console  click All Product Types button

    #wait until page contains element    xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/ul/li[10]     60s
     click element   xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/ul/li[10]  #click Tech
     #add- display all products under Tech
     click element   xpath: /html/body/div[1]    #click outside
     #sleep   5
     log to console  Tech selected

     #wait until page contains element    xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/header/span   60s
     click element   xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/header/span  #click All product types button
     #add- display all products under All Product Types
     #sleep   5
     log to console  click All Product Types button

     #wait until page contains element    xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/ul/li[11]/span    60s
     click element   xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/ul/li[11]/span  #click Vacuum Cleaners
     #add- display all products under Vacuum Cleaners
     click element   xpath: /html/body/div[1]    #click outside
     #sleep   5
     log to console  Vacuum Cleaners selected

    # wait until page contains element    xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/header/span    60s
     click element   xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/header/span  #click All product types button
     #add- display all products under All Product Types
     #sleep   5
     log to console  click All Product Types button

    # wait until page contains element    xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/ul/li[1]  60s
     click element   xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[1]/ul/li[1]  #click All Product Types
     #add- display all products under All Product Types
     click element   xpath: /html/body/div[1]    #click outside
     #sleep   5
     log to console  All Product Types selected

Filter button works properly

     #DISPLAY PRODUCTS... FILTER
     #sleep   5
    # wait until page contains element     xpath: //*[@id="filter-menu"]  60s
     click element   xpath: //*[@id="filter-menu"]   #click Filter button

     page should contain     Display products...     #Display Products in Filter

     click element   xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[2]/div/div/section[1]/div[1]/div[1]/label/i  #deselect With Image products
     #sleep   5
     click element   xpath: /html/body/div[1]    #click outside
     log to console  Display all products Without Image

     #sleep   5
     click element   xpath: //*[@id="filter-menu"]   #click Filter button

     click element   xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[2]/div/div/section[1]/div[1]/div[1]/label/i  #select With Image products
     #sleep   5
     click element   xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[2]/div/div/section[1]/div[1]/div[2]/label/i  #deselect Without Image products
     click element   xpath: /html/body/div[1]    #click outside
     log to console  Display all products With Image

     #sleep   5
     click element   xpath: //*[@id="filter-menu"]   #click Filter button

     click element   xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[2]/div/div/section[1]/div[1]/div[1]/label/i  #deselect With Image products
     click element   xpath: /html/body/div[1]    #click outside
     log to console  Display NO products -- "No Results Found"

     click element   xpath: //*[@id="filter-menu"]   #click Filter button

     click element   xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[2]/div/div/section[1]/div[1]/div[1]/label/i  #select With Image products
     click element   xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[2]/div/div/section[1]/div[1]/div[2]/label/i  #select Without Image products
     click element   xpath: /html/body/div[1]    #click outside
     #sleep   5
     log to console  Display all products With and Without Images

     #sleep   5
     click element   xpath: //*[@id="filter-menu"]   #click Filter button

     click element   xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[2]/div/div/section[1]/div[2]/div[1]/label/i  #deselect click My products
     click element   xpath: /html/body/div[1]    #click outside
     #sleep   5
     log to console  Display all Competitor products

     #sleep   5
     click element   xpath: //*[@id="filter-menu"]   #click Filter button

     click element   xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[2]/div/div/section[1]/div[2]/div[1]/label/i  #select click My products
     click element   xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[2]/div/div/section[1]/div[2]/div[2]/label/i  #deselect Competitor products
     click element   xpath: /html/body/div[1]    #click outside
     #sleep   5
     log to console  Display all My Products

     #sleep   5
     click element   xpath: //*[@id="filter-menu"]   #click Filter button

     click element   xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[2]/div/div/section[1]/div[2]/div[1]/label/i  #deselect click My products
     click element   xpath: /html/body/div[1]    #click outside
     #sleep   5
     log to console  Display NO products -- "No Results Found"

     #sleep   5
     click element   xpath: //*[@id="filter-menu"]   #click Filter button

     click element   xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[2]/div/div/section[1]/div[2]/div[1]/label/i  #select click My products
     click element   xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[2]/div/div/section[1]/div[2]/div[2]/label/i  #select Competitor products
     click element   xpath: /html/body/div[1]    #click outside
     #sleep   5
     log to console  Display all products

     #sleep   5
     click element   xpath: //*[@id="filter-menu"]   #click Filter button

     #RETAILERS FILTER
     click element   xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[2]/div/div/section[2]/div/div[1]/label/i  #deselect All Retailers
     click element   xpath: /html/body/div[1]    #click outside
     #sleep   5
     log to console  Display NO products -- "No Results Found"

     #sleep   5
     click element   xpath: //*[@id="filter-menu"]   #click Filter button

     click element   xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[2]/div/div/section[2]/div/div[2]/div/div/div  #select Russia
     click element   xpath: /html/body/div[1]    #click outside
     #sleep   5
     log to console  Display products under Russia

     #sleep   5
     click element   xpath: //*[@id="filter-menu"]   #click Filter button

     click element   xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[2]/div/div/section[2]/div/div[1]/label/i  #select All Retailers

     click element   xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[2]/div/div/section[3]/div/div/div/div  #deselect All Brands
     click element   xpath: /html/body/div[1]    #click outside
     #sleep   5
     log to console  Display NO brands -- "No Results Found"

     #sleep   5
     click element   xpath: //*[@id="filter-menu"]   #click Filter button

     click element   xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[2]/div/div/section[3]/div/div/ul/li[1]/label/i  #select Acer brand
     click element   xpath: /html/body/div[1]    #click outside
     #sleep   5
     log to console  Display Acer products

     #sleep   5
     click element   xpath: //*[@id="filter-menu"]   #click Filter button

     click element   xpath: /html/body/div[1]/div/div/section[1]/div[2]/div[2]/div/div/section[3]/div/div/div/div  #select All Brands

Add New Product
     wait until page contains element    xpath: /html/body/div[1]/div/div/section[1]/button
     click element   xpath: /html/body/div[1]/div/div/section[1]/button  #click Add New
     sleep   5

     #FIRST PANEL
     input text  xpath: /html/body/div[1]/div/div/section[2]/section/div[1]/div[1]/input     Acer Predator Triton 500 i5 [TEST]     # add new product Acer Predator Triton 500 i5 [TEST]
     log to console  Product name inputted successfully
     input text  xpath: /html/body/div[1]/div/div/section[2]/section/div[1]/div[2]/input     [TEST]-123  # product sku
     log to console  SKU inputted successfully
     click element  xpath: /html/body/div[1]/div/div/section[2]/section/div[2]/section[1]/div/header     # brand dropdown
     log to console  Brand clicked to dropdown
     click element   xpath: /html/body/div[1]/div/div/section[2]/section/div[2]/section[1]/div/ul/li[1]  #select Acer brand
     log to console  Acer selected for brand
     click element   xpath: /html/body/div[1]/div/div/section[2]/section/div[2]/section[2]/div/header    #product type dropdown
     log to console  Product Type clicked to dropdown
     click element   xpath: /html/body/div[1]/div/div/section[2]/section/div[2]/section[2]/div/ul/li[1]    #select Apparel and shoes
     log to console  Apparel and shoes selected for product type

     #CLICK NEXT
     click element   xpath: /html/body/div[1]/div/div/footer/div[2]/button[2]    #click Next
     log to console  click Next

     #SELECT RETAILERS
     sleep   5
     #select/unselect 1 region - retailers
     click element   xpath: /html/body/div[1]/div/div/section[3]/section/div/div[1]/div/div/div[3]/div/div   #click United Kingdom region
     log to console  United Kingdom retailers selected
     click element   xpath: /html/body/div[1]/div/div/section[3]/section/div/div[2]/div/label/i  #unselect UK region

     #use search to search retailers
     input text  xpath: /html/body/div[1]/div/div/section[3]/section/div/div[1]/input    amazon     #search for amazon retailers
     log to console  show amazon retailers
     sleep   5

     #select/unselect grp of retailers from search
     click element   xpath: /html/body/div[1]/div/div/section[3]/section/div/div[1]/div/div/div[3]/div/div   #click United Kingdom region
     log to console  United Kingdom retailers selected
     click element   xpath: /html/body/div[1]/div/div/section[3]/section/div/div[2]/div/label/i     #Unselect All United Kingdom retailers
     log to console  United Kingdom retailers unselected

     #select/unselect 1 retailer from search
     click element   xpath: /html/body/div[1]/div/div/section[3]/section/div/div[1]/div/div/div[3]/ul/li[1]/label/i     #select amazon.co.uk
     log to console  amazon.co.uk retailer selected
     click element   xpath: /html/body/div[1]/div/div/section[3]/section/div/div[2]/div/div/label/i      #unselect amazon.co.uk

     #amazon.co.uk retained as selected retailer
     clear element text  xpath: /html/body/div[1]/div/div/section[3]/section/div/div[1]/input    #clear search
     log to console  clear search field

     #select/unselect all retailers
     click element   xpath: /html/body/div[1]/div/div/section[3]/section/div/div[1]/div/label/i  #click Select All
     log to console  Select All retailers
     click element   xpath: /html/body/div[1]/div/div/section[3]/section/div/div[2]/div/label/i  #click Unselect All
     log to console  Unselect All retailers
     click element   xpath: /html/body/div[1]/div/div/section[3]/section/div/div[1]/div/label/i  #click Select All
     log to console  Select All retailers

     #CLICK NEXT
     click element   xpath: /html/body/div[1]/div/div/footer/div[2]/button[2]   #Next to show links for live search
     log to console  click Next

     click element   xpath: /html/body/div[1]/div/div/footer/div[2]/button[2]    #Next to activate live search
     sleep   30

     #SUGGESTED RETAIL PRICE
     click element   xpath: /html/body/div[1]/div/div/footer/div[2]/button[2]    #Next
     sleep   10

     input text  xpath: /html/body/div[1]/div/div/section[4]/section/section/div[1]/input    99999.99    #input price
     log to console  price inputted
     click element   xpath: /html/body/div[1]/div/div/section[4]/section/section/div[2]/div/span[1]  #Add Currency
     #click element   xpath: /html/body/div[1]/div/div/section[4]/section/section/div[2]/div/span[2]/i    #add currency
     log to console  Add currency
     click element   xpath: /html/body/div[1]/div/div/section[4]/section/section/div[2]/ul/li[1]    #choose GBP
     click element   xpath: /html/body/div[1]/div/div/footer/div[2]/button[2]    #Next
     sleep   5

     #PRODUCT IMAGE

     page should contain     Product Image   #product image
     click element     xpath: /html/body/div[1]/div/div/section[5]/section/section/div[1]/img  #provided image
     click element   xpath: /html/body/div[1]/div/div/section[5]/section/section/div[2]/label    #upload image

     click element   xpath: /html/body/div[1]/div/div/footer/div[2]/button[2]    #Next
     wait until page contains    Save    #acknowledge Save button
     click element   xpath: /html/body/div[1]/div/div/footer/div[2]/button[1]    #cancel

Go to Product Overview
     #PRODUCT OVERVIEW
     sleep   5
     Wait until element is visible  xpath: /html/body/div[1]/div/div/section[2]     60s
     click element   xpath: /html/body/div[1]/div/div/section[2]/a[1]/div[2]     #choose product
     wait until page contains    Acer Predator Triton 500 i5     #name of product
     log to console  redirected to product overview
     log to console  name of product found
     wait until page contains    Product Type Tech     #product type
     log to console  product type found
     wait until page contains    Product ID 7aadd862-c4f0-450b-ace5-8c4b67908b92     #product ID
     log to console  product ID found
     wait until page contains    Brand Acer     #Brand
     log to console  brand found
     wait until page contains    --     #Brand aliases
     log to console  brand aliases found

     #PRODUCT OVERVIEW
     sleep   10
     click element   xpath: /html/body/div[1]/div/div[1]/article/section[2]/div/section[1]/header/div/div/button  #gear Pricing
     log to console  Pricing gear clicked
     page should contain     Select Country
     page should contain     Period
     log to console  contains country, period
     click element   xpath: /html/body/div[1]/div/div[1]/article/section[2]/div/section[1]/header/div/div/div/div[1]/div/i   #Select Country
     log to console  country selected

     click element  xpath: /html/body/div[1]/div/div[1]/article/section[2]/div/section[1]/header/div/div/div/div[3]/input  #Select Period
     log to console  click date picker for period
     input text   xpath: /html/body/div[1]/div/div[1]/article/section[2]/div/div/div[1]/div[1]/input     2019/12/10  #input date FROM
     input text   xpath: /html/body/div[1]/div/div[1]/article/section[2]/div/div/div[2]/div[1]/input     2020/01/07  #input date TO
     click element   xpath: /html/body/div[1]    #click outside
     log to console  input date using text area

     click element   xpath: /html/body/div[1]/div/div[1]/article/section[2]/div/section[1]/header/div/div/button  #gear Pricing
     click element  xpath: /html/body/div[1]/div/div[1]/article/section[2]/div/section[1]/header/div/div/div/div[3]/input  #Select Period
     click element   xpath: /html/body/div[1]/div/div[1]/article/section[2]/div/div/div[1]/div[2]/table/thead/tr[1]/th[1]   #month picker left arrow
     click element   xpath: /html/body/div[1]/div/div[1]/article/section[2]/div/div/div[1]/div[2]/table/thead/tr[1]/th[1]   #month picker left arrow
     log to console  left arrow for month clicked
     click element   xpath: /html/body/div[1]/div/div[1]/article/section[2]/div/div/div[1]/div[2]/table/thead/tr[1]/th[3]    #month picker right arrow
     click element   xpath: /html/body/div[1]/div/div[1]/article/section[2]/div/div/div[1]/div[2]/table/thead/tr[1]/th[3]    #month picker right arrow
     click element   xpath: /html/body/div[1]    #click outside
     log to console  right arrow for month clicked
     sleep   5

     click element   xpath: /html/body/div[1]/div/div[1]/article/section[2]/div/section[1]/div/div[1]/div/header     #click center country dropdown
     log to console  center country clicked
     click element   xpath: /html/body/div[1]/div/div[1]/article/section[2]/div/section[1]/div/div[1]/div/ul/li  #choose United Kingdom
     click element   xpath: /html/body/div[1]    #click outside
     log to console  Country selected from center dropdown

#     mouse over   xpath: //*[@id="lowest"]    #hover LOWEST pricing graph
#     mouse over   xpath: //*[@id="highest"]   #hover HIGHEST pricing graph
#     log to console  hover over pricing graphs
     page should contain     Highest Price
     log to console  Highest Price found
     page should contain     Lowest Price
     log to console  Lowest Price found
     page should contain     ERP
     log to console  ERP found

Create new comparison group
     click element   xpath: /html/body/div[1]/div/div[1]/article/section[2]/div/section[1]/div/footer/button     #create pricing group
     sleep   5
     page should contain     Create new comparison group  #redirected to Create new comparison group page
     log to console  redirected to Create new comparison group page
     sleep   5
     click element   xpath: /html/body/div[1]/div/div/header/div[1]/button   #click Back button
     sleep   10
     element should be visible   xpath: /html/body/div[1]/div/div[1]/header/header/span    #verify back to product overview
     log to console  verify redirected back to Product module

     page should contain     Number of Websites
     page should contain     Image Matches
     page should contain     Consumer Rating
     page should contain     Total Specs

Create new compliance group
     click element   xpath: /html/body/div[1]/div/div[1]/article/section[2]/div/section[2]/footer/button     #create compliance group
     sleep   5
     page should contain     Create new compliance group  #redirected to Create new compliance group page
     log to console  redirected to Create new compliance group page
     sleep   5
     click element   xpath: /html/body/div[1]/div/div/header/div[1]/button   #click Back button
     sleep   10
     element should be visible   xpath: /html/body/div[1]/div/div[1]/header/header/span    #verify back to product overview
     log to console  verify redirected back to Product module
     sleep   5

Check Pricing section
     #CONFIGURE > WEBSITES , RETAILER DROPDOWN
     press keys  xpath: /html/body/div[1]    ARROW_DOWN  #convert to for-loop
     press keys  xpath: /html/body/div[1]    ARROW_DOWN
     press keys  xpath: /html/body/div[1]    ARROW_DOWN
     press keys  xpath: /html/body/div[1]    ARROW_DOWN
     press keys  xpath: /html/body/div[1]    ARROW_DOWN
     press keys  xpath: /html/body/div[1]    ARROW_DOWN
     press keys  xpath: /html/body/div[1]    ARROW_DOWN
     press keys  xpath: /html/body/div[1]    ARROW_DOWN
     press keys  xpath: /html/body/div[1]    ARROW_DOWN
     press keys  xpath: /html/body/div[1]    ARROW_DOWN
     press keys  xpath: /html/body/div[1]    ARROW_DOWN
     press keys  xpath: /html/body/div[1]    ARROW_DOWN
     press keys  xpath: /html/body/div[1]    ARROW_DOWN
     press keys  xpath: /html/body/div[1]    ARROW_DOWN
     press keys  xpath: /html/body/div[1]    ARROW_DOWN
#     set selenium speed  2s
#     set selenium timeout  60s
     click element   xpath: /html/body/div[1]/div/div[2]/section/div[1]/div[1]/div[1]/div[1]/button  #configure
     log to console  configure button clicked
     page should contain     Websites
     log to console  Websites found
     page should contain     Period
     log to console  Period found

     #set selenium speed  2s
     click element   xpath: /html/body/div[1]/div/div[2]/section/div[1]/div[1]/div[1]/div[1]/div/div/section[1]/div[2]/div[1]/label/i    #unselect all
     click element   xpath: /html/body/div[1]     #click outside
     click element   xpath: /html/body/div[1]/div/div[2]/section/div[1]/div[1]/div[1]/div[2]/header  #retailers dropdown
     log to console  No retailers available in dropdown

     click element   xpath: /html/body/div[1]/div/div[2]/section/div[1]/div[1]/div[1]/div[1]/button  #configure
     click element   xpath: /html/body/div[1]/div/div[2]/section/div[1]/div[1]/div[1]/div[1]/div/div/section[1]/div[2]/div[2]/div/label[1]/i    #select one retailer
     click element   xpath: /html/body/div[1]     #click outside
     click element   xpath: /html/body/div[1]/div/div[2]/section/div[1]/div[1]/div[1]/div[2]/header  #retailers dropdown
     log to console  only amazon.co.uk available in dropdown

     click element   xpath: /html/body/div[1]/div/div[2]/section/div[1]/div[1]/div[1]/div[1]/button  #configure
     click element   xpath: /html/body/div[1]/div/div[2]/section/div[1]/div[1]/div[1]/div[1]/div/div/section[1]/div[2]/div[2]/label/i    #select region
     click element   xpath: /html/body/div[1]     #click outside
     click element   xpath: /html/body/div[1]/div/div[2]/section/div[1]/div[1]/div[1]/div[2]/header  #retailers dropdown
     log to console  retailers for United Kingdom available in dropdown

     click element   xpath: /html/body/div[1]/div/div[2]/section/div[1]/div[1]/div[1]/div[1]/button  #configure
     click element   xpath: /html/body/div[1]/div/div[2]/section/div[1]/div[1]/div[1]/div[1]/div/div/section[1]/div[2]/div[2]/div/label[1]/i     #unselect one retailer
     click element   xpath: /html/body/div[1]     #click outside
     click element   xpath: /html/body/div[1]/div/div[2]/section/div[1]/div[1]/div[1]/div[2]/header  #retailers dropdown
     log to console  No amazon.co.uk available in dropdown

     click element   xpath: /html/body/div[1]/div/div[2]/section/div[1]/div[1]/div[1]/div[1]/button  #configure
     click element   xpath: /html/body/div[1]/div/div[2]/section/div[1]/div[1]/div[1]/div[1]/div/div/section[1]/div[2]/div[2]/div/label[1]/i     #select all
     click element   xpath: /html/body/div[1]     #click outside
     click element   xpath: /html/body/div[1]/div/div[2]/section/div[1]/div[1]/div[1]/div[2]/header  #retailers dropdown
     log to console  all retailers available in dropdown

     click element   xpath: /html/body/div[1]/div/div[2]/section/div[1]/div[1]/div[1]/div[1]/button  #configure
     input text  xpath: /html/body/div[1]/div/div[2]/section/div[1]/div[1]/div[1]/div[1]/div/div/section[1]/div[1]/input     co.uk   #search
     log to console  show retailers with co.uk
     clear element text   xpath: /html/body/div[1]/div/div[2]/section/div[1]/div[1]/div[1]/div[1]/div/div/section[1]/div[1]/input    #clear search
     click element   xpath: /html/body/div[1]     #click outside

     #CONFIGURE > PERIOD
     click element   xpath: /html/body/div[1]/div/div[2]/section/div[1]/div[1]/div[1]/div[1]/button  #configure
     click element   xpath: /html/body/div[1]/div/div[2]/section/div[1]/div[1]/div[1]/div[1]/div/div/section[2]/div/div[2]/input     #click input date
     input text  xpath: /html/body/div[1]/div/div[2]/section/div[1]/div[1]/div[1]/div[1]/div/div/div/div[1]/div[1]/input     2019/12/10
     input text  xpath: /html/body/div[1]/div/div[2]/section/div[1]/div[1]/div[1]/div[1]/div/div/div/div[2]/div[1]/input     2020/01/10
     press keys  xpath: /html/body/div[1]/div/div[2]/section/div[1]/div[1]/div[1]/div[1]/div/div/div/div[2]/div[1]/input     RETURN  #keyboard ENTER
     click element   xpath: /html/body/div[1]     #click outside

     #DATE PICKER
     click element   xpath: /html/body/div[1]/div/div[2]/section/div[1]/div[1]/div[2]/div[1]/button[1]   #left arrow date picker
     log to console  left arrow clicked for date picker
     sleep   3
     click element   xpath: /html/body/div[1]/div/div[2]/section/div[1]/div[1]/div[2]/div[1]/button[2]   #right arrow date picker
     log to console  right arrow clicked for date picker

     click element   xpath: /html/body/div[1]/div/div[2]/section/div[1]/div[1]/div[2]/div[2]/div[1]  #wk
     log to console  week date picker selected
     click element   xpath: /html/body/div[1]/div/div[2]/section/div[1]/div[1]/div[2]/div[2]/div[2]  #mo
     log to console  month date picker selected
     click element   xpath: /html/body/div[1]/div/div[2]/section/div[1]/div[1]/div[2]/div[2]/div[3]  #qtr
     log to console  quarter date picker selected
     click element   xpath: /html/body/div[1]/div/div[2]/section/div[1]/div[1]/div[2]/div[2]/div[4]  #yr
     log to console  year date picker selected

go to Search and Stocks section
     press keys  xpath: /html/body/div[1]    ARROW_DOWN
     press keys  xpath: /html/body/div[1]    ARROW_DOWN
     press keys  xpath: /html/body/div[1]    ARROW_DOWN
     press keys  xpath: /html/body/div[1]    ARROW_DOWN
     press keys  xpath: /html/body/div[1]    ARROW_DOWN
     press keys  xpath: /html/body/div[1]    ARROW_DOWN
     press keys  xpath: /html/body/div[1]    ARROW_DOWN
     press keys  xpath: /html/body/div[1]    ARROW_DOWN
     press keys  xpath: /html/body/div[1]    ARROW_DOWN
     press keys  xpath: /html/body/div[1]    ARROW_DOWN
     press keys  xpath: /html/body/div[1]    ARROW_DOWN
     press keys  xpath: /html/body/div[1]    ARROW_DOWN
     press keys  xpath: /html/body/div[1]    ARROW_DOWN
     press keys  xpath: /html/body/div[1]    ARROW_DOWN
     press keys  xpath: /html/body/div[1]    ARROW_DOWN
     press keys  xpath: /html/body/div[1]    ARROW_DOWN
     press keys  xpath: /html/body/div[1]    ARROW_DOWN
     press keys  xpath: /html/body/div[1]    ARROW_DOWN
     press keys  xpath: /html/body/div[1]    ARROW_DOWN
     press keys  xpath: /html/body/div[1]    ARROW_DOWN

     wait until page contains element    xpath: /html/body/div[1]/div/div[2]/section/div[2]/div[1]/div/button    #configure button
     click element   xpath: /html/body/div[1]/div/div[2]/section/div[2]/div[1]/div/button    #configure
     page should contain     Websites
     log to console  Websites found
     page should contain     View
     log to console  View found

     #WEBSITES
     sleep   5
     element should be enabled   xpath: /html/body/div[1]/div/div[2]/section/div[2]/div[1]/div/div/div/section[1]/div[2]/div[1]/label/i  #default select all
     log to console  verified default Websites: Select All

     click element   xpath: /html/body/div[1]/div/div[2]/section/div[2]/div[1]/div/div/div/section[1]/div[2]/div[1]/label/i  #deselect all
     click element   xpath: /html/body/div[1]     #click outside
     log to console  no data for table displayed on page

     click element   xpath: /html/body/div[1]/div/div[2]/section/div[2]/div[1]/div/button    #configure
     click element   xpath: /html/body/div[1]/div/div[2]/section/div[2]/div[1]/div/div/div/section[1]/div[2]/div[2]/div/label[1]/i  #one retailer selected
     click element   xpath: /html/body/div[1]     #click outside
     log to console  data for selected retailer displayed on page

     click element   xpath: /html/body/div[1]/div/div[2]/section/div[2]/div[1]/div/button    #configure
     click element   xpath: /html/body/div[1]/div/div[2]/section/div[2]/div[1]/div/div/div/section[1]/div[2]/div[2]/label/i  #one region selected
     click element   xpath: /html/body/div[1]     #click outside
     log to console  data for retailers under selected region displayed on page

     #SEARCH
     click element   xpath: /html/body/div[1]/div/div[2]/section/div[2]/div[1]/div/button    #configure
     input text  xpath: /html/body/div[1]/div/div[2]/section/div[2]/div[1]/div/div/div/section[1]/div[1]/input   .co.uk     #search websites
     log to console  keyword inputted to search
     sleep   2
     clear element text  xpath: /html/body/div[1]/div/div[2]/section/div[2]/div[1]/div/div/div/section[1]/div[1]/input   #clear search websites
     log to console  keyword cleared from search

     #VIEW
     element should be enabled     xpath: /html/body/div[1]/div/div[2]/section/div[2]/div[1]/div/div/div/section[1]/div[2]/div[1]/label/i  #default select all
     log to console  verified default View: Select All
     click element   xpath: /html/body/div[1]/div/div[2]/section/div[2]/div[1]/div/div/div/section[1]/div[2]/div[1]/label/i  #unselect all
     click element   xpath: /html/body/div[1]     #click outside
     log to console  No View available

     click element   xpath: /html/body/div[1]/div/div[2]/section/div[2]/div[1]/div/button    #configure
     click element   xpath: /html/body/div[1]/div/div[2]/section/div[2]/div[1]/div/div/div/section[2]/div/div/div[2]/i   #select one View
     click element   xpath: /html/body/div[1]     #click outside
     log to console  one View available

     click element   xpath: /html/body/div[1]/div/div[2]/section/div[2]/div[1]/div/button    #configure
     click element   xpath: /html/body/div[1]/div/div[2]/section/div[2]/div[1]/div/div/div/section[2]/div/div/div[1]/label/i   #select all View
     click element   xpath: /html/body/div[1]     #click outside
     log to console  All View available

     click element   xpath: /html/body/div[1]/div/div[2]/section/div[2]/div[1]/div/button    #configure
     click element   xpath: /html/body/div[1]/div/div[2]/section/div[2]/div[1]/div/div/div/section[1]/div[2]/div[1]/label/i  #deselect all Websites
     click element   xpath: /html/body/div[1]/div/div[2]/section/div[2]/div[1]/div/div/div/section[1]/div[2]/div[1]/label/i  #select all Websites
     click element   xpath: /html/body/div[1]/div/div[2]/section/div[2]/div[1]/div/div/div/section[2]/div/div/div[1]/label/i     #deselect all View
     click element   xpath: /html/body/div[1]     #click outside
     log to console  no table displayed on page  #no websites    #no view

     click element   xpath: /html/body/div[1]/div/div[2]/section/div[2]/div[1]/div/button    #configure
     click element   xpath: /html/body/div[1]/div/div[2]/section/div[2]/div[1]/div/div/div/section[1]/div[2]/div[1]/label/i  #select all websites
     click element   xpath: /html/body/div[1]/div/div[2]/section/div[2]/div[1]/div/div/div/section[2]/div/div/div[1]/label/i   #select all View
     click element   xpath: /html/body/div[1]     #click outside
     log to console  all websites all view available in table
     sleep   5

     page should contain element     xpath: /html/body/div[1]/div/div[2]/section/div[2]/div[2]/table/tbody/tr[1]/td  #VISIBILITY
     log to console  VISIBILITY found
     page should contain element     xpath: /html/body/div[1]/div/div[2]/section/div[2]/div[2]/table/tbody/tr[2]/td     #PRICE
     log to console  PRICE found
     page should contain element     xpath: /html/body/div[1]/div/div[2]/section/div[2]/div[2]/table/tbody/tr[3]/td     #AVAILABILITY
     log to console  AVAILABILITY found
     page should contain element     xpath: /html/body/div[1]/div/div[2]/section/div[2]/div[2]/table/tbody/tr[4]/td     #DELIVERY
     log to console  DELIVERY found
     page should contain element     xpath: /html/body/div[1]/div/div[2]/section/div[2]/div[2]/table/tbody/tr[5]/td     #SHIPPING
     log to console  SHIPPING found
     page should contain element     xpath: /html/body/div[1]/div/div[2]/section/div[2]/div[2]/table/tbody/tr[6]/td     #CONSUMER RATING
     log to console  CONSUMER RATING found

Edit product
    click element   xpath: /html/body/div[1]/div/div[1]/header/div/button[2]    #Edit button
    log to console   redirected to Edit Product page
    sleep   5
    page should contain  Product Name
    log to console   Product Name found
    page should contain  SKU
    log to console   SKU found
    page should contain  Brand
    log to console   Brand found
    page should contain  Product Type
    log to console   Product Type found
     sleep   5
    input text   xpath: /html/body/div[1]/div/div/section[2]/section/div[1]/div[1]/input     Acer Predator Triton 500 i5     #input product name
    log to console   product name edited
    click element    xpath: /html/body/div[1]/div/div/section[2]/section/div[2]/section[1]/div/header    #brand
    log to console   brand dropdown clickable
    click element    xpath: /html/body/div[1]/div/div/section[2]/section/div[2]/section[2]/div/header    #product type
    log to console   product name dropdown clickable

    #MATCH YOUR PRODUCT
    page should contain  Match your product
    click element    xpath: /html/body/div[1]/div/div/section[3]/section/section/div[1]/section[1]/span  #change
    input text   xpath: /html/body/div[1]/div/div/section[3]/section/section/div[1]/section[1]/input     https://www.amazon.co.uk/Acer-Predator-Triton-PT515-51-Notebook/dp/B07NHPMF87/ref=sr_1_3?keywords=Acer+Predator+Triton+500&qid=1567618662&s=gateway&sr=8-3
    click element    xpath: /html/body/div[1]/div/div/section[3]/section/section/div[1]/section[1]/span  #update
    log to console   updated live search link
    wait until page contains element     xpath: /html/body/div[1]/div/div/section[3]/section/section/div[1]/section[2]   60s    #live search result
    log to console   live search result found

#    #POP UP SELECT RETAILERS

    click element   xpath: /html/body/div[1]/div/div/section[3]/section/section/footer/button   #add/remove retailer
    log to console   add/remove retailers
    click element   xpath: //*[@id="retail"]/div/div/div/section    #click outside
    page should contain  Select retailers    #pop up window
    click element   xpath: /html/body/div[1]/div/div/section[3]/div/div/div/div/section/div/div[1]/div/label/i    #select all
    log to console   select all retailers
    click element   xpath: /html/body/div[1]/div/div/section[3]/div/div/div/div/section/div/div[2]/div/label/i    #deselect all
    log to console   deselect all retailers
    click element    xpath: /html/body/div[1]/div/div/section[3]/div/div/div/footer/button[1]    #cancel

    #SEARCH
#    input text  xpath: /html/body/div[1]/div/div/section[3]/div/div/div/div/section/div/div[1]/input   .co  #input .co to search
#    clear element text  xpath: /html/body/div[1]/div/div/section[3]/div/div/div/div/section/div/div[1]/input
#    log to console  search .co retailers
#    click element   xpath: /html/body/div[1]/div/div/section[3]/div/div/div/footer/button[1]   #cancel
    #click element   xpath: /html/body/div[1]/div/div/section[3]/div/div/div/footer/button[2]   #save

    #SRP
    sleep    5
    input text   xpath: /html/body/div[1]/div/div/section[4]/section/section/div/input   99999.99    #input SRP
    log to console   SRP inputted
    mouse over   xpath: /html/body/div[1]/div/div/section[4]/section/section/div/div[2]/i
    click element    xpath: /html/body/div[1]/div/div/section[4]/section/section/div/div[2]/i    #delete SRP
    log to console   SRP deleted
    click element    xpath: /html/body/div[1]/div/div/section[4]/section/section/div/div/span[1]     #add currency
    click element    xpath: /html/body/div[1]/div/div/section[4]/section/section/div/ul/li   #choose currency
    log to console   currency added
    page should contain element  xpath: /html/body/div[1]/div/div/section[4]/section/section/div     #added currency
    sleep    5

    #PRODUCT IMAGE
    click element    xpath: /html/body/div[1]/div/div/section[5]/section/section/div[2]/label    #upload image
    choose file      C:\Users\Detail\Desktop     4mb.jpg     #add image









































































